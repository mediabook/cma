//
//  SimulationCodeWindowController.m
//  CMA
//
//  Created by Scott Christley on 5/15/13.
//  Copyright (c) 2013 Scott Christley. All rights reserved.
//

#import "SimulationCodeWindowController.h"
#import "CMAServerConnection.h"

#import "internal.h"

@interface SimulationCodeWindowController ()
@end

@implementation SimulationCodeWindowController

- (id)initWithWindow:(NSWindow *)window
{
    self = [super initWithWindow:window];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

- (void)windowDidLoad
{
  [super windowDidLoad];
    
  // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
  [biomodelName setStringValue: [theBioModel modelName]];
}

- (void)setBioModel:(id)aModel
{
  if (theBioModel) [theBioModel release];
  theBioModel = [aModel retain];
  [biomodelName setStringValue: [theBioModel modelName]];
  [specTable reloadData];
}

- (IBAction)generateSimulationCode:(id)sender
{
  CMAServerConnection *theConnection = [[CMAServerConnection new] autorelease];
  NSMutableURLRequest *theRequest = [theConnection standardRequest];
  ModelSpecification *aSpec = [[theBioModel specifications] objectAtIndex: [specTable selectedRow]];
  
  NSString *body = [NSString stringWithFormat: @"procedure=GENERATE_SIMULATION_CODE&model_id=%d&spec_id=%d",
                    [theBioModel modelID], [aSpec specID]];
  printf("%s\n", [body UTF8String]);
  NSData *bodyData = [NSData dataWithBytes: [body UTF8String] length: [body length]];
  [theRequest setHTTPBody: bodyData];
  
  [theConnection performSynchronousRequest: theRequest];
  
  printf("%s\n", [[theConnection receivedData] bytes]);
  //[self processBioModelList: [theConnection receivedData]];
  
  
  //ModelSpecification *aSpec = [[ModelSpecification new] autorelease];
  //[aSpec parseCMAServerXMLDocument: [theConnection receivedData]];
  //[theBioModel addModelSpecification: aSpec];

}


//
//-----//-----//-----//-----//-----//-----//-----//-----
//
// Manage table data
//

- (void)tableViewSelectionDidChange:(NSNotification *)aNotification
{
	id obj = [aNotification object];
	if (obj == specTable) {
		if ([specTable selectedRow] < 0) {
			[generateCodeButton setEnabled: NO];
		} else {
			[generateCodeButton setEnabled: YES];
		}
	}
}

- (NSUInteger)numberOfRowsInTableView:(NSTableView *)tView
{
	if (tView == specTable) {
    NSArray *a = [theBioModel specifications];
		return [a count];
	}
  
	return 0;
}

- (id)tableView:(NSTableView *)tView objectValueForTableColumn:(NSTableColumn *)tableColumn row:(int)row
{
	if (tView == specTable) {
    NSArray *a = [theBioModel specifications];
	  ModelSpecification *aSpec = [a objectAtIndex: row];
		return [aSpec specName];
	}
  
  return nil;
}

-(void)tableView:(NSTableView *)aTableView setObjectValue:anObject forTableColumn:(NSTableColumn *)aTableColumn row:(int)rowIndex
{
}

- (void)tableView:(NSTableView *)tableView didClickTableColumn:(NSTableColumn *)tableColumn
{
}

@end
