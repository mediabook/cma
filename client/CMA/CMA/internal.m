//
//  internal.m
//  CMA
//
//  Created by Scott Christley on 5/10/13.
//  Copyright (c) 2013 Scott Christley. All rights reserved.
//

#import "internal.h"
#import "CMAServerConnection.h"

@implementation NSString (Escaping)

- (NSString *)stringWithURLPercentEscape
{
  return [(NSString *) CFURLCreateStringByAddingPercentEscapes(NULL, (CFStringRef)[[self mutableCopy] autorelease], NULL, CFSTR("￼=,!$&'()*+;@?\n\"<>#\t :/"),kCFStringEncodingUTF8) autorelease];
}

- (NSString *)stringWithReplaceURLPercentEscape
{
  return [[self stringByReplacingOccurrencesOfString:@"+" withString:@" "] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
}

@end

//
//-----//-----//-----//-----//-----//-----//-----//-----
//

NSString *checkXMLFail(NSXMLDocument *xmlDoc)
{
  NSXMLElement *root = [xmlDoc rootElement];
  NSString *errorDescription = nil;

  //printf("%s\n", [[root name] UTF8String]);
  //printf("%s\n", [[root stringValue] UTF8String]);
  if (![[root name] isEqualToString: @"cma_response"])
    return [NSString stringWithFormat:@"Poorly formed XML response from CMAServer.\n%@", [root XMLString]];

  NSXMLNode *aNode = [root attributeForName: @"result"];
  if ([[aNode stringValue] isEqualToString: @"fail"]) {
    errorDescription = @"Unknown server error.";
    int i;
    for (i = 0; i < [root childCount]; ++i) {
      NSXMLNode *item = [root childAtIndex:i];
        
      if ([[item name] isEqualToString: @"error_description"]) {
        errorDescription = [NSString stringWithString: [item stringValue]];
        break;
      }
    }
  }
  
  //printf("%s\n", [errorDescription UTF8String]);
  return errorDescription;
}

//
//-----//-----//-----//-----//-----//-----//-----//-----
//
// Ontologies
//
// ontology terms are managed in standard property lists
//
//-----//-----//-----//-----//-----//-----//-----//-----
//

// transform XML document into property list
NSArray *processOntologyTermsXML(NSData *theData)
{
  NSError *error;
  NSXMLDocument *xmlDoc = [[NSXMLDocument alloc] initWithData: theData options:0 error: &error];
  NSXMLElement *root = [xmlDoc rootElement];
  
  NSMutableArray *terms = [NSMutableArray array];
  
  int i, j;
  for (i = 0; i < [root childCount]; ++i) {
    NSXMLNode *ontItem = [root childAtIndex:i];
    
    NSMutableDictionary *term = [NSMutableDictionary dictionary];
    [terms addObject: term];
    
    for (j = 0; j < [ontItem childCount]; ++j) {
      NSXMLNode *termItem = [ontItem childAtIndex:j];
      printf("%s\n", [[termItem XMLString] UTF8String]);
      
      if ([[termItem name] isEqualToString: @"term_id"]) {
        [term setObject:[termItem stringValue] forKey: @"term_id"];
      }
      if ([[termItem name] isEqualToString: @"ontology_term_id"]) {
        [term setObject:[termItem stringValue] forKey: @"bioportal_id"];
      }
      if ([[termItem name] isEqualToString: @"term_name"]) {
        [term setObject:[termItem stringValue] forKey: @"name"];
      }
      if ([[termItem name] isEqualToString: @"term_definition"]) {
        [term setObject:[termItem stringValue] forKey: @"description"];
      }
      if ([[termItem name] isEqualToString: @"num_children"]) {
        [term setObject:[termItem stringValue] forKey: @"num_children"];
      }
    }
  }
  
  return terms;
}

// send query to CMA server
NSArray *searchOntologyTerms(NSString *termSubstring, BOOL exactMatch)
{
  CMAServerConnection *theConnection = [[CMAServerConnection new] autorelease];
  NSMutableURLRequest *theRequest = [theConnection standardRequest];

  NSString *body = [NSString stringWithFormat: @"procedure=TERM_SEARCH&term_substring=%@&is_exact=%d", termSubstring, exactMatch];
  NSData *bodyData = [NSData dataWithBytes: [body UTF8String] length: [body length]];
  [theRequest setHTTPBody: bodyData];
  
  [theConnection performSynchronousRequest: theRequest];
  
  return processOntologyTermsXML([theConnection receivedData]);
}
