//
//  BioModelSpecificationViewController.h
//  CMA
//
//  Created by Scott Christley on 5/9/13.
//  Copyright (c) 2013 Scott Christley. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "BioModel.h"
#import "ModelSpecification.h"

@interface BioModelSpecificationViewController : NSViewController
{
  BioModel *theBioModel;
  ModelSpecification *theSpec;
  
  IBOutlet NSTextField *specNameTextField;
  IBOutlet NSTextField *specDateTextField;
  IBOutlet NSTextField *mappingNameTextField;
  IBOutlet NSTextView *specTextField;
}

@end
