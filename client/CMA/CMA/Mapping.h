//
//  Mapping.h
//  CMA
//
//  Created by Scott Christley on 5/14/13.
//  Copyright (c) 2013 Scott Christley. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Mapping : NSObject
{
  int mappingID;
  NSString *mappingName;
}

- (int)mappingID;
- (NSString *)mappingName;

- (void)parseCMAServerXML: (NSXMLNode *)aNode;

@end
