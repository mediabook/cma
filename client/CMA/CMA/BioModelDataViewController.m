//
//  BioModelDataViewController.m
//  CMA
//
//  Created by Scott Christley on 5/9/13.
//  Copyright (c) 2013 Scott Christley. All rights reserved.
//

#import "BioModelDataViewController.h"

@interface BioModelDataViewController ()

@end

@implementation BioModelDataViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

@end
