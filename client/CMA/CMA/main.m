//
//  main.m
//  CMA
//
//  Created by Scott Christley on 5/3/13.
//  Copyright (c) 2013 Scott Christley. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
  return NSApplicationMain(argc, (const char **)argv);
}
