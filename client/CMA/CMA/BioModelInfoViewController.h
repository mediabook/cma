//
//  BioModelInfoViewController.h
//  CMA
//
//  Created by Scott Christley on 5/6/13.
//  Copyright (c) 2013 Scott Christley. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "BioModel.h"

@interface BioModelInfoViewController : NSViewController
{
  BioModel *theBioModel;  
  
  // BioModel main form
  IBOutlet NSTextField *bioModelNameTextField;
  IBOutlet NSTextField *bioModelVersionTextField;
  IBOutlet NSTextField *bioModelDescriptionTextField;
  IBOutlet NSTextField *bioModelDateTextField;
  IBOutlet NSTextField *bioModelOwnerTextField;
  IBOutlet NSTextField *bioModelNotesTextField;
  IBOutlet NSOutlineView *bioModelHistoryOutlineView;
}

- (void)makeFirstResponderForWindow: (NSWindow *)aWindow;

- (IBAction)updateField:(id)sender;

@end
