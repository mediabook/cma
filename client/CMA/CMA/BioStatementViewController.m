//
//  BioStatementViewController.m
//  CMA
//
//  Created by Scott Christley on 5/6/13.
//  Copyright (c) 2013 Scott Christley. All rights reserved.
//

#import "BioStatementViewController.h"
#import "AppDelegate.h"
#import "CMAServerConnection.h"

#import "internal.h"

@implementation BioStatementViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
      // register for notifications
      [[NSNotificationCenter defaultCenter] addObserver:self
                                               selector:@selector(bioModelWillLoad:)
                                                   name:BioModelWillLoadNotification object:nil];
      [[NSNotificationCenter defaultCenter] addObserver:self
                                               selector:@selector(bioModelDidLoad:)
                                                   name:BioModelDidLoadNotification object:nil];
      [[NSNotificationCenter defaultCenter] addObserver:self
                                               selector:@selector(bioModelWillSave:)
                                                   name:BioModelWillSaveNotification object:nil];
      [[NSNotificationCenter defaultCenter] addObserver:self
                                               selector:@selector(bioModelDidSave:)
                                                   name:BioModelDidSaveNotification object:nil];
      [[NSNotificationCenter defaultCenter] addObserver:self
                                               selector:@selector(bioModelWillRelinquish:)
                                                   name:BioModelWillRelinquishViewNotification object:nil];
      [[NSNotificationCenter defaultCenter] addObserver:self
                                               selector:@selector(bioModelDidDisplay:)
                                                   name:BioModelDidDisplayViewNotification object:nil];
    }
    
    return self;
}

- (void)setBioModel:(id)aModel
{
  if (theBioModel) [theBioModel release];
  theBioModel = [aModel retain];
}

- (void)allowEdit:(BOOL)aFlag
{
  [biostatementField setEditable: aFlag];
  [biostatementDefinitionField setEditable: aFlag];
  [biostatementDefinitionToken setEditable: aFlag];
  [biostatementTableColumn setEditable: aFlag];
}

- (void)updateViewContents
{
  [biostatementTable reloadData];
}

//
// Notifications
//
- (void)bioModelWillLoad:(NSNotification *)notification
{
  [self setBioModel: nil];
  [self allowEdit: NO];
}

- (void)bioModelDidLoad:(NSNotification *)notification
{
  [self setBioModel: [notification object]];
  if (!theBioModel) return;
  
  [self updateViewContents];
  [self allowEdit: YES];
}

- (void)bioModelWillSave:(NSNotification *)notification
{
}

- (void)bioModelDidSave:(NSNotification *)notification
{
}

- (void)bioModelWillRelinquish:(NSNotification *)notification
{
}

- (void)bioModelDidDisplay:(NSNotification *)notification
{
  if ([notification object] == self) {
    if (!theBioModel) {
      [self allowEdit: NO];

      NSDictionary *d = [notification userInfo];
      [self setBioModel: [d objectForKey: @"biomodel"]];
      if (!theBioModel) return;

      [self updateViewContents];
      [self allowEdit: YES];
    }
    
    //[bioModelNameTextField setStringValue: theBioModel->modelName];
  }
}

//
//-----//-----//-----//-----//-----//-----//-----//-----
//
// BioStatement entry
//

- (IBAction)delete:(id)sender
{
  if ([[[self view] window] firstResponder] == biostatementTable) {
    NSInteger idx = [biostatementTable selectedRow];
    //printf("got delete: %ld\n", idx);
    if (idx >= 0) {
      if ([theBioModel deleteBioStatementAtIndex: idx]) {
        [biostatementTable reloadData];

        [[NSNotificationCenter defaultCenter] postNotificationName: BioModelNeedsErrorCheckingNotification object: theBioModel userInfo: nil];
      }
    }
  }
}

- (IBAction)addBioStatement:(id)sender
{
  if (sender == biostatementField) {
    NSString *s = [sender stringValue];
    if ([s length] > 0) {
      BioStatement *stmt = [BioStatement new];
      stmt->statementType = 0;
      stmt->statementText = [[NSString alloc] initWithString: s];
      stmt->termID = 0;

      if ([theBioModel newBioStatement: stmt]) {
        [biostatementField setStringValue: @""];
        [biostatementTable reloadData];
        
        [[NSNotificationCenter defaultCenter] postNotificationName: BioModelNeedsErrorCheckingNotification object: theBioModel userInfo: nil];
      }
      
    }
  }

  if ((sender == biostatementDefinitionField) || (sender == biostatementDefinitionToken)) {
    NSString *s = [biostatementDefinitionField stringValue];
    if ((!s) || ([s length] == 0)) return;
    NSRange r = [s rangeOfString: @"_"];
    if (r.location != NSNotFound) {
      NSRunAlertPanel(@"Invalid definition", @"Underscores '_' not allowed in the defintion name, use different symbol", @"OK", nil, nil);
      [[[self view] window] makeFirstResponder: biostatementDefinitionField];
      return;
    }
    NSArray *a = [biostatementDefinitionToken objectValue];
    if ((!a) || ([a count] == 0)) return;
    
    BioStatement *stmt = [BioStatement new];
    stmt->statementType = 1;
    stmt->statementText = [[NSString alloc] initWithString: s];
    stmt->termID = [[ontologyTerm objectForKey: @"term_id"] intValue];
    stmt->termName = [[NSString alloc] initWithString: [ontologyTerm objectForKey: @"name"]];

    if ([theBioModel newBioStatement: stmt]) {
      if (ontologyTerm) [ontologyTerm release];
      ontologyTerm = nil;
      [biostatementDefinitionField setStringValue: @""];
      [biostatementDefinitionToken setObjectValue: nil];
      [biostatementTable reloadData];
      [[[self view] window] makeFirstResponder: biostatementDefinitionField];
      
      [[NSNotificationCenter defaultCenter] postNotificationName: BioModelNeedsErrorCheckingNotification object: theBioModel userInfo: nil];
    }
    
  }
}

- (IBAction)selectBioStatementType:(id)sender
{
  if ([[sender selectedItem] tag] == 0) {
    // Definition
    [biostatementField setHidden: YES];
    [biostatementDefinitionField setHidden: NO];
    [biostatementDefinitionLabel setHidden: NO];
    [biostatementDefinitionToken setHidden: NO];
    [[[self view] window] makeFirstResponder: biostatementField];
  } else {
    [biostatementField setHidden: NO];
    [biostatementDefinitionField setHidden: YES];
    [biostatementDefinitionLabel setHidden: YES];
    [biostatementDefinitionToken setHidden: YES];
    [[[self view] window] makeFirstResponder: biostatementDefinitionField];
  }
}

//
//-----//-----//-----//-----//-----//-----//-----//-----
//
// Ontology terms for token field
//

- (NSArray *)tokenField:(NSTokenField *)tokenFieldArg completionsForSubstring:(NSString *)substring
           indexOfToken:(NSInteger)tokenIndex indexOfSelectedItem:(NSInteger *)selectedIndex
{
  printf("tokenField\n");
	if ([substring length] < 3) return nil;
	
	NSArray *terms = searchOntologyTerms(substring, NO);
	if ([terms count] == 0) return nil;
	
	NSMutableArray *tokenList = [NSMutableArray array];
  int i;
	for (i = 0; i < [terms count]; ++i) {
		[tokenList addObject: [[terms objectAtIndex: i] objectForKey: @"name"]];
	}
  return tokenList;
}

- (NSArray *)tokenField:(NSTokenField *)tokenField shouldAddObjects:(NSArray *)tokens atIndex:(NSUInteger)index
{
  //printf("shouldAddObjects: %d %d\n", [[tokenField objectValue] count], [tokens count]);
  if (ontologyTerm) [ontologyTerm release];
  ontologyTerm = nil;

  // only one token allowed
  if (([[tokenField objectValue] count] > 1) || ([tokens count] > 1)) {
    NSRunAlertPanel(@"Illegal number of terms", @"Only one term is allowed.", @"OK", nil, nil);
    return nil;
  }
  
  // verify it is a complete valid ontology term
  NSArray *a = searchOntologyTerms([tokens objectAtIndex: 0], YES);
  if ((!a) || ([a count] != 1)) {
    NSRunAlertPanel(@"Invalid term", @"'%@' is not a valid term.", @"OK", nil, nil, [[tokenField objectValue] objectAtIndex:0]);
    return nil;
  }

  // cache the ontology term
  ontologyTerm = [[a objectAtIndex:0] retain];

  return tokens;
}

//
//-----//-----//-----//-----//-----//-----//-----//-----
//
// Manage table data
//

//
// NSTableView delegate protocol
//
- (void)tableViewSelectionDidChange:(NSNotification *)aNotification
{
	//id obj = [aNotification object];
  //NSInteger idx = [biostatementTable selectedRow];
}

- (NSUInteger)numberOfRowsInTableView:(NSTableView *)tView
{
	if (theBioModel) return [theBioModel->biostatements count];
  
	return 0;
}

- (id)tableView:(NSTableView *)tView objectValueForTableColumn:(NSTableColumn *)tableColumn row:(int)row
{
  if (theBioModel) {
    if ([[tableColumn identifier] isEqualToString: @"type"]) {
      BioStatement *aStatement = [theBioModel->biostatements objectAtIndex: row];
      if (aStatement->statementType) return @"Definition";
      else return @"BioStatement";
    }
    
		if ([[tableColumn identifier] isEqualToString: @"statement"]) {
			BioStatement *aStatement = [theBioModel->biostatements objectAtIndex: row];
      if (aStatement->statementType) return [NSString stringWithFormat: @"%@ IS A %@", aStatement->statementText, aStatement->termName];
			else return aStatement->statementText;
		}
	}
  
  return nil;
}

- (void)tableView:(NSTableView *)aTableView setObjectValue:anObject forTableColumn:(NSTableColumn *)aTableColumn row:(int)rowIndex
{
  if ([[aTableColumn identifier] isEqualToString: @"statement"]) {
    BioStatement *aStatement = [[theBioModel biostatements] objectAtIndex: rowIndex];
    if ([anObject length] == 0) return;
    if ([anObject isEqualToString: [aStatement statementText]]) return;

    printf("setObjectValue: %s\n", [anObject UTF8String]);

    if ([aStatement statementType]) {
      // gotta parse text if its a definition
      NSRange r = [anObject rangeOfString: @"IS A"];
      if (r.location == NSNotFound) {
        NSRunAlertPanel(@"Invalid format for definition", @"missing 'IS A'", @"OK", nil, nil);
        return;
      }

      NSRange rn; rn.location = 0; rn.length = r.location;
      NSString *name = [[anObject substringWithRange: rn] stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]];
      if ((!name) || ([name length] == 0)) {
        NSRunAlertPanel(@"Invalid format for definition", @"missing object name before IS A", @"OK", nil, nil);
        return;        
      }
      NSRange ru = [name rangeOfString: @"_"];
      if (ru.location != NSNotFound) {
        NSRunAlertPanel(@"Invalid definition", @"Underscores '_' not allowed in the object name, use different symbol", @"OK", nil, nil);
        return;
      }

      NSRange rt; rt.location = r.location + r.length; rt.length = [anObject length] - rt.location;
      NSString *termName = [[anObject substringWithRange: rt] stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]];
      if ((!termName) || ([termName length] == 0)) {
        NSRunAlertPanel(@"Invalid format for definition", @"missing ontology term after IS A", @"OK", nil, nil);
        return;
      }
      
      // verify it is a complete valid ontology term
      NSArray *a = searchOntologyTerms(termName, YES);
      if ((!a) || ([a count] != 1)) {
        NSRunAlertPanel(@"Invalid ontology term", @"'%@' is not a valid ontology term.", @"OK", nil, nil, termName);
        return;
      }

      // save the modified definition
      if ([aStatement updateBioStatement: name term: [a objectAtIndex:0]]) {
        [theBioModel updateChecked: NO];
        [[NSNotificationCenter defaultCenter] postNotificationName: BioModelNeedsErrorCheckingNotification object: theBioModel userInfo: nil];
      }
      
    } else {
      // save the modified statement
      if ([aStatement updateBioStatement: anObject]) {
        [theBioModel updateChecked: NO];
        [[NSNotificationCenter defaultCenter] postNotificationName: BioModelNeedsErrorCheckingNotification object: theBioModel userInfo: nil];
      }
    }
  }
}

- (void)tableView:(NSTableView *)tableView didClickTableColumn:(NSTableColumn *)tableColumn
{
}

@end
