//
//  BioModel.m
//  CMA
//
//  Created by Scott Christley on 5/3/13.
//  Copyright (c) 2013 Scott Christley. All rights reserved.
//

#import "BioModel.h"
#import "CMAServerConnection.h"

#import "internal.h"

// Notifications
NSString *BioModelWillLoadNotification = @"BioModelWillLoadNotification";
NSString *BioModelDidLoadNotification = @"BioModelDidLoadNotification";
NSString *BioModelWillSaveNotification = @"BioModelWillSaveNotification";
NSString *BioModelDidSaveNotification = @"BioModelDidSaveNotification";
NSString *BioModelWasModifiedNotification = @"BioModelWasModifiedNotification";
NSString *BioModelNeedsErrorCheckingNotification = @"BioModelNeedsErrorCheckingNotification";

@implementation BioStatement

- (int)statementID { return statementID; }
- (int)statementNum { return statementNum; }
- (int)statementType { return statementType; }
- (NSString *)statementText { return statementText; }
- (int)termID { return termID; }
- (NSString *)termName { return termName; }

- (void)parseCMAServerXML: (NSXMLNode *)aNode
{
  int i;
  for (i = 0; i < [aNode childCount]; ++i) {
    NSXMLNode *item = [aNode childAtIndex:i];

    if ([[item name] isEqualToString: @"statement_id"]) {
      statementID = [[item stringValue] intValue];
    }
    if ([[item name] isEqualToString: @"statement_num"]) {
      statementNum = [[item stringValue] intValue];
    }
    if ([[item name] isEqualToString: @"statement"]) {
      statementText = [[NSString alloc] initWithString: [item stringValue]];
    }
    if ([[item name] isEqualToString: @"statement_type"]) {
      statementType = [[item stringValue] intValue];
    }
    if ([[item name] isEqualToString: @"term_id"]) {
      termID = [[item stringValue] intValue];
    }
    if ([[item name] isEqualToString: @"term_name"]) {
      termName = [[NSString alloc] initWithString: [item stringValue]];
    }
  }
}

- (NSString *)parseCMAServerXMLDocument: (NSData *)theData
{
  NSError *error;
  NSXMLDocument *xmlDoc = [[NSXMLDocument alloc] initWithData: theData options:0 error: &error];
  if (!xmlDoc) return [NSString stringWithFormat: @"Invalid XML received from CMAServer: %@\n%@",
                       [error localizedDescription], [[[NSString alloc] initWithData:theData encoding:NSUTF8StringEncoding] autorelease]];

  NSXMLElement *root = [xmlDoc rootElement];
  printf("%s\n", [[root XMLString] UTF8String]);

  NSString *errorDesc = checkXMLFail(xmlDoc);
  if (errorDesc) return errorDesc;

  [self parseCMAServerXML: [root childAtIndex:0]];
  return nil;
}

- (BOOL)saveModification:(NSString *)fieldName value: (NSString *)fieldValue
{
  CMAServerConnection *theConnection = [[CMAServerConnection new] autorelease];
  NSMutableURLRequest *theRequest = [theConnection standardRequest];

  NSString *body = [NSString stringWithFormat: @"procedure=UPDATE_BIOSTATEMENT&statement_id=%d&field=%@&value=%@",
                    statementID, fieldName, [fieldValue stringWithURLPercentEscape]];
  NSData *bodyData = [NSData dataWithBytes: [body UTF8String] length: [body length]];
  [theRequest setHTTPBody: bodyData];
  
  [theConnection performSynchronousRequest: theRequest];
  
  // process return
  NSError *error;
  NSData *theData = [theConnection receivedData];
  NSXMLDocument *xmlDoc = [[NSXMLDocument alloc] initWithData: theData options:0 error: &error];
  if (!xmlDoc) {
    NSString *errorDesc = [NSString stringWithFormat: @"Invalid XML received from CMAServer: %@\n%@",
                           [error localizedDescription], [[[NSString alloc] initWithData:theData encoding:NSUTF8StringEncoding] autorelease]];
    NSRunAlertPanel(@"Could not update BioStatement", errorDesc, @"OK", nil, nil);
    return NO;
  }
  
  NSString *errorDesc = checkXMLFail(xmlDoc);
  if (errorDesc) {
    NSRunAlertPanel(@"Could not update BioStatement", errorDesc, @"OK", nil, nil);
    return NO;
  }

  return YES;
}

- (BOOL)updateBioStatement:(NSString *)aString
{
  if (![statementText isEqualToString: aString]) {
    if ([self saveModification: @"statement" value: aString]) {
      if (statementText) [statementText release];
      statementText = [[NSString alloc] initWithString: aString];
    } else return NO;
  }
  
  return YES;
}

- (BOOL)updateBioStatement:(NSString *)aString term:(NSDictionary *)aTerm
{
  if (![self saveModification: @"statement" value: aString]) return NO;
  
  if (![self saveModification: @"term_id" value: [aTerm objectForKey: @"term_id"]]) return NO;

  if (statementText) [statementText release];
  statementText = [[NSString alloc] initWithString: aString];
  termID = [[aTerm objectForKey: @"term_id"] intValue];
  if (termName) [termName release];
  termName = [[NSString alloc] initWithString: [aTerm objectForKey: @"name"]];

  return YES;
}

@end

@implementation BioModel

- (id)init
{
  self = [super init];
  if (self) {
    biostatements = [NSMutableArray new];
    specifications = [NSMutableArray new];
    simulations = [NSMutableArray new];
  }
  
  return self;
}
- (void)dealloc
{
  if (modelName) [modelName release];
  if (modelVersion) [modelVersion release];
  if (versionedFromName) [versionedFromName release];
  if (versionedFromVersion) [versionedFromVersion release];
  if (modelDate) [modelDate release];
  if (modelDescription) [modelDescription release];
  if (modelOwnerName) [modelOwnerName release];
  if (modelNotes) [modelNotes release];
  if (biostatements) [biostatements release];
  if (specifications) [specifications release];
  if (simulations) [simulations release];

  [super dealloc];
}

- (void)parseCMAServerXML: (NSXMLNode *)aNode
{
  int i;
  for (i = 0; i < [aNode childCount]; ++i) {
    NSXMLNode *item = [aNode childAtIndex:i];
    
    if ([[item name] isEqualToString: @"model_id"]) {
      modelID = [[item stringValue] intValue];
    }
    if ([[item name] isEqualToString: @"model_name"]) {
      modelName = [[[item stringValue] stringWithReplaceURLPercentEscape] retain];
    }
    if ([[item name] isEqualToString: @"model_version"]) {
      modelVersion = [[[item stringValue] stringWithReplaceURLPercentEscape] retain];
    }
    if ([[item name] isEqualToString: @"model_versioned_from"]) {
      versionedFrom = [[item stringValue] intValue];
    }
    if ([[item name] isEqualToString: @"model_versioned_from_name"]) {
      versionedFromName = [[[item stringValue] stringWithReplaceURLPercentEscape] retain];
    }
    if ([[item name] isEqualToString: @"model_versioned_from_version"]) {
      versionedFromVersion = [[[item stringValue] stringWithReplaceURLPercentEscape] retain];
    }
    if ([[item name] isEqualToString: @"model_date"]) {
      modelDate = [[NSDate dateWithTimeIntervalSince1970: [[item stringValue] intValue]] retain];
    }
    if ([[item name] isEqualToString: @"model_description"]) {
      modelDescription = [[[item stringValue] stringWithReplaceURLPercentEscape] retain];
    }
    if ([[item name] isEqualToString: @"model_owner"]) {
      modelOwner = [[item stringValue] intValue];
    }
    if ([[item name] isEqualToString: @"model_notes"]) {
      modelNotes = [[[item stringValue] stringWithReplaceURLPercentEscape] retain];
    }
    if ([[item name] isEqualToString: @"model_immutable"]) {
      isImmutable = [[item stringValue] intValue];
    }
    if ([[item name] isEqualToString: @"model_deprecated"]) {
      isDeprecated = [[item stringValue] intValue];
    }
    if ([[item name] isEqualToString: @"model_checked"]) {
      isChecked = [[item stringValue] intValue];
    }
    
    if ([[item name] isEqualToString: @"biostatement"]) {
      printf("%ld\n", (unsigned long)[item childCount]);
      BioStatement *aStatement = [[BioStatement new] autorelease];
      [biostatements addObject: aStatement];
      [aStatement parseCMAServerXML: item];
    }

    if ([[item name] isEqualToString: @"model_specification"]) {
      ModelSpecification *aSpec = [[ModelSpecification new] autorelease];
      [specifications addObject: aSpec];
      [aSpec parseCMAServerXML: item];
    }
  }
}

- (NSString *)parseCMAServerXMLDocument: (NSData *)theData;
{
  NSError *error;
  NSXMLDocument *xmlDoc = [[NSXMLDocument alloc] initWithData: theData options:0 error: &error];
  if (!xmlDoc) return [NSString stringWithFormat: @"Invalid XML received from CMAServer: %@\n%@",
                       [error localizedDescription], [[[NSString alloc] initWithData:theData encoding:NSUTF8StringEncoding] autorelease]];
  
  NSXMLElement *root = [xmlDoc rootElement];
  printf("%s\n", [[root XMLString] UTF8String]);
  
  NSString *errorDesc = checkXMLFail(xmlDoc);
  if (errorDesc) return errorDesc;
  
  [self parseCMAServerXML: [root childAtIndex:0]];
  return nil;
}

- (BOOL)saveModification:(NSString *)fieldName value: (NSString *)fieldValue
{
  CMAServerConnection *theConnection = [[CMAServerConnection new] autorelease];
  NSMutableURLRequest *theRequest = [theConnection standardRequest];
  
  NSString *body = [NSString stringWithFormat: @"procedure=UPDATE_BIOMODEL_MASTER&model_id=%d&field=%@&value=%@",
                    modelID, fieldName, [fieldValue stringWithURLPercentEscape]];
  NSData *bodyData = [NSData dataWithBytes: [body UTF8String] length: [body length]];
  [theRequest setHTTPBody: bodyData];
  
  [theConnection performSynchronousRequest: theRequest];
  
  // process return
  NSError *error;
  NSData *theData = [theConnection receivedData];
  NSXMLDocument *xmlDoc = [[NSXMLDocument alloc] initWithData: theData options:0 error: &error];
  if (!xmlDoc) {
    NSString *errorDesc = [NSString stringWithFormat: @"Invalid XML received from CMAServer: %@\n%@",
                           [error localizedDescription], [[[NSString alloc] initWithData:theData encoding:NSUTF8StringEncoding] autorelease]];
    NSRunAlertPanel(@"Could not update BioModel", errorDesc, @"OK", nil, nil);
    return NO;
  }
  
  NSString *errorDesc = checkXMLFail(xmlDoc);
  if (errorDesc) {
    NSRunAlertPanel(@"Could not update BioModel", errorDesc, @"OK", nil, nil);
    return NO;
  }

  return YES;
}

- (int)modelID { return modelID; }
- (NSString *)modelName { return modelName; }
- (NSString *)modelVersion { return modelVersion; }
- (int)versionedFrom { return versionedFrom; }
- (NSString *)versionedFromName { return versionedFromName; }
- (NSString *)versionedFromVersion { return versionedFromVersion; }
- (NSDate *)modelDate { return modelDate; }
- (NSString *)modelDescription { return modelDescription; }
- (int)modelOwner { return modelOwner; }
- (NSString *)modelOwnerName { return modelOwnerName; }
- (NSString *)modelNotes { return modelNotes; }
- (BOOL)isImmutable { return isImmutable; }
- (BOOL)isDeprecated { return isDeprecated; }
- (BOOL)isChecked { return isChecked; }
- (NSArray *)biostatements { return biostatements; }
- (NSArray *)specifications { return specifications; }
- (NSArray *)simulations { return simulations; }

- (void)addBioStatement:(BioStatement *)aStatement
{
  
}

- (void)addModelSpecification:(ModelSpecification *)aSpec
{
  if (!aSpec) return;
  
  [specifications addObject: aSpec];
  
  [[NSNotificationCenter defaultCenter]
   postNotificationName: BioModelWasModifiedNotification object: self
   userInfo: [NSDictionary dictionaryWithObjectsAndKeys:
              @"model_specification", @"field", aSpec, @"value", nil]];
}

- (BOOL)updateBioModelName:(NSString *)aName
{
  if (![modelName isEqualToString: aName]) {
    if ([self saveModification: @"model_name" value: aName]) {
      if (modelName) [modelName release];
      modelName = [aName retain];
      
      [[NSNotificationCenter defaultCenter]
       postNotificationName: BioModelWasModifiedNotification object: self
       userInfo: [NSDictionary dictionaryWithObjectsAndKeys:
                  @"model_name", @"field", modelName, @"value", nil]];
      //printf("new value: %s\n", [[sender stringValue] UTF8String]);
    } else return NO;
  }
  return YES;
}

- (BOOL)updateBioModelVersion:(NSString *)aVersion
{
  if (![modelVersion isEqualToString: aVersion]) {
    if ([self saveModification: @"model_version" value: aVersion]) {
      if (modelVersion) [modelVersion release];
      modelVersion = [aVersion retain];
      
      [[NSNotificationCenter defaultCenter]
       postNotificationName: BioModelWasModifiedNotification object: self
       userInfo: [NSDictionary dictionaryWithObjectsAndKeys:
                  @"model_version", @"field", modelVersion, @"value", nil]];
      //printf("new value: %s\n", [[sender stringValue] UTF8String]);
    } else return NO;
  }
  return YES;
}

- (BOOL)updateBioModelDescription:(NSString *)aDesc
{
  if (![modelDescription isEqualToString: aDesc]) {
    if ([self saveModification: @"model_description" value: aDesc]) {
      if (modelDescription) [modelDescription release];
      modelDescription = [aDesc retain];
      
      [[NSNotificationCenter defaultCenter]
       postNotificationName: BioModelWasModifiedNotification object: self
       userInfo: [NSDictionary dictionaryWithObjectsAndKeys:
                  @"model_description", @"field", modelDescription, @"value", nil]];
    } else return NO;
  }
  return YES;
}

- (BOOL)updateBioModelNotes:(NSString *)aString
{
  if (![modelNotes isEqualToString: aString]) {
    if ([self saveModification: @"model_notes" value: aString]) {
      if (modelNotes) [modelNotes release];
      modelNotes = [aString retain];
      
      [[NSNotificationCenter defaultCenter]
       postNotificationName: BioModelWasModifiedNotification object: self
       userInfo: [NSDictionary dictionaryWithObjectsAndKeys:
                  @"model_notes", @"field", modelNotes, @"value", nil]];
    } else return NO;
  }
  return YES;
}

- (BOOL)updateChecked:(BOOL)aFlag
{
  if (isChecked != aFlag) {
    NSString *val = @"f";
    if (aFlag) val = @"t";
    if ([self saveModification: @"model_checked" value: val]) {
      isChecked = aFlag;
      
      [[NSNotificationCenter defaultCenter]
       postNotificationName: BioModelWasModifiedNotification object: self
       userInfo: [NSDictionary dictionaryWithObjectsAndKeys:
                  @"model_checked", @"field", [NSNumber numberWithBool: aFlag], @"value", nil]];
    } else return NO;
  }
  return YES;
}

- (BOOL)errorCheckBioModel
{
  CMAServerConnection *theConnection = [[CMAServerConnection new] autorelease];
  NSMutableURLRequest *theRequest = [theConnection standardRequest];
  
  NSString *body = [NSString stringWithFormat: @"procedure=ERROR_CHECK_BIOMODEL&model_id=%d", modelID];
  NSData *bodyData = [NSData dataWithBytes: [body UTF8String] length: [body length]];
  [theRequest setHTTPBody: bodyData];
  
  [theConnection performSynchronousRequest: theRequest];
  
  printf("%s\n", [[theConnection receivedData] bytes]);
  
  // process return
  NSError *error;
  NSData *theData = [theConnection receivedData];
  NSXMLDocument *xmlDoc = [[NSXMLDocument alloc] initWithData: theData options:0 error: &error];
  if (!xmlDoc) {
    NSString *errorDesc = [NSString stringWithFormat: @"Invalid XML received from CMAServer: %@\n%@",
                           [error localizedDescription], [[[NSString alloc] initWithData:theData encoding:NSUTF8StringEncoding] autorelease]];
    NSRunAlertPanel(@"Could not error check BioModel", errorDesc, @"OK", nil, nil);
    return NO;
  }

  // TODO: separate server errors from model errors
  NSString *errorDesc = checkXMLFail(xmlDoc);
  if (errorDesc) {
    NSRunAlertPanel(@"BioModel has errors", [xmlDoc XMLString], @"OK", nil, nil);
    return NO;
  }

  return YES;
}

//
//-----//-----//-----//-----//-----//-----//-----//-----
//
#if 0
#pragma mark == MANAGE BIOSTATEMENTS ==
#endif

- (BOOL)newBioStatement:(BioStatement *)stmt
{
  CMAServerConnection *theConnection = [[CMAServerConnection new] autorelease];
  NSMutableURLRequest *theRequest = [theConnection standardRequest];
  
  NSString *body = [NSString stringWithFormat: @"procedure=NEW_BIOSTATEMENT&model_id=%d&statement=%@&is_definition=%d&term_id=%d",
                    modelID, [[stmt statementText] stringWithURLPercentEscape], [stmt statementType], [stmt termID]];
  NSData *bodyData = [NSData dataWithBytes: [body UTF8String] length: [body length]];
  [theRequest setHTTPBody: bodyData];
  
  [theConnection performSynchronousRequest: theRequest];
  
  NSString *errorDesc = [stmt parseCMAServerXMLDocument: [theConnection receivedData]];
  if (errorDesc) {
    NSRunAlertPanel(@"Could not create new BioStatement", errorDesc, @"OK", nil, nil);
    return NO;
  }
  
  [biostatements addObject: stmt];
  [self updateChecked: NO];

  return YES;
}

- (BOOL)deleteBioStatementAtIndex:(NSInteger)anIndex
{
  if (anIndex < 0) return NO;
  if (anIndex >= [biostatements count]) return NO;
  
  BioStatement *aStatement = [biostatements objectAtIndex: anIndex];
  
  CMAServerConnection *theConnection = [[CMAServerConnection new] autorelease];
  NSMutableURLRequest *theRequest = [theConnection standardRequest];

  NSString *body = [NSString stringWithFormat: @"procedure=DELETE_BIOSTATEMENT&model_id=%d&statement_id=%d", modelID, [aStatement statementID]];
  printf("delete: %s\n", [body UTF8String]);
  NSData *bodyData = [NSData dataWithBytes: [body UTF8String] length: [body length]];
  [theRequest setHTTPBody: bodyData];
  
  [theConnection performSynchronousRequest: theRequest];
  
  // process return
  NSError *error;
  NSData *theData = [theConnection receivedData];
  NSXMLDocument *xmlDoc = [[NSXMLDocument alloc] initWithData: theData options:0 error: &error];
  if (!xmlDoc) {
    NSString *errorDesc = [NSString stringWithFormat: @"Invalid XML received from CMAServer: %@\n%@",
                           [error localizedDescription], [[[NSString alloc] initWithData:theData encoding:NSUTF8StringEncoding] autorelease]];
    NSRunAlertPanel(@"Could not delete BioStatement", errorDesc, @"OK", nil, nil);
    return NO;
  }

  NSString *errorDesc = checkXMLFail(xmlDoc);
  if (errorDesc) {
    NSRunAlertPanel(@"Could not delete BioStatement", errorDesc, @"OK", nil, nil);
    return NO;
  }

  [biostatements removeObject: aStatement];
  [self updateChecked: NO];

  return YES;
}

@end
