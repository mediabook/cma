//
//  internal.h
//  CMA
//
//  Created by Scott Christley on 5/10/13.
//  Copyright (c) 2013 Scott Christley. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Escaping)

- (NSString *)stringWithURLPercentEscape;
- (NSString *)stringWithReplaceURLPercentEscape;

@end

//
//-----//-----//-----//-----//-----//-----//-----//-----
//
NSString *checkXMLFail(NSXMLDocument *xmlDoc);

//
//-----//-----//-----//-----//-----//-----//-----//-----
//
// Ontologies
//
NSArray *processOntologyTermsXML(NSData *theData);
NSArray *searchOntologyTerms(NSString *termSubstring, BOOL exactMatch);
