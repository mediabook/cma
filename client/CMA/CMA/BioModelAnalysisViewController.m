//
//  BioModelAnalysisViewController.m
//  CMA
//
//  Created by Scott Christley on 5/9/13.
//  Copyright (c) 2013 Scott Christley. All rights reserved.
//

#import "BioModelAnalysisViewController.h"

@interface BioModelAnalysisViewController ()

@end

@implementation BioModelAnalysisViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

@end
