//
//  BioModel.h
//  CMA
//
//  Created by Scott Christley on 5/3/13.
//  Copyright (c) 2013 Scott Christley. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ModelSpecification.h"

// Notifications
extern NSString *BioModelWillLoadNotification;
extern NSString *BioModelDidLoadNotification;
extern NSString *BioModelWillSaveNotification;
extern NSString *BioModelDidSaveNotification;
extern NSString *BioModelWasModifiedNotification;
extern NSString *BioModelNeedsErrorCheckingNotification;

@interface BioStatement : NSObject
{
  @public
	int statementID;
	int statementNum;
	int statementType;
	NSString *statementText;
  int termID;
  NSString *termName;
}

- (int)statementID;
- (int)statementNum;
- (int)statementType;
- (NSString *)statementText;
- (int)termID;
- (NSString *)termName;

- (void)parseCMAServerXML: (NSXMLNode *)aNode;
- (NSString *)parseCMAServerXMLDocument: (NSData *)theData;

- (BOOL)updateBioStatement:(NSString *)aString;
- (BOOL)updateBioStatement:(NSString *)aString term:(NSDictionary *)aTerm;

@end

@interface BioModel : NSObject
{
  @public
	int modelID;
	NSString *modelName;
	NSString *modelVersion;
	int versionedFrom;
	NSString *versionedFromName;
	NSString *versionedFromVersion;
	NSDate *modelDate;
  NSString *modelDescription;
  int modelOwner;
  NSString *modelOwnerName;
  NSString *modelNotes;
  BOOL isImmutable;
  BOOL isDeprecated;
  BOOL isChecked;
	
	NSMutableArray *biostatements;
  NSMutableArray *specifications;
  NSMutableArray *simulations;
}

- (NSString *)parseCMAServerXMLDocument: (NSData *)theData;

- (BOOL)saveModification:(NSString *)fieldName value: (NSString *)fieldValue;

- (int)modelID;
- (NSString *)modelName;
- (NSString *)modelVersion;
- (int)versionedFrom;
- (NSString *)versionedFromName;
- (NSString *)versionedFromVersion;
- (NSDate *)modelDate;
- (NSString *)modelDescription;
- (int)modelOwner;
- (NSString *)modelOwnerName;
- (NSString *)modelNotes;
- (BOOL)isImmutable;
- (BOOL)isDeprecated;
- (BOOL)isChecked;
- (NSArray *)biostatements;
- (NSArray *)specifications;
- (NSArray *)simulations;

- (void)addBioStatement:(BioStatement *)aStatement;
- (void)addModelSpecification:(ModelSpecification *)aSpec;

- (BOOL)updateBioModelName:(NSString *)aName;
- (BOOL)updateBioModelVersion:(NSString *)aVersion;
- (BOOL)updateBioModelDescription:(NSString *)aDesc;
- (BOOL)updateBioModelNotes:(NSString *)aString;
- (BOOL)updateChecked:(BOOL)aFlag;

- (BOOL)errorCheckBioModel;

- (BOOL)newBioStatement:(BioStatement *)stmt;
- (BOOL)deleteBioStatementAtIndex:(NSInteger)anIndex;

@end
