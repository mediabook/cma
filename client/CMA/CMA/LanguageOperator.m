//
//  LanguageOperator.m
//  CMA
//
//  Created by Scott Christley on 5/7/13.
//  Copyright (c) 2013 Scott Christley. All rights reserved.
//

#import "LanguageOperator.h"
#import "CMAServerConnection.h"

#import "internal.h"

@implementation LanguageOperator

+ (LanguageOperator *)newLanguageOperator:(NSString *)name reduceTo:(int)anInt arguments:(NSArray *)argList
{
  CMAServerConnection *theConnection = [[CMAServerConnection new] autorelease];
  NSMutableURLRequest *theRequest = [theConnection standardRequest];
  
  NSMutableString *body = [NSMutableString stringWithFormat: @"procedure=NEW_OPERATOR&operator_name=%@&reduce_id=%d&term_total=%ld",
                           [name stringWithURLPercentEscape], anInt, (unsigned long)[argList count]];
  int i;
  for (i = 0; i < [argList count]; ++i) {
    NSString *s = [NSString stringWithFormat: @"&term_id_%d=%@", i, [[argList objectAtIndex: i] objectForKey: @"term_id"]];
    [body appendString: s];
  }
  printf("%s\n", [body UTF8String]);
  NSData *bodyData = [NSData dataWithBytes: [body UTF8String] length: [body length]];
  [theRequest setHTTPBody: bodyData];
  
  [theConnection performSynchronousRequest: theRequest];

  LanguageOperator *anOperator = [LanguageOperator new];

  NSString *errorDesc = [anOperator parseCMAServerXMLDocument: [theConnection receivedData]];
  if (errorDesc) {
    NSRunAlertPanel(@"Could not create new language operator", errorDesc, @"OK", nil, nil);
    return nil;
  }
  
  return anOperator;
}

- (NSString *)parseCMAServerXMLDocument: (NSData *)theData
{
  NSError *error;
  NSXMLDocument *xmlDoc = [[NSXMLDocument alloc] initWithData: theData options:0 error: &error];
  if (!xmlDoc) return [NSString stringWithFormat: @"Invalid XML received from CMAServer: %@\n%@",
                       [error localizedDescription], [[[NSString alloc] initWithData:theData encoding:NSUTF8StringEncoding] autorelease]];
  
  NSXMLElement *root = [xmlDoc rootElement];
  printf("%s\n", [[root XMLString] UTF8String]);
  
  NSString *errorDesc = checkXMLFail(xmlDoc);
  if (errorDesc) return errorDesc;
  
  [self parseCMAServerXML: [root childAtIndex:0]];
  return nil;
}

- (void)parseCMAServerXML: (NSXMLNode *)aNode
{
  int i, j;
  arguments = [NSMutableArray new];
  for (i = 0; i < [aNode childCount]; ++i) {
    NSXMLNode *item = [aNode childAtIndex:i];
    
    if ([[item name] isEqualToString: @"operator_name"]) {
      declaration = [[NSString alloc] initWithString: [item stringValue]];
    }
    if ([[item name] isEqualToString: @"operator_id"]) {
      operatorID = [[item stringValue] intValue];
    }
    if ([[item name] isEqualToString: @"reduce_id"]) {
      reduceID = [[item stringValue] intValue];
    }
    
    if ([[item name] isEqualToString: @"operator_arguments"]) {
      NSMutableDictionary *term = [NSMutableDictionary dictionary];
      [arguments addObject: term];

      for (j = 0; j < [item childCount]; ++j) {
        NSXMLNode *termItem = [item childAtIndex:j];
        
        if ([[termItem name] isEqualToString: @"term_id"]) {
          [term setObject:[termItem stringValue] forKey: @"term_id"];
        }
        if ([[termItem name] isEqualToString: @"term_name"]) {
          [term setObject:[termItem stringValue] forKey: @"name"];
        }
      }
    }
  }
}

- (int)operatorID { return operatorID; }
- (int)reduceID { return reduceID; }
- (NSString *)declaration { return declaration; }
- (NSArray *)arguments { return arguments; }

- (BOOL)deleteLanguageOperator
{
  CMAServerConnection *theConnection = [[CMAServerConnection new] autorelease];
  NSMutableURLRequest *theRequest = [theConnection standardRequest];
  
  NSString *body = [NSString stringWithFormat: @"procedure=DELETE_OPERATOR&operator_id=%d", operatorID];
  printf("delete: %s\n", [body UTF8String]);
  NSData *bodyData = [NSData dataWithBytes: [body UTF8String] length: [body length]];
  [theRequest setHTTPBody: bodyData];
  
  [theConnection performSynchronousRequest: theRequest];
  
  // process return
  NSError *error;
  NSData *theData = [theConnection receivedData];
  NSXMLDocument *xmlDoc = [[NSXMLDocument alloc] initWithData: theData options:0 error: &error];
  if (!xmlDoc) {
    NSString *errorDesc = [NSString stringWithFormat: @"Invalid XML received from CMAServer: %@\n%@",
                           [error localizedDescription], [[[NSString alloc] initWithData:theData encoding:NSUTF8StringEncoding] autorelease]];
    NSRunAlertPanel(@"Could not delete language operator", errorDesc, @"OK", nil, nil);
    return NO;
  }
  
  NSString *errorDesc = checkXMLFail(xmlDoc);
  if (errorDesc) {
    NSRunAlertPanel(@"Could not delete language operator", errorDesc, @"OK", nil, nil);
    return NO;
  }
  
  return YES;
}

- (BOOL)updateLanguageOperator:(NSString *)name reduceTo:(int)anInt arguments:(NSArray *)argList
{
  CMAServerConnection *theConnection = [[CMAServerConnection new] autorelease];
  NSMutableURLRequest *theRequest = [theConnection standardRequest];
  
  NSMutableString *body = [NSMutableString stringWithFormat: @"procedure=UPDATE_OPERATOR&operator_id=%d&operator_name=%@&reduce_id=%d&term_total=%ld",
                           operatorID, [name stringWithURLPercentEscape], anInt, (unsigned long)[argList count]];
  int i;
  for (i = 0; i < [argList count]; ++i) {
    NSString *s = [NSString stringWithFormat: @"&term_id_%d=%@", i, [[argList objectAtIndex: i] objectForKey: @"term_id"]];
    [body appendString: s];
  }
  printf("%s\n", [body UTF8String]);
  NSData *bodyData = [NSData dataWithBytes: [body UTF8String] length: [body length]];
  [theRequest setHTTPBody: bodyData];
  
  [theConnection performSynchronousRequest: theRequest];
    
  NSString *errorDesc = [self parseCMAServerXMLDocument: [theConnection receivedData]];
  if (errorDesc) {
    NSRunAlertPanel(@"Could not update language operator", errorDesc, @"OK", nil, nil);
    return NO;
  }
  
  return YES;
}

@end
