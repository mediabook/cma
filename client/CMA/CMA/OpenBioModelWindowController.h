//
//  OpenBioModelWindowController.h
//  CMA
//
//  Created by Scott Christley on 5/23/13.
//  Copyright (c) 2013 Scott Christley. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface OpenBioModelWindowController : NSWindowController
{
  IBOutlet NSTableView *biomodelListTable;
  IBOutlet NSButton *openButton;
  NSMutableArray *biomodelList;
  int modelID;
}

- (IBAction)cancelOpenBioModel:(id)sender;
- (IBAction)openBioModel:(id)sender;

- (int)loadBioModel;

@end
