//
//  LanguageOperatorController.m
//  CMA
//
//  Created by Scott Christley on 5/7/13.
//  Copyright (c) 2013 Scott Christley. All rights reserved.
//

#import "LanguageOperatorController.h"
#import "CMAServerConnection.h"
#import "LanguageOperator.h"

#import "internal.h"

@interface LanguageOperatorController ()
- (BOOL)processOperatorList: (NSData *)theData;
- (void)setIsEditing: (BOOL)aFlag;
@end

@implementation LanguageOperatorController

- (id)initWithWindow:(NSWindow *)window
{
    self = [super initWithWindow:window];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

- (void)windowDidLoad
{
  [super windowDidLoad];
    
  // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.

}

- (void)setupReduceToItems
{
  [reduceToButton removeAllItems];
  [reduceToButton addItemWithTitle: @"No Reduce"];
  
  int i;
  for (i = 0; i < [operatorList count]; ++i) {
    LanguageOperator *anOperator = [operatorList objectAtIndex: i];
    NSString *s = [NSString stringWithFormat: @"(%d) %@", [anOperator operatorID], [anOperator declaration]];
    [reduceToButton addItemWithTitle: s];
  }
}

- (IBAction)showWindow:(id)sender
{
  CMAServerConnection *theConnection = [[CMAServerConnection new] autorelease];
  NSMutableURLRequest *theRequest = [theConnection standardRequest];
  
  NSString *body = @"procedure=OPERATOR_LIST";
  NSData *bodyData = [NSData dataWithBytes: [body UTF8String] length: [body length]];
  [theRequest setHTTPBody: bodyData];
  
  [theConnection performSynchronousRequest: theRequest];
  
  if ([self processOperatorList: [theConnection receivedData]]) {
    [super showWindow: sender];
    [self clearFields: self];
  }
}

- (void)setIsEditing: (BOOL)aFlag
{
  isEditing = aFlag;
  if (aFlag) {
    [clearButton setEnabled: YES];
    [saveButton setEnabled: YES];
  } else {
    [clearButton setEnabled: YES];
    [saveButton setEnabled: NO];
  }
}

- (BOOL)processOperatorList: (NSData *)theData
{
  NSError *error;
  NSXMLDocument *xmlDoc = [[NSXMLDocument alloc] initWithData: theData options:0 error: &error];
  if (!xmlDoc) {
    NSString *errorDesc = [NSString stringWithFormat: @"Invalid XML received from CMAServer: %@\n%@",
                           [error localizedDescription], [[[NSString alloc] initWithData:theData encoding:NSUTF8StringEncoding] autorelease]];
    NSRunAlertPanel(@"Could not load operators.", errorDesc, @"OK", nil, nil);
    return NO;
  }

  NSString *errorDesc = checkXMLFail(xmlDoc);
  if (errorDesc) {
    NSRunAlertPanel(@"Could not load operators.", errorDesc, @"OK", nil, nil);
    return NO;
  }
  
  NSXMLNode *root = [[xmlDoc rootElement] childAtIndex: 0];
  if (operatorList) [operatorList release];
  operatorList = [NSMutableArray new];
  
  int i;
  for (i = 0; i < [root childCount]; ++i) {
    NSXMLNode *opItem = [root childAtIndex:i];
    
    LanguageOperator *anOperator = [[LanguageOperator new] autorelease];
    [operatorList addObject: anOperator];
    [anOperator parseCMAServerXML: opItem];
  }

  [operatorTable reloadData];

  return YES;
}

- (IBAction)clearFields:(id)sender
{
  [operatorTextField setStringValue: @""];
  [argumentTokenField setStringValue: @""];
  [operatorTable deselectAll: self];
  [self setupReduceToItems];
  [reduceToButton selectItemAtIndex: 0];
  [self setIsEditing: NO];
  [[self window] makeFirstResponder: operatorTextField];
}

- (IBAction)saveOperator:(id)sender
{
  NSString *name = [operatorTextField stringValue];
  if ([name length] == 0) {
    NSRunAlertPanel(@"Invalid operator", @"Operator name cannot be empty", @"OK", nil, nil);
    [[self window] makeFirstResponder: operatorTextField];
    return;
  }

  NSArray *args = [argumentTokenField objectValue];
  if ([args count] == 0) {
    NSRunAlertPanel(@"Invalid operator", @"Operator has no domain arguments", @"OK", nil, nil);
    [[self window] makeFirstResponder: argumentTokenField];
    return;    
  }
  
  int i;
  for (i = 0; i < [args count]; ++i) {
    printf("%s\n", [[[args objectAtIndex: i] objectForKey: @"name"] UTF8String]);
  }

  NSInteger idx = [operatorTable selectedRow];
  if (idx >= 0) {
    LanguageOperator *anOperator = [operatorList objectAtIndex: idx];
    NSInteger reduceIndex = [reduceToButton indexOfSelectedItem];
    if (reduceIndex != 0) {
      LanguageOperator *anOp = [operatorList objectAtIndex:reduceIndex - 1];
      reduceIndex = [anOp operatorID];
    }
    if ([anOperator updateLanguageOperator: name reduceTo: reduceIndex arguments: args]) {
      [operatorTable reloadData];
      [self clearFields: self];
    }
  } else {
    NSInteger reduceIndex = [reduceToButton indexOfSelectedItem];
    if (reduceIndex != 0) {
      LanguageOperator *anOp = [operatorList objectAtIndex:reduceIndex - 1];
      reduceIndex = [anOp operatorID];
    }
    LanguageOperator *anOperator = [LanguageOperator newLanguageOperator: name reduceTo: reduceIndex arguments:args];
    if (anOperator) {
      [operatorList addObject: anOperator];
      [operatorTable reloadData];
      [self clearFields: self];
    }
  }
}

- (IBAction)delete:(id)sender
{
  if ([[self window] firstResponder] == operatorTable) {
    NSInteger idx = [operatorTable selectedRow];
    printf("got delete: %ld\n", idx);
    if (idx >= 0) {
      LanguageOperator *anOperator = [operatorList objectAtIndex: idx];
      if ([anOperator deleteLanguageOperator]) {
        [operatorList removeObjectAtIndex: idx];
        [operatorTable reloadData];
        [self clearFields: self];
      }
    }
  }
}

- (IBAction)updateField:(id)sender
{
  printf("updateField:\n");
  if (sender == operatorTextField) {
    NSString *s = [sender stringValue];
    if ([s length] > 0) {
      [self setIsEditing: YES];
    }
  }

  if (sender == argumentTokenField) {
    NSArray *a = [sender objectValue];
    if ([a count] > 0) {
      [self setIsEditing: YES];
    }
  }

  if (sender == reduceToButton) {
    [self setIsEditing: YES];
  }
}

- (NSArray *)tokenField:(NSTokenField *)tokenFieldArg completionsForSubstring:(NSString *)substring
           indexOfToken:(NSInteger)tokenIndex indexOfSelectedItem:(NSInteger *)selectedIndex
{
  printf("tokenField\n");
	if ([substring length] < 3) return nil;
	
	NSArray *terms = searchOntologyTerms(substring, NO);
	if ([terms count] == 0) return nil;
	
	NSMutableArray *tokenList = [NSMutableArray array];
  int i;
	for (i = 0; i < [terms count]; ++i) {
		[tokenList addObject: [[terms objectAtIndex: i] objectForKey: @"name"]];
	}
  return tokenList;
}

- (NSArray *)tokenField:(NSTokenField *)tokenField shouldAddObjects:(NSArray *)tokens atIndex:(NSUInteger)index
{
  int i;
  printf("shouldAddObjects\n");

  // verify it is a complete valid ontology term
  BOOL goodTokens = YES;
  for (i = 0; i < [tokens count]; ++i) {
    id anObj = [tokens objectAtIndex: i];
    if (![anObj isKindOfClass: [NSDictionary class]]) goodTokens = NO;
    else printf("%s\n", [[anObj objectForKey: @"name"] UTF8String]);
  }
  if (!goodTokens) return nil;
  
  [self setIsEditing: YES];

  return tokens;
}

- (id)tokenField:(NSTokenField *)tokenField representedObjectForEditingString:(NSString *)editingString
{
  printf("representedObjectForEditingString\n");
  NSArray *a = searchOntologyTerms(editingString, YES);
  if ((!a) || ([a count] != 1)) {
    NSRunAlertPanel(@"Invalid term", @"'%@' is not a valid term.", @"OK", nil, nil, editingString);
    return nil;
  }

  return [a objectAtIndex: 0];
}

- (NSString *)tokenField:(NSTokenField *)tokenField displayStringForRepresentedObject:(id)representedObject
{
  printf("%s\n", [[representedObject objectForKey: @"name"] UTF8String]);
  return [representedObject objectForKey: @"name"];
}

//- (NSString *)tokenField:(NSTokenField *)tokenField editingStringForRepresentedObject:(id)representedObject
//{
  //printf("%s\n", [[representedObject objectForKey: @"name"] UTF8String]);
  //return [representedObject objectForKey: @"name"];
//}

//
//-----//-----//-----//-----//-----//-----//-----//-----
//
// Manage table data
//

- (BOOL)tableView:(NSTableView *)aTableView shouldSelectRow:(NSInteger)rowIndex
{
  NSInteger idx = [operatorTable selectedRow];
  printf("shouldSelectRow: %ld", idx);
  if (idx == rowIndex) return YES;
  
  if (isEditing) {
    NSRunAlertPanel(@"Currently editing an operator", @"Clear or Save your edits before editing another operator", @"OK", nil, nil);
    return NO;
  }

  return YES;
}

- (void)tableViewSelectionDidChange:(NSNotification *)aNotification
{
  NSInteger idx = [operatorTable selectedRow];
  if (idx < 0) return;
  
  if (!isEditing) {
    LanguageOperator *anOperator = [operatorList objectAtIndex: idx];
    [operatorTextField setStringValue: [anOperator declaration]];
    [argumentTokenField setObjectValue: [anOperator arguments]];
    if ([anOperator reduceID] != 0) {
      int i;
      for (i = 0; i < [operatorList count]; ++i) {
        LanguageOperator *anOp = [operatorList objectAtIndex: i];
        if ([anOperator reduceID] == [anOp operatorID]) {
          [reduceToButton selectItemAtIndex: i+1];
          break;
        }
      }
    } else [reduceToButton selectItemAtIndex: 0];
  }
}

- (NSInteger)numberOfRowsInTableView:(NSTableView *)tView
{
	return [operatorList count];
}

- (id)tableView:(NSTableView *)tView objectValueForTableColumn:(NSTableColumn *)tableColumn row:(int)row
{
	if ([[tableColumn identifier] isEqualToString: @"operatorID"]) {
		LanguageOperator *anOperator = [operatorList objectAtIndex: row];
		return [[NSNumber numberWithInt: [anOperator operatorID]] retain];
	}
	if ([[tableColumn identifier] isEqualToString: @"reduceID"]) {
		LanguageOperator *anOperator = [operatorList objectAtIndex: row];
		return [[NSNumber numberWithInt: [anOperator reduceID]] retain];
	}
	if ([[tableColumn identifier] isEqualToString: @"declaration"]) {
		LanguageOperator *anOperator = [operatorList objectAtIndex: row];
		return [anOperator declaration];
	}
	if ([[tableColumn identifier] isEqualToString: @"arguments"]) {
		LanguageOperator *anOperator = [operatorList objectAtIndex: row];
    NSArray *args = [anOperator arguments];
    NSMutableArray *anArray = [NSMutableArray new];
    int i;
    for (i = 0; i < [args count]; ++i) [anArray addObject: [[args objectAtIndex:i] objectForKey: @"name"]];
		return anArray;
	}
	
	return nil;
}

-(void)tableView:(NSTableView *)aTableView setObjectValue:anObject forTableColumn:(NSTableColumn *)aTableColumn row:(int)rowIndex
{
}

- (void)tableView:(NSTableView *)tableView didClickTableColumn:(NSTableColumn *)tableColumn
{
}

@end
