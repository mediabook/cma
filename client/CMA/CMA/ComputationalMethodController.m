//
//  ComputationalMethodController.m
//  CMA
//
//  Created by Scott Christley on 5/8/13.
//  Copyright (c) 2013 Scott Christley. All rights reserved.
//

#import "ComputationalMethodController.h"

@interface ComputationalMethodController ()

@end

@implementation ComputationalMethodController

- (id)initWithWindow:(NSWindow *)window
{
    self = [super initWithWindow:window];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

- (void)windowDidLoad
{
    [super windowDidLoad];
    
    // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
}

@end
