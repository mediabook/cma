//
//  LanguageOperatorController.h
//  CMA
//
//  Created by Scott Christley on 5/7/13.
//  Copyright (c) 2013 Scott Christley. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface LanguageOperatorController : NSWindowController
{
  IBOutlet NSTextField *operatorTextField;
  IBOutlet NSPopUpButton *reduceToButton;
  IBOutlet NSTokenField *argumentTokenField;
  IBOutlet NSButton *saveButton;
  IBOutlet NSButton *clearButton;
  IBOutlet NSTableView *operatorTable;

  NSMutableArray *operatorList;

  BOOL isEditing;
}

- (IBAction)clearFields:(id)sender;
- (IBAction)saveOperator:(id)sender;
- (IBAction)updateField:(id)sender;

@end
