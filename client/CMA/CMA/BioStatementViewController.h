//
//  BioStatementViewController.h
//  CMA
//
//  Created by Scott Christley on 5/6/13.
//  Copyright (c) 2013 Scott Christley. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "BioModel.h"

@interface BioStatementViewController : NSViewController
{
  BioModel *theBioModel;
  id ontologyTerm;

  IBOutlet NSTextField *biostatementField;
  IBOutlet NSTextField *biostatementDefinitionField;
  IBOutlet NSTokenField *biostatementDefinitionToken;
  IBOutlet NSTextField *biostatementDefinitionLabel;
  IBOutlet NSPopUpButton *biostatementTypeButton;
  IBOutlet NSTableView *biostatementTable;
  IBOutlet NSTableColumn *biostatementTableColumn;
}

- (IBAction)addBioStatement:(id)sender;
- (IBAction)selectBioStatementType:(id)sender;

@end
