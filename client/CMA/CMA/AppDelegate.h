//
//  AppDelegate.h
//  CMA
//
//  Created by Scott Christley on 5/3/13.
//  Copyright (c) 2013 Scott Christley. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "BioModel.h"

#import "OntologyController.h"
#import "LanguageOperatorController.h"
#import "ModelSpecificationController.h"
#import "SimulationCodeWindowController.h"

#import "OpenBioModelWindowController.h"
#import "BioModelInfoViewController.h"
#import "BioStatementViewController.h"
#import "BioModelSpecificationViewController.h"
#import "BioModelDataViewController.h"
#import "BioModelSimulationViewController.h"
#import "BioModelAnalysisViewController.h"

// Notifications
extern NSString *BioModelWillRelinquishViewNotification;
extern NSString *BioModelDidDisplayViewNotification;

@interface AppDelegate : NSObject <NSApplicationDelegate>

{
  NSMutableDictionary *workbench;
  NSMutableDictionary *modelSpecificationItem;
  BioModel *theBioModel;
  
  // open biomodel panel
  OpenBioModelWindowController *openBioModelController;
  
  // BioModel
  IBOutlet NSWindow *biomodelWindow;
  IBOutlet NSTextField *bioModelHeaderTextField;
  IBOutlet NSTextField *bioModelStatusTextField;
  IBOutlet NSButton *performErrorCheckButton;
  IBOutlet NSOutlineView *biomodelOutlineView;
  IBOutlet NSMenuItem *deleteMenuItem;
  IBOutlet NSMenuItem *generateSpecificationMenuItem;
  IBOutlet NSMenuItem *generateSimulationCodeMenuItem;
  
  // BioModel Details views
  IBOutlet NSView *detailView;
  id currentDetailView;
  // Info detail view
  BioModelInfoViewController *infoViewController;
  
  // Statements detail view
  BioStatementViewController *biostatementsViewController;
  
  // Specifications detail view
  BioModelSpecificationViewController *specificationsViewController;
  
  // Data detail view
  BioModelDataViewController *dataViewController;
  
  // Simulations detail view
  BioModelSimulationViewController *simulationsViewController;
  
  // Analyses detail view
  BioModelAnalysisViewController *analysesViewController;
  
  // Data Management
  //

  // Simulation Management
  //
  ModelSpecificationController *msController;
  SimulationCodeWindowController *codeController;

  // KnowledgeBase
  //
  // ontologies
  OntologyController *ontologyController;
  // language operators
  LanguageOperatorController *loController;

}

// BioModel
- (IBAction)newDocument:(id)sender;
- (IBAction)newDocumentVersion:(id)sender;
- (IBAction)openDocument:(id)sender;
- (IBAction)errorCheckBioModel:(id)sender;

// Data Management

// Simulation Management
- (IBAction)showGenerateSpecificationWindow:(id)sender;
- (IBAction)showGenerateSimulationCodeWindow:(id)sender;

// KnowledgeBase
- (IBAction)showOntologyBrowserWindow:(id)sender;
- (IBAction)showLanguageOperatorWindow:(id)sender;

@end
