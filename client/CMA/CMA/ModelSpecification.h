//
//  ModelSpecification.h
//  CMA
//
//  Created by Scott Christley on 5/16/13.
//  Copyright (c) 2013 Scott Christley. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ModelSpecification : NSObject
{
  int specID;
  NSString *specName;
  int modelID;
  int mappingID;
  NSDate *specDate;
  NSString *specText;
}

- (void)parseCMAServerXML: (NSXMLNode *)aNode;
- (NSString *)parseCMAServerXMLDocument: (NSData *)theData;

- (int)specID;
- (NSString *)specName;
- (int)modelID;
- (int)mappingID;
- (NSDate *)specDate;
- (NSString *)specText;

- (void)loadModelSpecificationText;

@end
