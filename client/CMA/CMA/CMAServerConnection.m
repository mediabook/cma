//
//  CMAServerConnection.m
//  CMA
//
//  Created by Scott Christley on 5/3/13.
//  Copyright (c) 2013 Scott Christley. All rights reserved.
//

#import "CMAServerConnection.h"

@implementation CMAServerConnection

- (void)dealloc
{
  if (receivedData) [receivedData release];
  
  [super dealloc];
}

- (NSMutableURLRequest *)standardRequest
{
  NSURL *theURL = [NSURL URLWithString: @"http://sur-gpu01.bsd.uchicago.edu/newcma/php/CMAServer.php"];
  NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL: theURL];
  
  [theRequest setHTTPMethod: @"POST"];
  [theRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];

  return theRequest;
}

- (void)performRequest: (NSURLRequest *)theRequest
{
  NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
  if (theConnection) {
    // Create the NSMutableData to hold the received data.
    // receivedData is an instance variable declared elsewhere.
    receivedData = [[NSMutableData data] retain];
    isDone = NO;
  } else {
    // Inform the user that the connection failed.
    printf("ERROR: Could not create URL connection to BioPortal.\n");
    isDone = YES;
  }
}

- (void)performSynchronousRequest: (NSURLRequest *)theRequest
{
  [self performRequest: theRequest];
  
  while (![self isDone]) {
    [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
  }
}

- (BOOL)isDone { return isDone; }
- (NSData *)receivedData { return receivedData; }

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
  // This method is called when the server has determined that it
  // has enough information to create the NSURLResponse.
  
  // It can be called multiple times, for example in the case of a
  // redirect, so each time we reset the data.
  
  // zero out the data.
  [receivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
  // Append the new data to receivedData.
  // receivedData is an instance variable declared elsewhere.
  [receivedData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
  // release the connection, and the data object
  [connection release];
  [receivedData release];
  receivedData = nil;
  isDone = YES;
  
  // inform the user
  NSLog(@"Connection failed! Error - %@ %@",
        [error localizedDescription],
        [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
  // release the connection, and indicate done
  [connection release];
  isDone = YES;
}

@end
