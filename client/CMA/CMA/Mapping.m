//
//  Mapping.m
//  CMA
//
//  Created by Scott Christley on 5/14/13.
//  Copyright (c) 2013 Scott Christley. All rights reserved.
//

#import "Mapping.h"

@implementation Mapping

- (void)dealloc
{
  if (mappingName) [mappingName release];
  
  [super dealloc];
}
- (int)mappingID { return  mappingID; }
- (NSString *)mappingName { return mappingName; }

- (void)parseCMAServerXML: (NSXMLNode *)aNode;
{
  int i;
  for (i = 0; i < [aNode childCount]; ++i) {
    NSXMLNode *item = [aNode childAtIndex:i];
    
    if ([[item name] isEqualToString: @"mapping_id"]) {
      mappingID = [[item stringValue] intValue];
    }
    if ([[item name] isEqualToString: @"mapping_name"]) {
      mappingName = [[NSString alloc] initWithString: [item stringValue]];
    }
  }

}

@end
