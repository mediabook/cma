//
//  BioModelInfoViewController.m
//  CMA
//
//  Created by Scott Christley on 5/6/13.
//  Copyright (c) 2013 Scott Christley. All rights reserved.
//

#import "BioModelInfoViewController.h"
#import "AppDelegate.h"
#import "CMAServerConnection.h"

#import "internal.h"

@implementation BioModelInfoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
      // register for notifications
      [[NSNotificationCenter defaultCenter] addObserver:self
                                               selector:@selector(bioModelWillLoad:)
                                                   name:BioModelWillLoadNotification object:nil];
      [[NSNotificationCenter defaultCenter] addObserver:self
                                               selector:@selector(bioModelDidLoad:)
                                                   name:BioModelDidLoadNotification object:nil];
      [[NSNotificationCenter defaultCenter] addObserver:self
                                               selector:@selector(bioModelWillSave:)
                                                   name:BioModelWillSaveNotification object:nil];
      [[NSNotificationCenter defaultCenter] addObserver:self
                                               selector:@selector(bioModelDidSave:)
                                                   name:BioModelDidSaveNotification object:nil];
      [[NSNotificationCenter defaultCenter] addObserver:self
                                               selector:@selector(bioModelWillRelinquish:)
                                                   name:BioModelWillRelinquishViewNotification object:nil];
      [[NSNotificationCenter defaultCenter] addObserver:self
                                               selector:@selector(bioModelDidDisplay:)
                                                   name:BioModelDidDisplayViewNotification object:nil];
    }
    
    return self;
}

- (void)setBioModel:(id)aModel
{
  if (theBioModel) [theBioModel release];
  theBioModel = [aModel retain];
}

- (void)updateViewContents
{
  [bioModelNameTextField setStringValue: [theBioModel modelName]];
  [bioModelVersionTextField setStringValue: [theBioModel modelVersion]];
  if ([theBioModel modelDescription]) [bioModelDescriptionTextField setStringValue: [theBioModel modelDescription]];
  else [bioModelDescriptionTextField setStringValue: @""];
  if ([theBioModel modelOwnerName]) [bioModelOwnerTextField setStringValue: [theBioModel modelOwnerName]];
  else [bioModelOwnerTextField setStringValue: @""];
  [bioModelDateTextField setStringValue: [NSDateFormatter localizedStringFromDate: [theBioModel modelDate]
                                                                        dateStyle: NSDateFormatterMediumStyle
                                                                        timeStyle: NSDateFormatterMediumStyle]];
  if ([theBioModel modelNotes]) [bioModelNotesTextField setStringValue: [theBioModel modelNotes]];
  else [bioModelNotesTextField setStringValue: @""];
}

- (void)makeFirstResponderForWindow: (NSWindow *)aWindow
{
  [aWindow makeFirstResponder: bioModelNameTextField];
}

- (IBAction)updateField:(id)sender
{
  if (sender == bioModelNameTextField) {
    if (![theBioModel updateBioModelName: [bioModelNameTextField stringValue]]) {
      [bioModelDateTextField setStringValue: [theBioModel modelName]];
    }
    return;
  }

  if (sender == bioModelVersionTextField) {
    if (![theBioModel updateBioModelVersion: [bioModelVersionTextField stringValue]]) {
      [bioModelVersionTextField setStringValue: [theBioModel modelVersion]];
    }
    return;
  }

  if (sender == bioModelDescriptionTextField) {
    if (![theBioModel updateBioModelDescription: [bioModelDescriptionTextField stringValue]]) {
      [bioModelDescriptionTextField setStringValue: [theBioModel modelDescription]];
    }
    return;
  }

  if (sender == bioModelNotesTextField) {
    if (![theBioModel updateBioModelNotes: [bioModelNotesTextField stringValue]]) {
      [bioModelNotesTextField setStringValue: [theBioModel modelNotes]];
    }
    return;
  }
}

- (void)allowEdit:(BOOL)aFlag
{
  [bioModelNameTextField setEditable: aFlag];
  [bioModelVersionTextField setEditable: aFlag];
  [bioModelDescriptionTextField setEditable: aFlag];
}

//
// Notifications
//
- (void)bioModelWillLoad:(NSNotification *)notification
{
  [self setBioModel: nil];
  [self allowEdit: NO];
}

- (void)bioModelDidLoad:(NSNotification *)notification
{
  [self setBioModel: [notification object]];
  if (!theBioModel) return;

  [self updateViewContents];
  [self allowEdit: YES];
}

- (void)bioModelWillSave:(NSNotification *)notification
{
}

- (void)bioModelDidSave:(NSNotification *)notification
{
}

- (void)bioModelWillRelinquish:(NSNotification *)notification
{
}

- (void)bioModelDidDisplay:(NSNotification *)notification
{
  if ([notification object] == self) {
    if (!theBioModel) {
      [self allowEdit: NO];

      // if no biomodel set, set it then update the view contents
      NSDictionary *d = [notification userInfo];
      [self setBioModel: [d objectForKey: @"biomodel"]];
      if (!theBioModel) return;

      [self updateViewContents];
      [self allowEdit: YES];
    }

    //[bioModelNameTextField setStringValue: theBioModel->modelName];
  }
}

@end
