//
//  AppDelegate.m
//  CMA
//
//  Created by Scott Christley on 5/3/13.
//  Copyright (c) 2013 Scott Christley. All rights reserved.
//

#import "AppDelegate.h"
#import "CMAServerConnection.h"

#import "internal.h"

enum	// detail views
{
	bioModelNoneTag = 0,
	bioModelInfoTag,
	bioModelStatementsTag,
	bioModelSpecificationsTag,
	bioModelDataTag,
  bioModelSimulationsTag,
  bioModelAnalysesTag
};

// Notifications
NSString *BioModelWillRelinquishViewNotification = @"BioModelWillRelinquishViewNotification";
NSString *BioModelDidDisplayViewNotification = @"BioModelDidDisplayViewNotification";

@interface AppDelegate ()
- (void)updateSpecificationsItem;
@end

@implementation AppDelegate

- (id)init
{
  self = [super init];
  if (self) {
		workbench = [NSMutableDictionary new];
		NSMutableArray *a = [NSMutableArray array];
		[workbench setObject: a forKey: @"managedObjects"];
		NSMutableDictionary *d = [NSMutableDictionary dictionary];
		[d setObject: @"Info" forKey: @"name"];
		[a addObject: d];
    
		d = [NSMutableDictionary dictionary];
		[d setObject: @"Statements" forKey: @"name"];
		[a addObject: d];
    
		modelSpecificationItem = [NSMutableDictionary dictionary];
		[modelSpecificationItem setObject: @"Specifications" forKey: @"name"];
		[a addObject: modelSpecificationItem];
    
		d = [NSMutableDictionary dictionary];
		[d setObject: @"Data" forKey: @"name"];
		[a addObject: d];
    
		d = [NSMutableDictionary dictionary];
		[d setObject: @"Simulations" forKey: @"name"];
		[a addObject: d];
    
		d = [NSMutableDictionary dictionary];
		[d setObject: @"Analyses" forKey: @"name"];
		[a addObject: d];
		
		a = [NSMutableArray array];
		[d setObject: a forKey: @"children"];
		d = [NSMutableDictionary dictionary];
		[d setObject: @"subgroup" forKey: @"name"];
		[a addObject: d];

    // register for notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(bioModelWasModified:)
                                                 name:BioModelWasModifiedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(bioModelNeedsErrorChecking:)
                                                 name:BioModelNeedsErrorCheckingNotification object:nil];
}
  
  return self;
}

- (void)dealloc
{
    [super dealloc];
}

#if 0
#pragma mark == MANAGE VIEWS ==
#endif
//
//-----//-----//-----//-----//-----//-----//-----//-----
//

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
  // Insert code here to initialize your application
  [generateSpecificationMenuItem setTarget: nil];
  [generateSimulationCodeMenuItem setTarget: nil];
}

- (void)awakeFromNib
{
}

- (void)displayNeedCheckStatus:(BOOL)aFlag withMessage:(NSString *)aMessage
{
  [bioModelStatusTextField setStringValue: aMessage];
  [performErrorCheckButton setEnabled: aFlag];
  if (aFlag) {
    [generateSpecificationMenuItem setTarget: nil];
    [generateSpecificationMenuItem setAction: nil];
    [generateSimulationCodeMenuItem setTarget: nil];
    [generateSimulationCodeMenuItem setAction: nil];
  } else {
    [generateSpecificationMenuItem setTarget: self];
    [generateSpecificationMenuItem setAction: @selector(showGenerateSpecificationWindow:)];
    [generateSimulationCodeMenuItem setTarget: self];
    [generateSimulationCodeMenuItem setAction: @selector(showGenerateSimulationCodeWindow:)];
  }
  [generateSpecificationMenuItem setEnabled: !aFlag];
  [generateSimulationCodeMenuItem setEnabled: !aFlag];
}

- (void)swapDetailView:(int)aTag
{

  if ([currentDetailView view] != nil) {
    [[NSNotificationCenter defaultCenter] postNotificationName: BioModelWillRelinquishViewNotification object: currentDetailView userInfo: nil];
		[[currentDetailView view] removeFromSuperview];	// remove the current view
  }

  switch(aTag) {
    case bioModelInfoTag:
      if (!infoViewController) infoViewController = [[BioModelInfoViewController alloc] initWithNibName:@"BioModelInfo" bundle:nil];
			if (infoViewController != nil) {
				currentDetailView = infoViewController;
			}
      break;

    case bioModelStatementsTag:
      if (!biostatementsViewController) biostatementsViewController = [[BioStatementViewController alloc] initWithNibName:@"BioModelStatements" bundle:nil];
			if (biostatementsViewController != nil) {
				currentDetailView = biostatementsViewController;
			}
      break;

    case bioModelSpecificationsTag:
      if (!specificationsViewController) specificationsViewController = [[BioModelSpecificationViewController alloc] initWithNibName:@"BioModelSpecifications" bundle:nil];
			if (specificationsViewController != nil) {
				currentDetailView = specificationsViewController;
			}
      break;

    case bioModelDataTag:
      if (!dataViewController) dataViewController = [[BioModelDataViewController alloc] initWithNibName:@"BioModelData" bundle:nil];
			if (dataViewController != nil) {
				currentDetailView = dataViewController;
			}
      break;

    case bioModelSimulationsTag:
      if (!simulationsViewController) simulationsViewController = [[BioModelSimulationViewController alloc] initWithNibName:@"BioModelSimulations" bundle:nil];
			if (simulationsViewController != nil) {
				currentDetailView = simulationsViewController;
			}
      break;

    case bioModelAnalysesTag:
      if (!analysesViewController) analysesViewController = [[BioModelAnalysisViewController alloc] initWithNibName:@"BioModelAnalyses" bundle:nil];
			if (analysesViewController != nil) {
				currentDetailView = analysesViewController;
			}
      break;
  }

  // embed the current view to our host view
  [deleteMenuItem setTarget: currentDetailView];
	[detailView addSubview: [currentDetailView view]];
	
	// make sure we automatically resize the controller's view to the current window size
	[[currentDetailView view] setFrame: [detailView bounds]];

  if (theBioModel) {
    NSUInteger selectedRow = [[biomodelOutlineView selectedRowIndexes] firstIndex];
    id item = [biomodelOutlineView itemAtRow:selectedRow];
    
    if (item)
      [[NSNotificationCenter defaultCenter] postNotificationName: BioModelDidDisplayViewNotification object: currentDetailView
                                                        userInfo: [NSDictionary dictionaryWithObjectsAndKeys: theBioModel, @"biomodel", item, @"outlineItem", nil]];
    else
      [[NSNotificationCenter defaultCenter] postNotificationName: BioModelDidDisplayViewNotification object: currentDetailView
                                                        userInfo: [NSDictionary dictionaryWithObjectsAndKeys: theBioModel, @"biomodel", nil]];
  } else
    [[NSNotificationCenter defaultCenter] postNotificationName: BioModelDidDisplayViewNotification object: currentDetailView
                                                      userInfo: nil];

}

//
//-----//-----//-----//-----//-----//-----//-----//-----
//
#if 0
#pragma mark == MANAGE BIOMODEL ==
#endif

- (NSString *)finishLoad:(NSData *)theData
{
  theBioModel = [BioModel new];
  NSString *errorDesc = [theBioModel parseCMAServerXMLDocument: theData];
  if (errorDesc) {
    [theBioModel release];
    theBioModel = nil;
    return errorDesc;
  }
  
  [[NSNotificationCenter defaultCenter] postNotificationName: BioModelDidLoadNotification object: theBioModel userInfo: nil];
  
  [self updateSpecificationsItem];
  
  // select Info item in outline view
  NSIndexSet *idx = [NSIndexSet indexSetWithIndex: [biomodelOutlineView rowForItem: [[workbench objectForKey: @"managedObjects"] objectAtIndex: 0]]];
  [biomodelOutlineView selectRowIndexes: idx byExtendingSelection: NO];
  
  [self swapDetailView: bioModelInfoTag];
  [infoViewController makeFirstResponderForWindow: biomodelWindow];
  [self updateBioModelDisplay];
  
  return nil;
}

- (IBAction)newDocument:(id)sender
{
  CMAServerConnection *theConnection = [[CMAServerConnection new] autorelease];
  NSMutableURLRequest *theRequest = [theConnection standardRequest];

  NSString *body = @"procedure=NEW_BIOMODEL";
  NSData *bodyData = [NSData dataWithBytes: [body UTF8String] length: [body length]];
  [theRequest setHTTPBody: bodyData];
  
  [theConnection performSynchronousRequest: theRequest];
  
  NSString *errorDesc = [self finishLoad: [theConnection receivedData]];
  if (errorDesc) {
    NSRunAlertPanel(@"Could not create new BioModel", errorDesc, @"OK", nil, nil);
  }
}

- (IBAction)newDocumentVersion:(id)sender
{
  
}

- (void)openDocument:(id)sender
{
  if (!openBioModelController) openBioModelController = [[OpenBioModelWindowController alloc] initWithWindowNibName:@"OpenBioModelPanel"];
  int anID = [openBioModelController loadBioModel];
  if (anID >= 0) [self loadBioModel: anID];
}

- (void)loadBioModel: (int)anID
{
  [[NSNotificationCenter defaultCenter] postNotificationName: BioModelWillLoadNotification object: self userInfo: nil];

  CMAServerConnection *theConnection = [[CMAServerConnection new] autorelease];
  NSMutableURLRequest *theRequest = [theConnection standardRequest];

  NSString *body = [NSString stringWithFormat: @"procedure=OPEN_BIOMODEL&model_id=%d", anID];
  NSData *bodyData = [NSData dataWithBytes: [body UTF8String] length: [body length]];
  [theRequest setHTTPBody: bodyData];
  
  [theConnection performSynchronousRequest: theRequest];

  [self finishLoad: [theConnection receivedData]];
}

- (void)updateSpecificationsItem
{
  NSMutableArray *a = [NSMutableArray array];
  [modelSpecificationItem setObject: a forKey: @"children"];

  NSArray *specs = [theBioModel specifications];
  int i;
  for (i = 0; i < [specs count]; ++i) {
    ModelSpecification *aSpec = [specs objectAtIndex: i];
    NSMutableDictionary *d = [NSMutableDictionary dictionary];
    [a addObject: d];
    [d setObject: [aSpec specName] forKey: @"name"];
    [d setObject: aSpec forKey: @"model_specification"];
  }

  [biomodelOutlineView reloadItem: modelSpecificationItem];
}

- (void)updateBioModelDisplay
{
  [bioModelHeaderTextField setStringValue: theBioModel->modelName];
  
  if (theBioModel->isChecked) [self displayNeedCheckStatus: NO withMessage: @"BioModel has no errors."];
  else [self displayNeedCheckStatus: YES withMessage: @"BioModel needs to be checked for errors."];
	//[bioModelNameTextField setStringValue: theBioModel->modelName];
  
	//[biostatementTable reloadData];
}

- (IBAction)addGroup:(id)sender
{
#if 0
	var selectedRow = [[biomodelOutlineView selectedRowIndexes] firstIndex];
  var item = [biomodelOutlineView itemAtRow:selectedRow];
  
	var c = [item objectForKey: @"children"];
	if (!c) {
		c = [CPArray array];
		[item setObject: c forKey: @"children"];
	}
	
	var d = [CPDictionary dictionary];
	[d setObject: @"New Group" forKey: @"name"];
	[c addObject: d];
	
	[biomodelOutlineView reloadData];
#endif
}

- (void)bioModelWasModified: (NSNotification *)aNotification
{
  NSDictionary *d = [aNotification userInfo];
  printf("%s\n", [[d description] UTF8String]);

  if ([[d objectForKey: @"field"] isEqualToString: @"model_name"])
    [self updateBioModelDisplay];

  if ([[d objectForKey: @"field"] isEqualToString: @"model_specification"])
    [self updateSpecificationsItem];
}

- (void)bioModelNeedsErrorChecking: (NSNotification *)aNotification
{
  [self displayNeedCheckStatus: YES withMessage:@"BioModel needs to be checked for errors."];
}

- (IBAction)errorCheckBioModel:(id)sender
{
  if ([theBioModel errorCheckBioModel]) {
    // no errors
    [self displayNeedCheckStatus: NO withMessage: @"BioModel has no errors."];
    [theBioModel updateChecked: YES];
  } else {
    [self displayNeedCheckStatus: YES withMessage: @"BioModel has errors."];
    [theBioModel updateChecked: NO];
  }
}

//
//-----//-----//-----//-----//-----//-----//-----//-----
//
#if 0
#pragma mark == MANAGE DATA ==
#endif

//
//-----//-----//-----//-----//-----//-----//-----//-----
//
#if 0
#pragma mark == MANAGE SIMULATIONS ==
#endif

- (IBAction)showGenerateSpecificationWindow:(id)sender
{
	if (!msController) {
		msController = [[ModelSpecificationController alloc] initWithWindowNibName: @"GenerateSpecification"];
	}
  [msController setBioModel: theBioModel];
	[msController showWindow: self];
}

- (IBAction)showGenerateSimulationCodeWindow:(id)sender
{
	if (!codeController) {
		codeController = [[SimulationCodeWindowController alloc] initWithWindowNibName: @"GenerateSimulationCode"];
	}
  [codeController setBioModel: theBioModel];
	[codeController showWindow: self];  
}

//
//-----//-----//-----//-----//-----//-----//-----//-----
//
#if 0
#pragma mark == MANAGE KNOWLEDGEBASE ==
#endif

- (IBAction)showOntologyBrowserWindow:(id)sender
{
	if (!ontologyController) {
		ontologyController = [[OntologyController alloc] initWithWindowNibName: @"Ontologies"];
	}
	[ontologyController showWindow: self];
}

- (IBAction)showLanguageOperatorWindow:(id)sender
{
	if (!loController) {
		loController = [[LanguageOperatorController alloc] initWithWindowNibName: @"LanguageOperators"];
	}
	[loController showWindow: self];
}

//
//-----//-----//-----//-----//-----//-----//-----//-----
//
#if 0
#pragma mark == BIOMODEL OUTLINE VIEW ==
#endif

- (NSInteger)outlineView:(NSOutlineView *)outlineView numberOfChildrenOfItem:(id)item
{
  //    return (item == nil) [[workbench objectForKey: @"managedObjects"] count] : 0;
  
  if (item == nil)
    return [[workbench objectForKey: @"managedObjects"] count];
  else {
  	NSArray *c = [item objectForKey: @"children"];
  	if (c) return [c count];
  	else return 0;
  }
}

- (BOOL)outlineView:(NSOutlineView *)outlineView isItemExpandable:(id)item
{
	if (item == nil) return YES;
	
	NSArray *c = [item objectForKey: @"children"];
	if (!c) return NO;
	if ([c count] == 0) return NO;
  
	return YES;
}

- (id)outlineView:(NSOutlineView *)outlineView
            child:(int)index
           ofItem:(id)item
{
  if (item == nil) {
		return [[workbench objectForKey: @"managedObjects"] objectAtIndex: index];
	} else {
		return [[item objectForKey: @"children"] objectAtIndex: index];
	}
}

- (id)outlineView:(NSOutlineView *)outlineView objectValueForTableColumn:(NSTableColumn *)tableColumn
           byItem:(id)item
{
	if ([[tableColumn identifier] isEqualToString: @"name"])
		return [item objectForKey: @"name"];
	else
		return nil;
}

- (BOOL)outlineView:(NSOutlineView *)outlineView shouldSelectItem:(id)item
{
  if ([[item objectForKey: @"name"] isEqualToString: @"Specifications"]) {
    return NO;
  }

  return YES;
}

- (void)outlineViewSelectionDidChange:(NSNotification *)aNotification
{
	id obj = [aNotification object];
	if (obj == biomodelOutlineView) {
    NSUInteger selectedRow = [[biomodelOutlineView selectedRowIndexes] firstIndex];
    id item = [biomodelOutlineView itemAtRow:selectedRow];

    id rootItem = nil;
    id parent = [biomodelOutlineView parentForItem: item];
    while (parent) {
      rootItem = parent;
      parent = [biomodelOutlineView parentForItem: parent];
    }
    if (!rootItem) rootItem = item;

    if ([[item objectForKey: @"name"] isEqualToString: @"Info"]) {
    	[self swapDetailView: bioModelInfoTag];
   	}
    
    if ([[item objectForKey: @"name"] isEqualToString: @"Statements"]) {
    	[self swapDetailView: bioModelStatementsTag];
   	}
    
    if ([[rootItem objectForKey: @"name"] isEqualToString: @"Specifications"]) {
    	[self swapDetailView: bioModelSpecificationsTag];
   	}
    
    if ([[item objectForKey: @"name"] isEqualToString: @"Data"]) {
    	[self swapDetailView: bioModelDataTag];
   	}
    
    if ([[item objectForKey: @"name"] isEqualToString: @"Simulations"]) {
    	[self swapDetailView: bioModelSimulationsTag];
   	}
    
    if ([[item objectForKey: @"name"] isEqualToString: @"Analyses"]) {
    	[self swapDetailView: bioModelAnalysesTag];
   	}
    
    //[[theWindow contentView] setBackgroundColor:[CPColor blackColor]];
	}
}

@end
