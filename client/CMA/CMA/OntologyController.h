//
//  OntologyController.h
//  CMA
//
//  Created by Scott Christley on 5/6/13.
//  Copyright (c) 2013 Scott Christley. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <WebKit/WebKit.h>

@interface OntologyController : NSWindowController
{
  IBOutlet NSOutlineView *ontologyTable;
  IBOutlet NSTextField *termID;
  IBOutlet NSTextField *bioportalID;
  IBOutlet NSTextField *nameField;
  IBOutlet NSTextField *descriptionField;
  IBOutlet WebView *mathField;
  
  NSMutableArray *ontologyList;

  IBOutlet NSSearchField *searchField;
  int checkFindTyping;
  int checkFindTypingLength;
}
- (IBAction)openBioPortal:(id)sender;

@end
