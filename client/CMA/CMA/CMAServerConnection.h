//
//  CMAServerConnection.h
//  CMA
//
//  Created by Scott Christley on 5/3/13.
//  Copyright (c) 2013 Scott Christley. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CMAServerConnection : NSObject
{
  BOOL isDone;
  NSMutableData *receivedData;
}

- (NSMutableURLRequest *)standardRequest;
- (void)performRequest: (NSURLRequest *)theRequest;
- (void)performSynchronousRequest: (NSURLRequest *)theRequest;
- (BOOL)isDone;
- (NSData *)receivedData;

@end
