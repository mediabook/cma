//
//  OpenBioModelWindowController.m
//  CMA
//
//  Created by Scott Christley on 5/23/13.
//  Copyright (c) 2013 Scott Christley. All rights reserved.
//

#import "OpenBioModelWindowController.h"
#import "CMAServerConnection.h"

#import "internal.h"

@interface OpenBioModelWindowController ()

@end

@implementation OpenBioModelWindowController

- (id)initWithWindow:(NSWindow *)window
{
    self = [super initWithWindow:window];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

- (void)windowDidLoad
{
    [super windowDidLoad];
    
    // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
}

- (IBAction)cancelOpenBioModel:(id)sender
{
  [[NSApplication sharedApplication] stopModal];
  [[self window] close];
  modelID = -1;
}

- (IBAction)openBioModel:(id)sender
{
  [[NSApplication sharedApplication] stopModal];
  
	NSInteger row = [biomodelListTable selectedRow];
	id aModel = [biomodelList objectAtIndex: row];
	modelID = [[aModel objectForKey: @"modelID"] intValue];
  
  [[self window] close];
}

//
//-----//-----//-----//-----//-----//-----//-----//-----
//
// Process XML from CMA server requests
//
- (void)processBioModelList:(NSData *)theData
{
  NSError *error;
  NSXMLDocument *xmlDoc = [[NSXMLDocument alloc] initWithData: theData options:0 error: &error];
  NSXMLElement *root = [xmlDoc rootElement];
  //printf("%s\n", [[root XMLString] UTF8String]);
  
  if (biomodelList) [biomodelList release];
  biomodelList = [NSMutableArray new];
  
  NSArray *a = [root elementsForName: @"biomodel"];
  //printf("%s\n", [[a description] UTF8String]);
  int i, j;
  for (i = 0; i < [a count]; ++i) {
    NSXMLNode *n = [a objectAtIndex: i];
    NSMutableDictionary *aModel = [NSMutableDictionary dictionary];
    [biomodelList addObject: aModel];
    
    for (j = 0; j < [n childCount]; ++j) {
      NSXMLNode *item = [n childAtIndex:j];
      
      if ([[item name] isEqualToString: @"model_id"]) {
        [aModel setObject: [item stringValue] forKey: @"modelID"];
      }
      if ([[item name] isEqualToString: @"model_name"]) {
        [aModel setObject: [[item stringValue] stringWithReplaceURLPercentEscape] forKey: @"modelName"];
      }
      
      //printf("%s: %s", [[item name] UTF8String], [[item stringValue] UTF8String]);
    }
  }
}

- (int)loadBioModel
{
  CMAServerConnection *theConnection = [[CMAServerConnection new] autorelease];
  NSMutableURLRequest *theRequest = [theConnection standardRequest];
  
  NSString *body = @"procedure=BIOMODEL_LIST";
  NSData *bodyData = [NSData dataWithBytes: [body UTF8String] length: [body length]];
  [theRequest setHTTPBody: bodyData];
  
  [theConnection performSynchronousRequest: theRequest];
  
  [self processBioModelList: [theConnection receivedData]];

  [self showWindow: self];
  [[NSApplication sharedApplication] runModalForWindow: [self window]];

  return modelID;
}

//
//-----//-----//-----//-----//-----//-----//-----//-----
//
// Manage table data
//

//
// NSTableView delegate protocol
//
- (void)tableViewSelectionDidChange:(NSNotification *)aNotification
{
	id obj = [aNotification object];
	if (obj == biomodelListTable) {
    printf("%ld\n", [biomodelListTable selectedRow]);
		if ([biomodelListTable selectedRow] < 0) {
			[openButton setEnabled: NO];
		} else {
			[openButton setEnabled: YES];
		}
	}
}

- (NSUInteger)numberOfRowsInTableView:(NSTableView *)tView
{
	if (tView == biomodelListTable) {
		return [biomodelList count];
	}
  
	return 0;
}

- (id)tableView:(NSTableView *)tView objectValueForTableColumn:(NSTableColumn *)tableColumn row:(int)row
{
	if (tView == biomodelListTable) {
	  id aModel = [biomodelList objectAtIndex: row];
		return [aModel objectForKey: [tableColumn identifier]];
	}
  
  return nil;
}

-(void)tableView:(NSTableView *)aTableView setObjectValue:anObject forTableColumn:(NSTableColumn *)aTableColumn row:(int)rowIndex
{
}

- (void)tableView:(NSTableView *)tableView didClickTableColumn:(NSTableColumn *)tableColumn
{
}

@end
