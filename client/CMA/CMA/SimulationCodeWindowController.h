//
//  SimulationCodeWindowController.h
//  CMA
//
//  Created by Scott Christley on 5/15/13.
//  Copyright (c) 2013 Scott Christley. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "BioModel.h"

@interface SimulationCodeWindowController : NSWindowController
{
  BioModel *theBioModel;
  
  IBOutlet NSTextField *biomodelName;
  IBOutlet NSTableView *specTable;
  IBOutlet NSButton *generateCodeButton;
  IBOutlet NSTextView *resultLog;
  
  NSMutableArray *specList;
}

- (void)setBioModel:(id)aModel;

- (IBAction)generateSimulationCode:(id)sender;

@end
