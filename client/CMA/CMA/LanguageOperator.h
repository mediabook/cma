//
//  LanguageOperator.h
//  CMA
//
//  Created by Scott Christley on 5/7/13.
//  Copyright (c) 2013 Scott Christley. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LanguageOperator : NSObject
{
  @public
  int operatorID;
	int reduceID;
	NSString *declaration;
	NSMutableArray *arguments;
}

+ (LanguageOperator *)newLanguageOperator:(NSString *)name reduceTo:(int)anInt arguments:(NSArray *)argList;

- (NSString *)parseCMAServerXMLDocument: (NSData *)theData;
- (void)parseCMAServerXML: (NSXMLNode *)aNode;

- (int)operatorID;
- (int)reduceID;
- (NSString *)declaration;
- (NSArray *)arguments;

- (BOOL)deleteLanguageOperator;
- (BOOL)updateLanguageOperator:(NSString *)name reduceTo:(int)anInt arguments:(NSArray *)argList;

@end
