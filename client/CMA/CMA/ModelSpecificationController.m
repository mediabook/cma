//
//  ModelSpecificationController.m
//  CMA
//
//  Created by Scott Christley on 5/8/13.
//  Copyright (c) 2013 Scott Christley. All rights reserved.
//

#import "ModelSpecificationController.h"
#import "CMAServerConnection.h"
#import "Mapping.h"
#import "ModelSpecification.h"

#import "internal.h"

@interface ModelSpecificationController ()
- (void)loadMappingList;
@end

@implementation ModelSpecificationController

- (id)initWithWindow:(NSWindow *)window
{
    self = [super initWithWindow:window];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

- (void)windowDidLoad
{
  [super windowDidLoad];
  
  // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
  [self loadMappingList];
  [biomodelName setStringValue: [theBioModel modelName]];
  [specNameTextField setStringValue: @"Untitled"];
}

- (void)setBioModel:(id)aModel
{
  if (theBioModel) [theBioModel release];
  theBioModel = [aModel retain];
  [biomodelName setStringValue: [theBioModel modelName]];
}

- (IBAction)generateSpecification:(id)sender
{
  CMAServerConnection *theConnection = [[CMAServerConnection new] autorelease];
  NSMutableURLRequest *theRequest = [theConnection standardRequest];
  Mapping *aMapping = [mappingList objectAtIndex: [mappingTable selectedRow]];

  NSString *body = [NSString stringWithFormat: @"procedure=GENERATE_SPECIFICATION&model_id=%d&mapping_id=%d&spec_name=%@",
                    [theBioModel modelID], [aMapping mappingID], [[specNameTextField stringValue] stringWithURLPercentEscape]];
  NSData *bodyData = [NSData dataWithBytes: [body UTF8String] length: [body length]];
  [theRequest setHTTPBody: bodyData];
  
  [theConnection performSynchronousRequest: theRequest];
  
  printf("%s\n", [[theConnection receivedData] bytes]);
  //[self processBioModelList: [theConnection receivedData]];


  ModelSpecification *aSpec = [ModelSpecification new];
  NSString *errorDesc = [aSpec parseCMAServerXMLDocument: [theConnection receivedData]];
  if (errorDesc) {
    NSRunAlertPanel(@"Could not generate model specification", errorDesc, @"OK", nil, nil);
    return;
  }
  [theBioModel addModelSpecification: aSpec];
  [biomodelSpecification selectAll: self];
  [biomodelSpecification insertText: [aSpec specText]];
}

- (void)loadMappingList
{
  CMAServerConnection *theConnection = [[CMAServerConnection new] autorelease];
  NSMutableURLRequest *theRequest = [theConnection standardRequest];
  
  NSString *body = @"procedure=MAPPING_LIST";
  NSData *bodyData = [NSData dataWithBytes: [body UTF8String] length: [body length]];
  [theRequest setHTTPBody: bodyData];
  
  [theConnection performSynchronousRequest: theRequest];

  [self processMappingList: [theConnection receivedData]];
}

- (void)processMappingList: (NSData *)theData
{
  NSError *error;
  NSXMLDocument *xmlDoc = [[NSXMLDocument alloc] initWithData: theData options:0 error: &error];
  NSXMLElement *root = [xmlDoc rootElement];
  printf("%s\n", [[root XMLString] UTF8String]);
  //printf("%d\n", [root childCount]);
  
  mappingList = [NSMutableArray new];
  
  int i;
  for (i = 0; i < [root childCount]; ++i) {
    NSXMLNode *item = [root childAtIndex:i];
    
    Mapping *aMapping = [Mapping new];
    [mappingList addObject: aMapping];
    [aMapping parseCMAServerXML: item];
  }
  
  [mappingTable reloadData];
}

//
//-----//-----//-----//-----//-----//-----//-----//-----
//
// Manage table data
//

- (void)tableViewSelectionDidChange:(NSNotification *)aNotification
{
	id obj = [aNotification object];
	if (obj == mappingTable) {
    printf("%ld\n", [mappingTable selectedRow]);
		if ([mappingTable selectedRow] < 0) {
			[generateSpecificationButton setEnabled: NO];
		} else {
			[generateSpecificationButton setEnabled: YES];
		}
	}
}

- (NSUInteger)numberOfRowsInTableView:(NSTableView *)tView
{
	if (tView == mappingTable) {
		return [mappingList count];
	}
  
	return 0;
}

- (id)tableView:(NSTableView *)tView objectValueForTableColumn:(NSTableColumn *)tableColumn row:(int)row
{
	if (tView == mappingTable) {
	  Mapping *aMapping = [mappingList objectAtIndex: row];
		return [aMapping mappingName];
	}
  
  return nil;
}

-(void)tableView:(NSTableView *)aTableView setObjectValue:anObject forTableColumn:(NSTableColumn *)aTableColumn row:(int)rowIndex
{
}

- (void)tableView:(NSTableView *)tableView didClickTableColumn:(NSTableColumn *)tableColumn
{
}


@end
