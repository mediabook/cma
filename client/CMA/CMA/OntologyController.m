//
//  OntologyController.m
//  CMA
//
//  Created by Scott Christley on 5/6/13.
//  Copyright (c) 2013 Scott Christley. All rights reserved.
//

#import "OntologyController.h"
#import "CMAServerConnection.h"

@interface OntologyController ()
- (void)processOntologyList: (NSData *)theData;
- (void)processChildTerms: (NSData *)theData forItem:(NSMutableDictionary *)anItem;
@end

@implementation OntologyController

- (id)initWithWindow:(NSWindow *)window
{
    self = [super initWithWindow:window];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

- (void)windowDidLoad
{
  [super windowDidLoad];

  // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.

  // load ontology list
  NSString *body = @"procedure=ONTOLOGY_LIST";
  NSData *bodyData = [NSData dataWithBytes: [body UTF8String] length: [body length]];
  NSURL *theURL = [NSURL URLWithString: @"http://sur-gpu01.bsd.uchicago.edu/newcma/php/CMAServer.php"];
  NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL: theURL];
  
  [theRequest setHTTPMethod: @"POST"];
  [theRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
  [theRequest setHTTPBody: bodyData];
  
  CMAServerConnection *theConnection = [[CMAServerConnection new] autorelease];
  [theConnection performRequest: theRequest];
  
  while (![theConnection isDone]) {
    [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
  }
  
  [self processOntologyList: [theConnection receivedData]];  
}

- (void)retrieveChildTerms:(NSMutableDictionary *)anItem
{
  NSString *body = [NSString stringWithFormat: @"procedure=TERM_LIST&term_id=%@", [anItem objectForKey: @"term_id"]];
  NSData *bodyData = [NSData dataWithBytes: [body UTF8String] length: [body length]];
  NSURL *theURL = [NSURL URLWithString: @"http://sur-gpu01.bsd.uchicago.edu/newcma/php/CMAServer.php"];
  NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL: theURL];
  
  [theRequest setHTTPMethod: @"POST"];
  [theRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
  [theRequest setHTTPBody: bodyData];
  
  CMAServerConnection *theConnection = [[CMAServerConnection new] autorelease];
  [theConnection performRequest: theRequest];
  
  while (![theConnection isDone]) {
    [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
  }
  
  [self processChildTerms: [theConnection receivedData] forItem: anItem];
}

- (void)processOntologyList: (NSData *)theData
{
  NSError *error;
  NSXMLDocument *xmlDoc = [[NSXMLDocument alloc] initWithData: theData options:0 error: &error];
  NSXMLElement *root = [xmlDoc rootElement];
  //printf("%s\n", [[root XMLString] UTF8String]);
  //printf("%d\n", [root childCount]);

  ontologyList = [NSMutableArray new];
  
  int i, j, k;
  for (i = 0; i < [root childCount]; ++i) {
    NSXMLNode *ontItem = [root childAtIndex:i];

    NSMutableDictionary *anOntology = [NSMutableDictionary dictionary];
    [ontologyList addObject: anOntology];
    for (j = 0; j < [ontItem childCount]; ++j) {
      NSXMLNode *item = [ontItem childAtIndex:j];
      
      if ([[item name] isEqualToString: @"ontology_id"]) {
        [anOntology setObject:[item stringValue] forKey: @"ontology_id"];
      }
      if ([[item name] isEqualToString: @"name"]) {
        [anOntology setObject:[item stringValue] forKey: @"name"];
      }
      if ([[item name] isEqualToString: @"bioportal_id"]) {
        [anOntology setObject:[item stringValue] forKey: @"bioportal_id"];
      }
      if ([[item name] isEqualToString: @"version_id"]) {
        [anOntology setObject:[item stringValue] forKey: @"version_id"];
      }
      if ([[item name] isEqualToString: @"description"]) {
        [anOntology setObject:[item stringValue] forKey: @"description"];
      }
      
      if ([[item name] isEqualToString: @"ontology_term"]) {
        //printf("%d\n", [item childCount]);
        NSMutableArray *terms = [anOntology objectForKey: @"children"];
        if (!terms) {
          terms = [NSMutableArray array];
          [anOntology setObject: terms forKey: @"children"];
        }
        NSMutableDictionary *term = [NSMutableDictionary dictionary];
        [terms addObject: term];
        
        for (k = 0; k < [item childCount]; ++k) {
          NSXMLNode *termItem = [item childAtIndex:k];
          //printf("%s\n", [[termItem XMLString] UTF8String]);
          
          if ([[termItem name] isEqualToString: @"term_id"]) {
            [term setObject:[termItem stringValue] forKey: @"term_id"];
          }
          if ([[termItem name] isEqualToString: @"ontology_term_id"]) {
            [term setObject:[termItem stringValue] forKey: @"bioportal_id"];
          }
          if ([[termItem name] isEqualToString: @"term_name"]) {
            [term setObject:[termItem stringValue] forKey: @"name"];
          }
          if ([[termItem name] isEqualToString: @"term_definition"]) {
            [term setObject:[termItem stringValue] forKey: @"description"];
          }
          if ([[termItem name] isEqualToString: @"num_children"]) {
            [term setObject:[termItem stringValue] forKey: @"num_children"];
          }
        }
      }
    }
  }
  //printf("%s\n", [[ontologyList description] UTF8String]);
  [ontologyTable reloadData];
}

- (void)processChildTerms: (NSData *)theData forItem:(NSMutableDictionary *)anItem
{
  NSError *error;
  NSXMLDocument *xmlDoc = [[NSXMLDocument alloc] initWithData: theData options:0 error: &error];
  NSXMLElement *root = [xmlDoc rootElement];
  printf("%s\n", [[root XMLString] UTF8String]);
  printf("%ld\n", (unsigned long)[root childCount]);
  
	NSMutableArray *terms = [NSMutableArray array];
	[anItem setObject: terms forKey: @"children"];

  int i, j;
  for (i = 0; i < [root childCount]; ++i) {
    NSXMLNode *ontItem = [root childAtIndex:i];

    NSMutableDictionary *term = [NSMutableDictionary dictionary];
    [terms addObject: term];

    for (j = 0; j < [ontItem childCount]; ++j) {
      NSXMLNode *termItem = [ontItem childAtIndex:j];
      printf("%s\n", [[termItem XMLString] UTF8String]);
      
      if ([[termItem name] isEqualToString: @"term_id"]) {
        [term setObject:[termItem stringValue] forKey: @"term_id"];
      }
      if ([[termItem name] isEqualToString: @"ontology_term_id"]) {
        [term setObject:[termItem stringValue] forKey: @"bioportal_id"];
      }
      if ([[termItem name] isEqualToString: @"term_name"]) {
        [term setObject:[termItem stringValue] forKey: @"name"];
      }
      if ([[termItem name] isEqualToString: @"term_definition"]) {
        [term setObject:[termItem stringValue] forKey: @"description"];
      }
      if ([[termItem name] isEqualToString: @"num_children"]) {
        [term setObject:[termItem stringValue] forKey: @"num_children"];
      }
    }
  }
}

- (IBAction)openBioPortal:(id)sender
{
	id item = [ontologyTable itemAtRow: [ontologyTable selectedRow]];
	if (!item) return;
  
	id rootItem = item;
	id parent = [ontologyTable parentForItem: item];
	while (parent) {
		rootItem = parent;
		parent = [ontologyTable parentForItem: parent];
	}
  
	NSMutableString *url = [NSMutableString stringWithFormat: @"http://bioportal.bioontology.org/ontologies/%@", [rootItem objectForKey: @"version_id"]];
	if (rootItem != item) [url appendFormat: @"/?p=terms&conceptid=%@", [item objectForKey: @"bioportal_id"]];
  
	//window.open(url);
}

//
//-----//-----//-----//-----//-----//-----//-----//-----
//
// auto-completion for search field
//

- (void)controlTextDidBeginEditing:(NSNotification *)aNotification
{
  id obj = [aNotification object];
  
  if (obj == searchField) {
    checkFindTyping = 0;
    checkFindTypingLength = 0;
  }
}


- (void)controlTextDidChange:(NSNotification *)aNotification
{
  id obj = [aNotification object];

  if (obj == searchField) {
    NSString *currentTyped = [searchField stringValue];
  
    if ([currentTyped length] == 0) {
      checkFindTyping = 0;
      checkFindTypingLength = 0;
    }
    
    if (checkFindTypingLength < [currentTyped length]) {
      checkFindTyping++;

      [searchField setStringValue: [NSString stringWithFormat: @"%@abc", currentTyped]];
      NSRange range = {checkFindTyping, 3};
      [(NSText *)[[searchField window] firstResponder] setSelectedRange:range];

      /*
      for (NSString *occurence in array) {
        if (currentTyped.length >= checkFindTyping) {
          if ([[currentTyped substringToIndex:checkFindTyping] length] >= checkFindTyping) {
            if ([occurence length] >= checkFindTyping) {
              NSString *preString = [occurence substringToIndex:checkFindTyping];
              if ([preString isEqualToString:[currentTyped substringToIndex:checkFindTyping]]) {
                checkFindTypingLength = [currentTyped length];
                [searchField setStringValue:occurence];
                [searchField selectText:self];
                NSRange range = {checkFindTyping, occurence.length};
                [(NSText *)[[searchField window] firstResponder] setSelectedRange:range];
                break;
              }
            }
          }
        }
      } */
    }
  }
}

//
//-----//-----//-----//-----//-----//-----//-----//-----
//
// Manage the outline view for the biomodel
//

- (NSInteger)outlineView:(NSOutlineView *)outlineView numberOfChildrenOfItem:(id)item
{
  //    return (item == nil) [[workbench objectForKey: @"managedObjects"] count] : 0;
  
  if (item == nil)
    return [ontologyList count];
  else {
  	id c = [item objectForKey: @"children"];
  	if (c) return [c count];
  	c = [item objectForKey: @"num_children"];
  	if (c) return [c intValue];
  }
  
  return 0;
}

- (BOOL)outlineView:(NSOutlineView *)outlineView isItemExpandable:(id)item
{
	if (item == nil) return YES;
	
	if ([[item objectForKey: @"num_children"] intValue] != 0) return YES;
	id c = [item objectForKey: @"children"];
	if (!c) return NO;
	if ([c count] == 0) return NO;
  
	return YES;
}

- (void)outlineViewItemWillExpand:(NSNotification *)aNotification
{
	id item = [[aNotification userInfo] objectForKey: @"NSObject"];
	id c = [item objectForKey: @"children"];
	if (c) return;
	c = [item objectForKey: @"num_children"];
	if (c == 0) return; // should not be expandable anyways if no children
    
	// if children not loaded then retrieve them
	[self retrieveChildTerms: item];
}

- (id)outlineView:(NSOutlineView *)outlineView
            child:(int)index
           ofItem:(id)item
{
  if (item == nil) {
		return [ontologyList objectAtIndex: index];
	} else {
		return [[item objectForKey: @"children"] objectAtIndex: index];
	}
	
	return nil;
}

- (id)outlineView:(NSOutlineView *)outlineView objectValueForTableColumn:(NSTableColumn *)tableColumn byItem:(id)item
{
  printf("%s\n", [[item objectForKey: @"name"] UTF8String]);
  return [item objectForKey: @"name"];
}

- (void)outlineViewSelectionDidChange:(NSNotification *)aNotification
{
	id item = [ontologyTable itemAtRow: [ontologyTable selectedRow]];
	if (!item) return;
    
	id anID = [item objectForKey: @"ontology_id"];
	if (!anID) anID = [item objectForKey: @"term_id"];
	[termID setStringValue: anID];
	[bioportalID setStringValue: [item objectForKey: @"bioportal_id"]];
	[nameField setStringValue: [item objectForKey: @"name"]];
	[descriptionField setStringValue: [item objectForKey: @"description"]];
}

@end
