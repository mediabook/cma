//
//  BioModelSpecificationViewController.m
//  CMA
//
//  Created by Scott Christley on 5/9/13.
//  Copyright (c) 2013 Scott Christley. All rights reserved.
//

#import "BioModelSpecificationViewController.h"
#import "AppDelegate.h"
#import "CMAServerConnection.h"

#import "internal.h"

@interface BioModelSpecificationViewController ()

@end

@implementation BioModelSpecificationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
      // register for notifications
      [[NSNotificationCenter defaultCenter] addObserver:self
                                               selector:@selector(bioModelWillLoad:)
                                                   name:BioModelWillLoadNotification object:nil];
      [[NSNotificationCenter defaultCenter] addObserver:self
                                               selector:@selector(bioModelDidLoad:)
                                                   name:BioModelDidLoadNotification object:nil];
      [[NSNotificationCenter defaultCenter] addObserver:self
                                               selector:@selector(bioModelWillSave:)
                                                   name:BioModelWillSaveNotification object:nil];
      [[NSNotificationCenter defaultCenter] addObserver:self
                                               selector:@selector(bioModelDidSave:)
                                                   name:BioModelDidSaveNotification object:nil];
      [[NSNotificationCenter defaultCenter] addObserver:self
                                               selector:@selector(bioModelWillRelinquish:)
                                                   name:BioModelWillRelinquishViewNotification object:nil];
      [[NSNotificationCenter defaultCenter] addObserver:self
                                               selector:@selector(bioModelDidDisplay:)
                                                   name:BioModelDidDisplayViewNotification object:nil];
    }
    
    return self;
}

- (void)setBioModel:(id)aModel
{
  if (theBioModel) [theBioModel release];
  theBioModel = [aModel retain];
}

- (void)setModelSpecification:(id)aSpec
{
  if (theSpec) [theSpec release];
  theSpec = [aSpec retain];
}

- (void)allowEdit:(BOOL)aFlag
{
  [specNameTextField setEditable: aFlag];
}

- (void)updateViewContents
{
  if (!theSpec) return;
  
  [specNameTextField setStringValue: [theSpec specName]];
  
  [specDateTextField setStringValue: [NSDateFormatter localizedStringFromDate: [theSpec specDate]
                                                                    dateStyle: NSDateFormatterMediumStyle
                                                                    timeStyle: NSDateFormatterMediumStyle]];

  if ([theSpec specText]) {
    [specTextField selectAll: self];
    [specTextField insertText: [theSpec specText]];
  }
}

//
// Notifications
//
- (void)bioModelWillLoad:(NSNotification *)notification
{
  [self setBioModel: nil];
  [self allowEdit: NO];
}

- (void)bioModelDidLoad:(NSNotification *)notification
{
  [self setBioModel: [notification object]];
  if (!theBioModel) return;
  
  [self updateViewContents];
  [self allowEdit: YES];
}

- (void)bioModelWillSave:(NSNotification *)notification
{
}

- (void)bioModelDidSave:(NSNotification *)notification
{
}

- (void)bioModelWillRelinquish:(NSNotification *)notification
{
}

- (void)bioModelDidDisplay:(NSNotification *)notification
{
  if ([notification object] == self) {
    if (!theBioModel) {
      [self allowEdit: NO];
      
      // if no biomodel set, set it then update the view contents
      NSDictionary *d = [notification userInfo];
      [self setBioModel: [d objectForKey: @"biomodel"]];
      if (!theBioModel) return;
      
      [self updateViewContents];
      [self allowEdit: YES];
    }
    
    // check specification that was selected in outline
    NSDictionary *item = [[notification userInfo] objectForKey: @"outlineItem"];
    if (!item) return;

    ModelSpecification *aSpec = [item objectForKey: @"model_specification"];
    [self setModelSpecification: aSpec];
    if (!theSpec) return;
    
    // load specification if needed
    if (![theSpec specText]) [theSpec loadModelSpecificationText];
    
    [self updateViewContents];
    [self allowEdit: YES];
  }
}

@end
