//
//  ModelSpecificationController.h
//  CMA
//
//  Created by Scott Christley on 5/8/13.
//  Copyright (c) 2013 Scott Christley. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "BioModel.h"

@interface ModelSpecificationController : NSWindowController
{
  BioModel *theBioModel;

  IBOutlet NSTextField *biomodelName;
  IBOutlet NSTextField *specNameTextField;
  IBOutlet NSTableView *mappingTable;
  IBOutlet NSButton *generateSpecificationButton;
  IBOutlet NSTextView *biomodelSpecification;
  
  NSMutableArray *mappingList;
}

- (void)setBioModel:(id)aModel;

- (IBAction)generateSpecification:(id)sender;

@end
