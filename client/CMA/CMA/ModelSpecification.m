//
//  ModelSpecification.m
//  CMA
//
//  Created by Scott Christley on 5/16/13.
//  Copyright (c) 2013 Scott Christley. All rights reserved.
//

#import "ModelSpecification.h"
#import "CMAServerConnection.h"

#import "internal.h"

@implementation ModelSpecification

- (void)parseCMAServerXML: (NSXMLNode *)aNode
{
  int i;
  for (i = 0; i < [aNode childCount]; ++i) {
    NSXMLNode *item = [aNode childAtIndex:i];
    
    if ([[item name] isEqualToString: @"spec_id"]) {
      specID = [[item stringValue] intValue];
    }
    if ([[item name] isEqualToString: @"spec_name"]) {
      specName = [[[item stringValue] stringWithReplaceURLPercentEscape] retain];
    }
    if ([[item name] isEqualToString: @"model_id"]) {
      modelID = [[item stringValue] intValue];
    }
    if ([[item name] isEqualToString: @"mapping_id"]) {
      modelID = [[item stringValue] intValue];
    }
    if ([[item name] isEqualToString: @"spec_date"]) {
      specDate = [[NSDate alloc] initWithTimeIntervalSince1970: [[item stringValue] intValue]];
    }
    if ([[item name] isEqualToString: @"spec"]) {
      specText = [[NSString alloc] initWithString: [item XMLString]];
    }
  }
}

- (NSString *)parseCMAServerXMLDocument: (NSData *)theData
{
  NSError *error;
  NSXMLDocument *xmlDoc = [[NSXMLDocument alloc] initWithData: theData options:0 error: &error];
  if (!xmlDoc) return [NSString stringWithFormat: @"Invalid XML received from CMAServer: %@\n%@",
                       [error localizedDescription], [[[NSString alloc] initWithData:theData encoding:NSUTF8StringEncoding] autorelease]];

  NSXMLElement *root = [xmlDoc rootElement];
  printf("%s\n", [[root XMLString] UTF8String]);

  NSString *errorDesc = checkXMLFail(xmlDoc);
  if (errorDesc) return errorDesc;

  [self parseCMAServerXML: [root childAtIndex:0]];
  return nil;
}

- (int)specID { return specID; }
- (NSString *)specName { return specName; }
- (int)modelID { return modelID; }
- (int)mappingID { return mappingID; }
- (NSDate *)specDate { return specDate; }
- (NSString *)specText { return specText; }

- (void)loadModelSpecificationText
{
  CMAServerConnection *theConnection = [[CMAServerConnection new] autorelease];
  NSMutableURLRequest *theRequest = [theConnection standardRequest];
  
  NSString *body = [NSString stringWithFormat: @"procedure=LOAD_SPECIFICATION_TEXT&spec_id=%d", specID];
  NSData *bodyData = [NSData dataWithBytes: [body UTF8String] length: [body length]];
  [theRequest setHTTPBody: bodyData];
  
  [theConnection performSynchronousRequest: theRequest];
  
  [self parseCMAServerXMLDocument: [theConnection receivedData]];
  
  // TODO: process return
  //[self parseBioModelXML: [theConnection receivedData]];
  
}

@end
