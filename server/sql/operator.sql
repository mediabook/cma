DROP TABLE operator_master;
DROP TABLE operator_detail;

CREATE TABLE operator_master (
       operator_id SERIAL PRIMARY KEY,
       operator_name varchar,
       reduce_id int
);

CREATE TABLE operator_detail (
       operator_id int,
       term_no int,
       term_id int,
       PRIMARY KEY(operator_id, term_no)
);

/*
DROP TRIGGER delete_trigger ON operator_master;
DROP FUNCTION delete_terms_trigger();
DROP FUNCTION new_operator(varchar(256), int, varchar(256) ARRAY);

CREATE FUNCTION delete_terms_trigger() RETURNS trigger as $$
BEGIN
DELETE FROM operator_detail WHERE operator_id=OLD.operator_id;
RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER delete_trigger AFTER DELETE ON operator_master FOR EACH ROW EXECUTE PROCEDURE delete_terms_trigger();

CREATE FUNCTION new_operator(new_operator_name varchar(256), new_reduce_id int, new_terms varchar(256) ARRAY) RETURNS BOOLEAN as $$
DECLARE new_operator_id INTEGER;
DECLARE new_term_counter INTEGER := 1;
DECLARE new_term varchar(256);
BEGIN
INSERT INTO operator_master (operator_name, reduce_id) VALUES(new_operator_name, 0) RETURNING operator_id INTO new_operator_id;
IF new_reduce_id IS NULL THEN
UPDATE operator_master SET reduce_id=new_operator_id WHERE operator_id = new_operator_id;
ELSE
UPDATE operator_master SET reduce_id=new_reduce_id WHERE operator_id = new_operator_id;
END IF;
FOREACH new_term IN ARRAY new_terms
LOOP
INSERT INTO operator_detail (operator_id, term_no, term) VALUES (new_operator_id, new_term_counter, new_term);
new_term_counter := new_term_counter + 1;
END LOOP;
RETURN true;
END;
$$ LANGUAGE plpgsql;
*/
