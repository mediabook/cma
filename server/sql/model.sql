DROP TABLE model_master;
DROP TABLE model_detail;
DROP TABLE model_specification;
DROP TABLE model_code;
DROP TABLE simulation_run;

CREATE TABLE model_master (
       model_id SERIAL PRIMARY KEY,
       model_name varchar,
       model_version varchar,
       model_versioned_from int,
       model_date int,
       model_description varchar,
       model_owner int,
       model_notes varchar,
       model_immutable BOOLEAN,
       model_deprecated BOOLEAN,
       model_checked BOOLEAN
);

CREATE TABLE model_detail (
       model_id int,
       statement_num int,
       statement_id int
);

CREATE TABLE model_specification (
       spec_id SERIAL PRIMARY KEY,
       spec_name varchar,
       model_id int,
       mapping_id int,
       spec_date int,
       specification xml
);

CREATE TABLE model_code (
       code_id SERIAL PRIMARY KEY,
       model_id int,
       spec_id int,
       code_date int
);

CREATE TABLE model_parameters (
       parameter_id SERIAL PRIMARY KEY,
       code_id int,
       parameter_name varchar,
       default_value varchar,
       statement_id int
);

CREATE TABLE simulation_run (
       run_id SERIAL PRIMARY KEY,
       code_id int,
       run_type int,
       run_date int
);

CREATE TABLE simulation_parameters (
       run_id int,
       parameter_id int,
       parameter_value varchar
);
