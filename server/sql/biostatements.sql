DROP TABLE biostatements;

CREATE TABLE biostatements (
       statement_id SERIAL PRIMARY KEY,
       statement_type int,
       statement varchar,
       term_id int,
       annotations varchar(1024));