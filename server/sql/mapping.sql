DROP TABLE mapping_master;
DROP TABLE mapping_detail;
DROP TABLE mapping_ro;
DROP TABLE mapping_ov;

CREATE TABLE mapping_master(mapping_id SERIAL PRIMARY KEY, mapping_name varchar(256), mapping_version varchar(256), mapping_versioned_from int, mapping_date varchar(256), mapping_immutable BOOLEAN, cm_id int);
CREATE TABLE mapping_detail(mapping_id int, rule_no int, rule_id SERIAL PRIMARY KEY, rule_name varchar(256), rule_code varchar(1024));
CREATE TABLE mapping_ro(rule_id int, operator_no int, operator_id int);
CREATE TABLE mapping_ov(rule_id int, operator_no int, var_no int, variable varchar(256));