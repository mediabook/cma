DROP TABLE cm_master;
DROP TABLE cm_detail;
DROP TABLE cm_sorts;
DROP TABLE cm_subsorts;
DROP TABLE cm_inc;
DROP TABLE cm_equations;
DROP TABLE cm_operators;

CREATE TABLE cm_master(cm_id SERIAL PRIMARY KEY, cm_name varchar(256), cm_version varchar(256), cm_versioned_from int, cm_date varchar(256), cm_immutable BOOLEAN, cm_issys BOOLEAN);
CREATE TABLE cm_detail(cm_id int, line_no int, line_type varchar(64), line_id int);
CREATE TABLE cm_sorts(line_id SERIAL PRIMARY KEY, sort varchar(256));
CREATE TABLE cm_subsorts(line_id SERIAL PRIMARY KEY, sort varchar(256), subsort varchar(256));
CREATE TABLE cm_inc(line_id SERIAL PRIMARY KEY, cm_id int);
CREATE TABLE cm_equations(line_id SERIAL PRIMARY KEY, eq_left varchar(256), eq_right varchar(256), condition varchar(256), ctor varchar(8), assoc varchar(8), comm varchar(8), iter varchar(8), id varchar(256));
CREATE TABLE cm_operators(line_id SERIAL PRIMARY KEY, operator varchar(256), parameters varchar(1024), result varchar(256), ctor varchar(8), assoc varchar(8), comm varchar(8), iter varchar(8), id varchar(256));
CREATE TABLE cm_variables(line_id SERIAL PRIMARY KEY, variable varchar(256), definition varchar(256));