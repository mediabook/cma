DROP TABLE ontology;
DROP TABLE ontology_term_master;
DROP TABLE ontology_term_detail;

CREATE TABLE ontology(
       ontology_id SERIAL PRIMARY KEY,
       name varchar,
       description varchar,
       bioportal_id int,
       version_id int,
       bioportal_info xml
);

CREATE TABLE ontology_term_master(
       term_id SERIAL PRIMARY KEY,
       ontology_id int,
       ontology_term_id varchar,
       term_name varchar,
       term_definition varchar
);

CREATE TABLE ontology_term_detail(
       term_detail_id SERIAL PRIMARY KEY,
       term_id int,
       term_relation varchar,
       relation_value varchar
);

