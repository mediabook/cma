<?php

// Connection to the database
//include $_SERVER['DOCUMENT_ROOT'] . '/cma/include/php/dbConnection.php'; 
include '/etc/cma/dbConnection.php'; 

//
//-----//-----//-----//-----//-----//-----//-----//-----
//
// Standard XML responses
//
//-----//-----//-----//-----//-----//-----//-----//-----
//

// success
function successXML()
{
    $xml_output = new DOMDocument();
    $xml_response = $xml_output->appendChild($xml_output->createElement("cma_response"));
    $xml_response->setAttribute("result", "success");
    
    $xml_output->formatOutput = true;
    echo $xml_output->saveXML();
}

function failXML($errorDesc)
{
    $xml_output = new DOMDocument();
    $xml_response = $xml_output->appendChild($xml_output->createElement("cma_response"));
    $xml_response->setAttribute("result", "fail");
    $xml_response->appendChild($xml_output->createElement("error_description", $errorDesc));

    $xml_output->formatOutput = true;
    echo $xml_output->saveXML();
}

//
//-----//-----//-----//-----//-----//-----//-----//-----
//
// Maude interface functions
//
//-----//-----//-----//-----//-----//-----//-----//-----
//

// Send a command to maude
function sendMaudeCommand($pipes, $command, $returnOutput = true)
{
    fwrite($pipes[0], $command . "\n");
    if($returnOutput)
        return getMaudeOutput($pipes);
}

// Function that reads in the next block of Maude command line output
function getMaudeOutput($pipes)
{
    // Output container
    $output = array();
    
    // Read in all available lines
    while(true)
    {
        // Buffer for the current line
        $linebuffer = "";
        $readChar = fgetc($pipes[1]);
        
        // Read in to the buffer until a newline is found
        while($readChar != "\n")
        {
            $linebuffer .= $readChar;
            
            // Check if the current line is the Maude> prompt
            if($linebuffer == "Maude> ")
            {
                return $output;
            }
            $readChar = fgetc($pipes[1]);
        }
        // Push the buffer into the container
        array_push($output, $linebuffer);
    }
}

function loadOntology($file)
{
    fwrite($file, "load /home/www-data/common/ontology\n");

    $ontologyMod = "mod ONTOLOGY_ALL is\n";
    $result = pg_query("SELECT bioportal_id FROM ontology");
    while ($row = pg_fetch_row($result)) {
       fwrite($file, "load /home/www-data/common/sorts_" . $row[0] . "\n");
       $ontologyMod = $ontologyMod . "   including ONTOLOGY_" . $row[0] . "_SORTS .\n";
    }

    $ontologyMod = $ontologyMod . "endm\n";
    fwrite($file, $ontologyMod);
}

function loadBiologyKR($file, $processID)
{
    fwrite($file, "load /home/www-data/common/biology_krbase\n");

    $opFilename = sys_get_temp_dir() . "/" . $processID . "_operators.maude";
    $opFile = fopen($opFilename, "w");

    fwrite($opFile, "mod BIOLOGY_KR is\n");
    fwrite($opFile, "   including BIOLOGY_KRBASE .\n");
    fwrite($opFile, "   including ONTOLOGY_ALL .\n");
    fwrite($opFile, "\n");

    // ontology terms used by operators
    $result = pg_query("SELECT DISTINCT term_name FROM operator_detail a, ontology_term_master b WHERE a.term_id = b.term_id");
    while ($row = pg_fetch_row($result)) {
       fwrite($opFile, "   subsort \"" . $row[0] . "\" < noun .\n");
    }
    fwrite($opFile, "\n");

    // language operators
    $result = pg_query("SELECT operator_id, operator_name, reduce_id FROM operator_master");
    while ($row = pg_fetch_row($result)) {
       fwrite($opFile, "   op " . $row[1] . " :");

       $argResult = pg_query("SELECT term_no, a.term_id, term_name"
    	       	  . " FROM operator_detail a, ontology_term_master b"
	       	  . " WHERE operator_id = " . $row[0] . " and a.term_id = b.term_id order by term_no");
       while ($argRow = pg_fetch_row($argResult)) {
          fwrite($opFile, " \"" . $argRow[2] . "\"");
       }
       fwrite($opFile, " -> statement .\n");

    }
    fwrite($opFile, "endm\n");
    fclose($opFile);

    // biology knowledgebase
    fwrite($file, "load $opFilename\n");
}

function loadModules($file, $modelID, $processID)
{
    // ontologies
    loadOntology($file);

    // biology knowledgebase
    loadBiologyKR($file, $processID);

    // computational methods
    fwrite($file, "load /home/www-data/common/model_spec\n");

    // mapping
    fwrite($file, "load /home/www-data/common/model_mapping\n");
}

function writeMaudeBioModel($file, $modelID, $mappingID)
{
    fwrite($file, "mod BIOMODEL" . $modelID . " is\n");
    fwrite($file, "including BIOLOGY_KR .\n");

    // TODO: should include appropriate mapping module
    fwrite($file, "including MAPPING_REACTION_RULE .\n");

    // definitions
    fwrite($file, "\n*** definitions\n");
    $result = pg_prepare("defs", "SELECT statement_num, a.statement_id, statement, b.term_id, c.term_name"
    	    . " FROM model_detail a, biostatements b, ontology_term_master c"
	    . " WHERE model_id = $1 and a.statement_id = b.statement_id and b.statement_type = 1 and b.term_id = c.term_id");
    $result = pg_execute("defs", array($modelID));
    while ($row = pg_fetch_row($result)) {
       fwrite($file, "op " . $row[2] . " : -> \"" . $row[4] . "\" .\n");
    }

   // individual biostatements
    fwrite($file, "\n*** biostatements\n");
   $result = pg_prepare("statements", "SELECT statement_num, a.statement_id, statement, b.term_id"
   	   . " FROM model_detail a, biostatements b"
	   . " WHERE model_id = $1 and a.statement_id = b.statement_id and b.statement_type = 0");
   $result = pg_execute("statements", array($modelID));
   while ($row = pg_fetch_row($result)) {
       fwrite($file, "op statement" . $row[1] . " : -> statement .\n");
       fwrite($file, "eq statement" . $row[1] . " = " . $row[2] . " .\n");
       //fwrite($file, "eq statementID(statement" . $row[1] . ") = <string> " . $row[1] . " </string> .\n");
    }

   // all biostatements
   fwrite($file, "op biostatements : -> statementSet .\n");
   fwrite($file, "eq biostatements = \n");
   $result = pg_execute("statements", array($modelID));
   while ($row = pg_fetch_row($result)) {
       fwrite($file, "   (statement" . $row[1] . ")\n");
    }
    fwrite($file, "   .\n");

    // biomodel
    fwrite($file, "\n*** biomodel\n");
    fwrite($file, "op biomodel : -> specification .\n");
    fwrite($file, "eq biomodel = spec(BKR(biostatements), PS(emptyStatement), MS(xmlDict(emptyXMLKeyValue))) .\n");
    fwrite($file, "endm\n");
}

function extractResultSort($maudeText)
{
    $sortPos = strpos($maudeText, "result ");
    if ($sortPos === false) return NULL;
    $pos = strpos($maudeText, ": ", $sortPos);
    if ($pos === false) return NULL;
    $sortStr = substr($maudeText, $sortPos + 7, $pos - $sortPos - 7);
    return $sortStr;
    $resultStr = substr($fullResult, $pos + 2);
}

function extractResult($maudeText)
{
    $sortPos = strpos($maudeText, "result ");
    if ($sortPos === false) return NULL;
    $pos = strpos($maudeText, ": ", $sortPos);
    if ($pos === false) return NULL;
    $resultStr = substr($maudeText, $pos + 2);
    return $resultStr;
}

// clean up the XML produced by Maude
function cleanSpecXML($specText)
{
    // eliminate extraneous parentheses
    $specPos = strpos($specText, "<modelSpecification>");
    if ($specPos === false) return NULL;
    $specStr = substr($specText, $specPos);
    $specStr = str_replace("(", "", $specStr);
    $specStr = str_replace(")", "", $specStr);
    $specText = substr($specText, 0, $specPos) . $specStr;

    // put into xml document
    $spec = new DOMDocument();
    $spec->loadXML($specText);
    $node = $spec->getElementsByTagName("spec")->item(0);

    $list = $node->getElementsByTagName("string");
    foreach ($list as $item) {
       $item->nodeValue = trim($item->nodeValue);
    }

    return $node;
}


//
//-----//-----//-----//-----//-----//-----//-----//-----
//
// BioModel
//
//-----//-----//-----//-----//-----//-----//-----//-----
//

// create new biomodel
function newBioModel()
{
    $modelIDResult = pg_query("INSERT INTO model_master(model_name, model_version, model_date)"
    		   . "VALUES('Untitled BioModel', 1, " . time() . ") RETURNING model_id");
    if (!$modelIDResult) return failXML("CMAServer error in newBioModel function.");
    $modelIDRow = pg_fetch_row($modelIDResult);
    if (!$modelIDRow) return failXML("CMAServer error in newBioModel function.");

    openBioModel($modelIDRow[0]);
}

// create new biomodel from an existing model
function newBioModelFromExisting($existingModelID)
{
}

// get list of all biomodels
//
function biomodelList()
{
    $xml_output = new DOMDocument();
    $xml_rList = $xml_output->appendChild($xml_output->createElement("biomodel_list"));

    $result = pg_query("SELECT model_id, model_name FROM model_master");
    while($row = pg_fetch_row($result)) {
       $modelID = $row[0];
       $modelName = $row[1];

       $xml_opNode = $xml_rList->appendChild($xml_output->createElement("biomodel"));
       $xml_opNode->appendChild($xml_output->createElement("model_id", $modelID));
       $xml_opNode->appendChild($xml_output->createElement("model_name", urlencode($modelName)));
    }

    $xml_output->formatOutput = true;
    echo $xml_output->saveXML();
}

//
// load existing biomodel
//
function openBioModel($modelID)
{
    $xml_output = new DOMDocument();
    $xml_response = $xml_output->appendChild($xml_output->createElement("cma_response"));
    $xml_biomodel = $xml_response->appendChild($xml_output->createElement("biomodel"));

    // header data
    $result = pg_prepare("header", "SELECT model_name, model_version, model_versioned_from, model_date, model_description, model_owner, model_notes, model_immutable, model_deprecated, model_checked"
    	    . " FROM model_master where model_id = $1");
    if (!$result) return failXML("CMAServer error in openBioModel function.");
    $result = pg_execute("header", array($modelID));
    if (!$result) return failXML("CMAServer error in openBioModel function.");
    while ($row = pg_fetch_row($result)) {
       $xml_biomodel->appendChild($xml_output->createElement("model_id", $modelID));
       $xml_biomodel->appendChild($xml_output->createElement("model_name", urlencode($row[0])));
       $xml_biomodel->appendChild($xml_output->createElement("model_version", urlencode($row[1])));
       $xml_biomodel->appendChild($xml_output->createElement("model_versioned_from", urlencode($row[2])));
       $xml_biomodel->appendChild($xml_output->createElement("model_date", urlencode($row[3])));
       $xml_biomodel->appendChild($xml_output->createElement("model_description", urlencode($row[4])));
       $xml_biomodel->appendChild($xml_output->createElement("model_owner", urlencode($row[5])));
       $xml_biomodel->appendChild($xml_output->createElement("model_notes", urlencode($row[6])));
       if ($row[7] == "t") $xml_biomodel->appendChild($xml_output->createElement("model_immutable", "1"));
       else $xml_biomodel->appendChild($xml_output->createElement("model_immutable", "0"));
       if ($row[8] == "t") $xml_biomodel->appendChild($xml_output->createElement("model_deprecated", "1"));
       else $xml_biomodel->appendChild($xml_output->createElement("model_deprecated", "0"));
       if ($row[9] == "t") $xml_biomodel->appendChild($xml_output->createElement("model_checked", "1"));
       else $xml_biomodel->appendChild($xml_output->createElement("model_checked", "0"));
    }

    // biostatements data
    $result = pg_prepare("detail", "SELECT statement_num, a.statement_id, statement_type, statement, term_id"
    	    . " FROM model_detail a, biostatements b "
	    . " WHERE model_id = $1 and a.statement_id = b.statement_id order by statement_num");
    if (!$result) return failXML("CMAServer error in openBioModel function.");
    $termResult = pg_prepare("term", "SELECT term_name FROM ontology_term_master WHERE term_id = $1");
    if (!$termResult) return failXML("CMAServer error in openBioModel function.");
    $result = pg_execute("detail", array($modelID));
    if (!$result) return failXML("CMAServer error in openBioModel function.");
    while ($row = pg_fetch_row($result)) {
       $xml_stmt = $xml_biomodel->appendChild($xml_output->createElement("biostatement"));
       $xml_stmt->appendChild($xml_output->createElement("statement_num", $row[0]));
       $xml_stmt->appendChild($xml_output->createElement("statement_id", $row[1]));
       $xml_stmt->appendChild($xml_output->createElement("statement_type", $row[2]));
       $xml_stmt->appendChild($xml_output->createElement("statement", $row[3]));

       // get ontology term info if definition
       if ($row[4]) {
	  $termResult = pg_execute("term", array($row[4]));
    	  if (!$termResult) return failXML("CMAServer error in openBioModel function.");
	  $term = pg_fetch_row($termResult);
    	  if (!$term) return failXML("CMAServer error in openBioModel function.");

          $xml_stmt->appendChild($xml_output->createElement("term_id", $row[4]));
          $xml_stmt->appendChild($xml_output->createElement("term_name", $term[0]));
       }
    }

    // model specifications
    $result = pg_prepare("spec", "SELECT spec_id, spec_name, mapping_id, spec_date"
    	    . " FROM model_specification "
	    . " WHERE model_id = $1");
    if (!$result) return failXML("CMAServer error in openBioModel function.");
    $result = pg_execute("spec", array($modelID));
    if (!$result) return failXML("CMAServer error in openBioModel function.");
    while ($row = pg_fetch_row($result)) {
       $xml_spec = $xml_biomodel->appendChild($xml_output->createElement("model_specification"));
       $xml_spec->appendChild($xml_output->createElement("spec_id", $row[0]));
       $xml_spec->appendChild($xml_output->createElement("spec_name", urlencode($row[1])));
       $xml_spec->appendChild($xml_output->createElement("mapping_id", $row[2]));
       $xml_spec->appendChild($xml_output->createElement("spec_date", $row[3]));
    }

    // model codes
    $result = pg_prepare("code", "SELECT code_id, model_id, spec_id, code_date"
    	    . " FROM model_code "
	    . " WHERE model_id = $1");
    if (!$result) return failXML("CMAServer error in openBioModel function.");
    $result = pg_execute("code", array($modelID));
    if (!$result) return failXML("CMAServer error in openBioModel function.");
    while ($row = pg_fetch_row($result)) {
       $xml_code = $xml_biomodel->appendChild($xml_output->createElement("model_code"));
       $xml_code->appendChild($xml_output->createElement("code_id", $row[0]));
       $xml_code->appendChild($xml_output->createElement("model_id", $row[1]));
       $xml_code->appendChild($xml_output->createElement("spec_id", $row[2]));
       $xml_code->appendChild($xml_output->createElement("code_date", $row[3]));
    }

    $xml_response->setAttribute("result", "success");
    $xml_output->formatOutput = true;
    echo $xml_output->saveXML();
}

// update field in biomodel master record
function updateBioModelMaster($modelID, $modelField, $fieldValue)
{
    // validate the column name
    $result = pg_prepare("column", "SELECT column_name FROM information_schema.columns"
    	    . " WHERE table_name = 'model_master' and column_name = $1");
    if (!$result) return failXML("CMAServer error in updateBioModelMaster function.");
    $result = pg_execute("column", array($modelField));
    if (!$result) return failXML("CMAServer error in updateBioModelMaster function.");
    $row = pg_fetch_row($result);
    if (!$row) return failXML("CMAServer error in updateBioModelMaster function.");

    // cannot change the model id
    if ($row[0] == "model_id") return failXML("CMAServer error in updateBioModelMaster function.");

    // update the field
    $result = pg_prepare("update", "UPDATE model_master SET $row[0] = $1 where model_id = $2");
    if (!$result) return failXML("CMAServer error in updateBioModelMaster function.");
    $result = pg_execute("update", array($fieldValue, $modelID));
    if (!$result) return failXML("CMAServer error in updateBioModelMaster function.");

    return successXML();
}

// create new biostatement
function newBioStatement($modelID, $stmtText, $stmtType, $termID)
{
    if ($stmtType) {
        $result = pg_prepare("biostatement", "INSERT INTO biostatements (statement, statement_type, term_id)"
    	    	. " VALUES($1, $2, $3) RETURNING statement_id");
    	if (!$result) return failXML("CMAServer error in newBioStatement function.");
        $result = pg_execute("biostatement", array($stmtText, $stmtType, $termID));
    	if (!$result) return failXML("CMAServer error in newBioStatement function.");
    } else {
        $result = pg_prepare("biostatement", "INSERT INTO biostatements (statement, statement_type)"
    	    	. " VALUES($1, $2) RETURNING statement_id");
    	if (!$result) return failXML("CMAServer error in newBioStatement function.");
        $result = pg_execute("biostatement", array($stmtText, $stmtType));
    	if (!$result) return failXML("CMAServer error in newBioStatement function.");
    }
    $row = pg_fetch_row($result);
    if (!$row) return failXML("CMAServer error in newBioStatement function.");

    $result = pg_prepare("detail", "INSERT INTO model_detail (model_id, statement_id) VALUES ($1, $2)");
    if (!$result) return failXML("CMAServer error in newBioStatement function.");
    $result = pg_execute("detail", array($modelID, $row[0]));
    if (!$result) return failXML("CMAServer error in newBioStatement function.");
    
    $xml_output = new DOMDocument();
    $xml_response = $xml_output->appendChild($xml_output->createElement("cma_response"));
    $xml_line = $xml_response->appendChild($xml_output->createElement("biostatement"));
    $xml_line->appendChild($xml_output->createElement("statement_id", $row[0]));

    $xml_response->setAttribute("result", "success");
    $xml_output->formatOutput = true;
    echo $xml_output->saveXML();
}

// update field for biostatement
function updateBioStatement($stmtID, $modelField, $fieldValue)
{
    // validate the column name
    $result = pg_prepare("column", "SELECT column_name FROM information_schema.columns"
    	    . " WHERE table_name = 'biostatements' and column_name = $1");
    if (!$result) return failXML("CMAServer error in updateBioStatement function.");
    $result = pg_execute("column", array($modelField));
    if (!$result) return failXML("CMAServer error in updateBioStatement function.");
    $row = pg_fetch_row($result);
    if (!$row) return failXML("CMAServer error in updateBioStatement function.");

    // cannot change the id
    if ($row[0] == "statement_id") return failXML("CMAServer error in updateBioStatement function.");

    // update the field
    $result = pg_prepare("update", "UPDATE biostatements SET $row[0] = $1 where statement_id = $2");
    if (!$result) return failXML("CMAServer error in updateBioStatement function.");
    $result = pg_execute("update", array($fieldValue, $stmtID));
    if (!$result) return failXML("CMAServer error in updateBioStatement function.");

    return successXML();
}

// delete biostatement
function deleteBioStatement($modelID, $stmtID)
{
    $result = pg_prepare("delete", "DELETE from model_detail where model_id = $1 and statement_id = $2");
    if (!$result) return failXML("CMAServer error in deleteBioStatement function.");
    $result = pg_execute("delete", array($modelID, $stmtID));
    if (!$result) return failXML("CMAServer error in deleteBioStatement function.");

    return successXML();
}

//
//-----//-----//-----//-----//-----//-----//-----//-----
//
// BioModel error checking
//
//-----//-----//-----//-----//-----//-----//-----//-----
//
function errorCheckBioModel($modelID)
{
    $xml_output = new DOMDocument();
    $xml_response = $xml_output->appendChild($xml_output->createElement("cma_response"));
    $xml_line = $xml_response->appendChild($xml_output->createElement("error_check_results"));

    $isError = 0;

    // temporary file for input to Maude
    $processID = uniqid(rand(), true);
    $tmpFile = sys_get_temp_dir() . "/" . $processID . ".maude";
    $file = fopen($tmpFile, "w");

    // setup standard modules in Maude
    loadModules($file, $modelID, $processID);
    fclose($file);

    // Specifies the pipes
    $descriptorspec = array(
        0 => array("pipe", "r"),  // stdin is a pipe that the child will read from
     	1 => array("pipe", "w"),  // stdout is a pipe that the child will write to
   	2 => array("pipe", "w") // stderr is a file to write to
    );

    // Open Maude
    $process = proc_open('maude -no-banner 2>&1', $descriptorspec, $pipes);
    getMaudeOutput($pipes);

    // load standard modules
    $maudeResult = sendMaudeCommand($pipes, "load $tmpFile");
    foreach($maudeResult as $line) {
       $isError = 1;
       $xml_line->appendChild($xml_output->createElement("init", $line));
    }

    $defFileName = sys_get_temp_dir() . "/" . $processID . "_defs.maude";
    $defFile = fopen($defFileName, "w");
    fwrite($defFile, "mod BIOMODEL_DEFS" . $modelID . " is\n");
    fwrite($defFile, "including BIOLOGY_KR .\n");

    // Verify the definitions individually
    $result = pg_prepare("defs", "SELECT statement_num, a.statement_id, statement_type, statement, b.term_id, c.term_name"
    	    . " FROM model_detail a, biostatements b, ontology_term_master c"
	    . " WHERE model_id = $1 and a.statement_id = b.statement_id and b.statement_type = 1 and b.term_id = c.term_id");
    if (!$result) return failXML("CMAServer error in errorCheckBioModel function.");
    $result = pg_execute("defs", array($modelID));
    if (!$result) return failXML("CMAServer error in errorCheckBioModel function.");
    while ($row = pg_fetch_row($result)) {
       $modelFile = sys_get_temp_dir() . "/" . $processID . "_biomodel.maude";
       $file = fopen($modelFile, "w");

       fwrite($file, "mod BIOMODEL" . $modelID . "_DEFS" . $row[1] . " is\n");
       fwrite($file, "including BIOLOGY_KR .\n");
       fwrite($file, "op " . $row[3] . " : -> \"" . $row[5] . "\" .\n");
       fwrite($file, "endm\n");
       fclose($file);

       $hasError = 0;
       $maudeResult = sendMaudeCommand($pipes, "load $modelFile");
       foreach($maudeResult as $line) {
          $hasError = 1;
          $isError = 1;
          $xml_def = $xml_line->appendChild($xml_output->createElement("definition"));
          $xml_def->appendChild($xml_output->createElement("model_id", $modelID));
          $xml_def->appendChild($xml_output->createElement("statement_id", $row[1]));
          $xml_def->appendChild($xml_output->createElement("error", $line));
       }

       if (!$hasError) {
          fwrite($defFile, "op " . $row[3] . " : -> \"" . $row[5] . "\" .\n");
       }
    }

    fwrite($defFile, "endm\n");
    fclose($defFile);

    // Verify the biostatements
    $maudeResult = sendMaudeCommand($pipes, "load $defFileName");

    $result = pg_prepare("statements", "SELECT statement_num, a.statement_id, statement_type, statement, b.term_id"
    	    . " FROM model_detail a, biostatements b"
	    . " WHERE model_id = $1 and a.statement_id = b.statement_id and statement_type = 0");
    if (!$result) return failXML("CMAServer error in errorCheckBioModel function.");
    $result = pg_execute("statements", array($modelID));
    if (!$result) return failXML("CMAServer error in errorCheckBioModel function.");
    while ($row = pg_fetch_row($result)) {
       $modelFile = sys_get_temp_dir() . "/" . $processID . "_biomodel.maude";
       $file = fopen($modelFile, "w");

       fwrite($file, "mod BIOMODEL" . $modelID . "_STMT" . $row[1] . " is\n");
       fwrite($file, "including BIOMODEL_DEFS" . $modelID . " .\n");
       fwrite($file, "op biomodel : -> statementSet .\n");
       fwrite($file, "eq biomodel = " . $row[3] . " .\n");
       fwrite($file, "endm\n");
       fclose($file);

       // syntax is correct if maude can parse it
       $maudeResult = sendMaudeCommand($pipes, "load $modelFile");
       foreach($maudeResult as $line) {
          $hasError = 1;
          $isError = 1;
          $xml_def = $xml_line->appendChild($xml_output->createElement("biostatement"));
          $xml_def->appendChild($xml_output->createElement("model_id", $modelID));
          $xml_def->appendChild($xml_output->createElement("statement_id", $row[1]));
          $xml_def->appendChild($xml_output->createElement("statement", $row[3]));
          $xml_def->appendChild($xml_output->createElement("error", $line));
       }

       // perform reduce to verify valid statement
       $maudeResult = sendMaudeCommand($pipes, "reduce biomodel .");
       $fullResult = "";
       foreach($maudeResult as $line) {
          $fullResult = $fullResult . trim($line);
       }

       // extract result sort
       $resultSort = extractResultSort($fullResult);
       if ($resultSort != "statement") {
          $isError = 1;
          $xml_def = $xml_line->appendChild($xml_output->createElement("biostatement"));
          $xml_def->appendChild($xml_output->createElement("model_id", $modelID));
          $xml_def->appendChild($xml_output->createElement("statement_id", $row[1]));
          $xml_def->appendChild($xml_output->createElement("statement", $row[3]));
          $xml_def->appendChild($xml_output->createElement("error", "Does not reduce to statement, possibly incorrect type for variables"));
       }

    }

    if ($isError) { $xml_response->setAttribute("result", "fail"); }
    else { $xml_response->setAttribute("result", "success"); }
    $xml_output->formatOutput = true;
    echo $xml_output->saveXML();
}

//
//-----//-----//-----//-----//-----//-----//-----//-----
//
// Model specifications
//
//-----//-----//-----//-----//-----//-----//-----//-----
//

// generate model specification
function generateSpecification($modelID, $mappingID, $specName)
{
    // verify model has been error checked
    $result = pg_prepare("header", "SELECT model_checked"
    	    . " FROM model_master where model_id = $1");
    $result = pg_execute("header", array($modelID));
    $row = pg_fetch_row($result);
    if ($row[0] != "t") return failXML();

    // temporary file for input to Maude
    $processID = uniqid(rand(), true);
    $tmpFilename = sys_get_temp_dir() . "/" . $processID . ".maude";
    $modelFilename = sys_get_temp_dir() . "/" . $processID . "_biomodel.maude";
    $file = fopen($tmpFilename, "w");

    loadModules($file, $modelID, $processID);
    fwrite($file, "load $modelFilename\n");
    fclose($file);

    $file = fopen($modelFilename, "w");
    writeMaudeBioModel($file, $modelID, $mappingID);
    fclose($file);

    // Specifies the pipes
    $descriptorspec = array(
        0 => array("pipe", "r"),  // stdin is a pipe that the child will read from
     	1 => array("pipe", "w"),  // stdout is a pipe that the child will write to
   	2 => array("pipe", "w") // stderr is a file to write to
    );

    // Open Maude
    $process = proc_open('maude -no-banner 2>&1', $descriptorspec, $pipes);
    getMaudeOutput($pipes);

    // load up the biomodel
    $maudeResponse = "";
    $result = sendMaudeCommand($pipes, "load $tmpFilename");
    foreach($result as $line) {
       $maudeResponse = $maudeResponse . $line . "\n";
    }

    // perform rewrite
    $result = sendMaudeCommand($pipes, "rewrite biomodel .");
    $fullResult = "";
    foreach($result as $line) {
       $fullResult = $fullResult . trim($line);
    }
    $maudeResponse = $maudeResponse . $fullResult;

    // extract result
    $resultStr = extractResult($fullResult);
    if (!$resultStr) return failXML($maudeResponse);

    $node = cleanSpecXML($resultStr);
    if (!$node) return failXML($maudeResponse);

    $xml_save = new DOMDocument();
    $xml_node = $xml_save->importNode($node, true);
    $xml_save->formatOutput = true;
    $xml_save->appendChild($xml_node);

    // verify that all biostatements were processed
    $checkNode = $xml_save->getElementsByTagName("bioKR")->item(0);
    if (trim($checkNode->nodeValue) != "emptyStatement")
       return failXML("Not all biostatements could be translated, maybe missing mapping?\n\n" . $checkNode->nodeValue);

    // save the resultant specification
    $specDate = time();
    $result = pg_prepare("insert", "INSERT INTO model_specification (spec_name, model_id, mapping_id, spec_date, specification)"
    	    . " VALUES ($1, $2, $3, $4, $5) RETURNING spec_id");
    if (!$result) return failXML("CMAServer error in generateSpecification function, could not insert specification into database.");
    $result = pg_execute("insert", array($specName, $modelID, $mappingID, $specDate, $xml_save->saveXML()));
    if (!$result) return failXML("CMAServer error in generateSpecification function, could not insert specification into database.");
    $row = pg_fetch_row($result);
    if (!$row) return failXML("CMAServer error in generateSpecification function, could not insert specification into database.");

    // return specification to client
    $xml_output = new DOMDocument();
    $xml_response = $xml_output->appendChild($xml_output->createElement("cma_response"));
    $xml_list = $xml_response->appendChild($xml_output->createElement("model_specification"));
    $xml_list->appendChild($xml_output->createElement("spec_id", $row[0]));
    $xml_list->appendChild($xml_output->createElement("spec_name", urlencode($specName)));
    $xml_list->appendChild($xml_output->createElement("model_id", $modelID));
    $xml_list->appendChild($xml_output->createElement("mapping_id", $mappingID));
    $xml_list->appendChild($xml_output->createElement("spec_date", $specDate));
    $node = $xml_output->importNode($node, true);
    $xml_list->appendChild($node);

    $xml_response->setAttribute("result", "success");
    $xml_output->formatOutput = true;
    echo $xml_output->saveXML();
}

function loadSpecificationText($specID)
{
    $xml_output = new DOMDocument();
    $xml_response = $xml_output->appendChild($xml_output->createElement("cma_response"));
    $xml_list = $xml_response->appendChild($xml_output->createElement("model_specification"));

    // header data
    $result = pg_prepare("spec", "SELECT specification"
    	    . " FROM model_specification where spec_id = $1");
    $result = pg_execute("spec", array($specID));
    while ($row = pg_fetch_row($result)) {
       $xml_list->appendChild($xml_output->createElement("spec_id", $specID));

       $spec = new DOMDocument();
       $spec->loadXML($row[0]);
       $node = $spec->getElementsByTagName("spec")->item(0);

       $node = $xml_output->importNode($node, true);
       $xml_list->appendChild($node);
    }

    $xml_output->formatOutput = true;
    echo $xml_output->saveXML();
}

//
//-----//-----//-----//-----//-----//-----//-----//-----
//
// Simulations
//
//-----//-----//-----//-----//-----//-----//-----//-----
//

// generate simulation code
function generateSimulationCode($modelID, $specID)
{
    $xml_output = new DOMDocument();
    $xml_response = $xml_output->appendChild($xml_output->createElement("cma_response"));
    $xml_list = $xml_response->appendChild($xml_output->createElement("simulation_code"));

    // insert simulation code entry
    $result = pg_prepare("insert", "INSERT INTO model_code (model_id, spec_id, code_date)"
    	    . " VALUES ($1, $2, $3) RETURNING code_id");
    $result = pg_execute("insert", array($modelID, $specID, time()));
    $row = pg_fetch_row($result);
    $codeID = $row[0];
    $xml_list->appendChild($xml_output->createElement("code_id", $codeID));

    // create work area for simulation
    $common = "/home/www-data/common/";
    $path = "/home/www-data/simulation/model" . $modelID . "/biomodel" . $codeID;
    mkdir($path, 0755, true);
    copy($common . "CMABiomodel.m", $path . "/CMABiomodel.m");
    copy($common . "GNUmakefile", $path . "/GNUmakefile");
    copy($common . "processSpec.sh", $path . "/processSpec.sh");
    copy($common . "compile.sh", $path . "/compile.sh");
    copy($common . "runSimulation.sh", $path . "/runSimulation.sh");
    copy($common . "blank.bioswarm", $path . "/blank.bioswarm");

    // process model specification
    global $dbuser, $dbpassword, $dbname;
    $cmd = "cd $path; bash ./processSpec.sh $codeID $dbuser " . escapeshellarg($dbpassword) . " $dbname";
    system($cmd);

    // generate code and compile
    system("cd $path; bash ./compile.sh");

    $xml_output->formatOutput = true;
    echo $xml_output->saveXML();
}

// parameters for running a simulation
function loadModelParameters($codeID)
{
    $xml_output = new DOMDocument();
    $xml_response = $xml_output->appendChild($xml_output->createElement("cma_response"));
    $xml_list = $xml_response->appendChild($xml_output->createElement("model_parameters"));

    $result = pg_prepare("param", "SELECT parameter_id, code_id, parameter_name, default_value, statement_id"
    	    . " FROM model_parameters where code_id = $1");
    if (!$result) return failXML("CMAServer error in loadModelParameters function.");
    $result = pg_execute("param", array($codeID));
    if (!$result) return failXML("CMAServer error in loadModelParameters function.");

    while($row = pg_fetch_row($result)) {
       $xml_opNode = $xml_list->appendChild($xml_output->createElement("parameter"));
       $xml_opNode->appendChild($xml_output->createElement("parameter_id", $row[0]));
       $xml_opNode->appendChild($xml_output->createElement("code_id", $row[1]));
       $xml_opNode->appendChild($xml_output->createElement("parameter_name", $row[2])); 
       $xml_opNode->appendChild($xml_output->createElement("default_value", $row[3]));
       $xml_opNode->appendChild($xml_output->createElement("statement_id", $row[4]));
    }

    $xml_response->setAttribute("result", "success");
    $xml_output->formatOutput = true;
    echo $xml_output->saveXML();
}

// run simulation
function runSimulation($modelID, $codeID)
{
    $xml_output = new DOMDocument();
    $xml_response = $xml_output->appendChild($xml_output->createElement("cma_response"));
    $xml_list = $xml_response->appendChild($xml_output->createElement("simulation_run"));

    // insert simulation run entry
    $result = pg_prepare("insert", "INSERT INTO simulation_run (code_id, run_type, run_date)"
    	    . " VALUES ($1, $2, $3) RETURNING run_id");
    $result = pg_execute("insert", array($codeID, 0, time()));
    $row = pg_fetch_row($result);
    $runID = $row[0];
    $xml_list->appendChild($xml_output->createElement("run_id", $runID));

    // create work area for simulation
    $common = "/home/www-data/common/";
    $path = "/home/www-data/simulation/model" . $modelID . "/biomodel" . $codeID;
    $runPath = $path . "/run" . $runID;
    mkdir($runPath, 0755, true);

    // run the simulation
    // TODO: should throw job in a queue
    global $dbuser, $dbpassword, $dbname;
    $cmd = "cd $path; bash ./runSimulation.sh $runID $dbuser " . escapeshellarg($dbpassword) . " $dbname";
    system($cmd);

    $xml_output->formatOutput = true;
    echo $xml_output->saveXML();
}

//
//-----//-----//-----//-----//-----//-----//-----//-----
//
// Language Operators
//
//-----//-----//-----//-----//-----//-----//-----//-----
//
// get operator list
//
function operatorList()
{
    $xml_output = new DOMDocument();
    $xml_response = $xml_output->appendChild($xml_output->createElement("cma_response"));
    $xml_list = $xml_response->appendChild($xml_output->createElement("operator_list"));

    $result = pg_query("SELECT operator_id, operator_name, reduce_id FROM operator_master");
    if (!$result) return failXML("CMAServer error in operatorList function.");
    $argResult = pg_prepare("arguments", "SELECT term_no, a.term_id, term_name"
    	       . " FROM operator_detail a, ontology_term_master b"
	       . " WHERE operator_id = $1 and a.term_id = b.term_id order by term_no");
    if (!$argResult) return failXML("CMAServer error in operatorList function.");

    while($row = pg_fetch_row($result)) {
       $xml_opNode = $xml_list->appendChild($xml_output->createElement("operator"));
       $xml_opNode->appendChild($xml_output->createElement("operator_id", $row[0]));
       $xml_opNode->appendChild($xml_output->createElement("operator_name", $row[1])); 
       $xml_opNode->appendChild($xml_output->createElement("reduce_id", $row[2]));

       $argResult = pg_execute("arguments", array($row[0]));
       if (!$argResult) return failXML("CMAServer error in operatorList function.");
       while ($argRow = pg_fetch_row($argResult)) {
          $xml_argNode = $xml_opNode->appendChild($xml_output->createElement("operator_arguments"));
          $xml_argNode->appendChild($xml_output->createElement("term_no", $argRow[0]));
          $xml_argNode->appendChild($xml_output->createElement("term_id", $argRow[1]));
          $xml_argNode->appendChild($xml_output->createElement("term_name", $argRow[2]));
       }
    }

    $xml_response->setAttribute("result", "success");
    $xml_output->formatOutput = true;
    echo $xml_output->saveXML();
}

// translates the domain arguments in the HTTP POST into an array
function processDomainArguments()
{
    // Output container
    $output = array();

    $termNo = 0;
    $termTotal = $_POST["term_total"];
    if (!$termTotal) {
       failXML("CMAServer error: term total not given.");
       return NULL;
    }

    for ($termNo = 0; $termNo < $termTotal; $termNo++) {
       $argString = "term_id_" . $termNo;
       $termID = $_POST[$argString];
       if (!$termID) {
          failXML("CMAServer error: missing term id.");
	  return NULL;
       }

       $term = array();
       $term["term_no"] = $termNo;
       $term["term_id"] = $termID;
       array_push($output, $term);
    }

    return $output;
}

// create new operator
function newLanguageOperator($operatorName, $reduceID, $domainArguments)
{
    if (is_null($domainArguments)) return;

    $result = pg_query("BEGIN");
    if (!$result) return failXML("CMAServer error in newLanguageOperator function.");
    $result = pg_prepare("new operator", "INSERT INTO operator_master (operator_name, reduce_id)"
      	    . " VALUES($1, $2) RETURNING operator_id");
    if (!$result) goto need_rollback;
    $result = pg_execute("new operator", array($operatorName, $reduceID));
    if (!$result) goto need_rollback;
    $row = pg_fetch_row($result);
    if (!$row) goto need_rollback;

    $argResult = pg_prepare("new detail", "INSERT INTO operator_detail (operator_id, term_no, term_id) VALUES ($1, $2, $3)");
    if (!$argResult) goto need_rollback;
    foreach ($domainArguments as $arg) {
       $argResult = pg_execute("new detail", array($row[0], $arg["term_no"], $arg["term_id"]));
       if (!$argResult) goto need_rollback;
    }
    pg_query("COMMIT");

    return loadLanguageOperator($row[0]);

need_rollback:
    pg_query("ROLLBACK");
    return failXML("CMAServer error in newLanguageOperator function.");
}

function loadLanguageOperator($operatorID)
{
    $xml_output = new DOMDocument();
    $xml_response = $xml_output->appendChild($xml_output->createElement("cma_response"));
    $xml_opNode = $xml_response->appendChild($xml_output->createElement("operator"));

    $result = pg_prepare("operator", "SELECT operator_name, reduce_id FROM operator_master where operator_id = $1");
    if (!$result) return failXML("CMAServer error in loadLanguageOperator function.");
    $result = pg_execute("operator", array($operatorID));
    if (!$result) return failXML("CMAServer error in loadLanguageOperator function.");
    $row = pg_fetch_row($result);
    if (!$row) return failXML("CMAServer error in loadLanguageOperator function.");

    $xml_opNode->appendChild($xml_output->createElement("operator_id", $operatorID));
    $xml_opNode->appendChild($xml_output->createElement("operator_name", $row[0])); 
    $xml_opNode->appendChild($xml_output->createElement("reduce_id", $row[1]));

    $argResult = pg_prepare("arguments", "SELECT term_no, a.term_id, term_name"
    	       . " FROM operator_detail a, ontology_term_master b"
	       . " WHERE operator_id = $1 and a.term_id = b.term_id order by term_no");
    if (!$argResult) return failXML("CMAServer error in loadLanguageOperator function.");
    $argResult = pg_execute("arguments", array($operatorID));
    if (!$argResult) return failXML("CMAServer error in loadLanguageOperator function.");
    while ($argRow = pg_fetch_row($argResult)) {
       $xml_argNode = $xml_opNode->appendChild($xml_output->createElement("operator_arguments"));
       $xml_argNode->appendChild($xml_output->createElement("term_no", $argRow[0]));
       $xml_argNode->appendChild($xml_output->createElement("term_id", $argRow[1]));
       $xml_argNode->appendChild($xml_output->createElement("term_name", $argRow[2]));
    }

    $xml_response->setAttribute("result", "success");
    $xml_output->formatOutput = true;
    echo $xml_output->saveXML();
}

// update operator
function updateLanguageOperator($operatorID, $operatorName, $reduceID, $domainArguments)
{
    if (is_null($domainArguments)) return;

    $result = pg_query("BEGIN");
    if (!$result) return failXML("CMAServer error in updateLanguageOperator function.");
    $result = pg_prepare("update operator", "UPDATE operator_master set operator_name = $1, reduce_id = $2 WHERE operator_id = $3");
    if (!$result) goto need_rollback;
    $result = pg_execute("update operator", array($operatorName, $reduceID, $operatorID));
    if (!$result) goto need_rollback;

    // delete the argument entries and re-insert
    $result = pg_prepare("delete detail", "DELETE FROM operator_detail WHERE operator_id = $1");
    if (!$result) goto need_rollback;
    $result = pg_execute("delete detail", array($operatorID));
    if (!$result) goto need_rollback;

    $argResult = pg_prepare("new detail", "INSERT INTO operator_detail (operator_id, term_no, term_id) VALUES ($1, $2, $3)");
    if (!$argResult) goto need_rollback;
    foreach ($domainArguments as $arg) {
       $argResult = pg_execute("new detail", array($operatorID, $arg["term_no"], $arg["term_id"]));
       if (!$argResult) goto need_rollback;
    }
    pg_query("COMMIT");

    return loadLanguageOperator($operatorID);

need_rollback:
    pg_query("ROLLBACK");
    return failXML("CMAServer error in updateLanguageOperator function.");
}

// delete operator
function deleteLanguageOperator($operatorID)
{
    // TODO: Check if operator is used by any models

    $result = pg_query("BEGIN");
    if (!$result) return failXML("CMAServer error in deleteLanguageOperator function.");

    $result = pg_prepare("delete operator", "DELETE FROM operator_master WHERE operator_id = $1");
    if (!$result) goto need_rollback;
    $result = pg_execute("delete operator", array($operatorID));
    if (!$result) goto need_rollback;

    $result = pg_prepare("delete detail", "DELETE FROM operator_detail WHERE operator_id = $1");
    if (!$result) goto need_rollback;
    $result = pg_execute("delete detail", array($operatorID));
    if (!$result) goto need_rollback;

    pg_query("COMMIT");

    return successXML();

need_rollback:
    pg_query("ROLLBACK");
    return failXML("CMAServer error in deleteLanguageOperator function.");
}

//
//-----//-----//-----//-----//-----//-----//-----//-----
//
// Ontologies
//
//-----//-----//-----//-----//-----//-----//-----//-----
//
// get ontology list
//
function ontologyList()
{
    $xml_output = new DOMDocument();
    $xml_list = $xml_output->appendChild($xml_output->createElement("ontology_list"));

    $prevID = -1;
    $result = pg_query("select a.ontology_id, a.name, a.description, a.bioportal_id, a.version_id, b.term_id, ontology_term_id, term_name, term_definition from ontology a, ontology_term_master b, ontology_term_detail c where c.term_relation='root' and b.term_id = c.term_id and a.ontology_id = b.ontology_id order by a.name");
    while($row = pg_fetch_row($result)) {
       if ($row[0] != $prevID) {
       	  $xml_opNode = $xml_list->appendChild($xml_output->createElement("ontology"));
       	  $xml_opNode->appendChild($xml_output->createElement("ontology_id", $row[0]));
       	  $xml_opNode->appendChild($xml_output->createElement("name", $row[1])); 
       	  $xml_opNode->appendChild($xml_output->createElement("description", $row[2])); 
       	  $xml_opNode->appendChild($xml_output->createElement("bioportal_id", $row[3])); 
       	  $xml_opNode->appendChild($xml_output->createElement("version_id", $row[4]));
	  $prevID = $row[0];
       }

       $cntRes = pg_query("select count(*) from ontology_term_master a, ontology_term_detail b where a.term_id = b.term_id and b.relation_value = '$row[6]' and b.term_relation = 'is_a' and a.ontology_id = $row[0]");
       $cntRow = pg_fetch_row($cntRes);
       
       $xml_term = $xml_opNode->appendChild($xml_output->createElement("ontology_term"));
       $xml_term->appendChild($xml_output->createElement("term_id", $row[5]));
       $xml_term->appendChild($xml_output->createElement("ontology_term_id", $row[6]));
       $xml_term->appendChild($xml_output->createElement("term_name", $row[7]));
       $xml_term->appendChild($xml_output->createElement("term_definition", $row[8]));
       $xml_term->appendChild($xml_output->createElement("num_children", $cntRow[0]));
    }

    $xml_output->formatOutput = true;
    echo $xml_output->saveXML();
}

//
// get ontology terms
//
function ontologyTermList($termID)
{
    $xml_output = new DOMDocument();
    $xml_list = $xml_output->appendChild($xml_output->createElement("term_list"));

    $result = pg_query("select ontology_id, ontology_term_id from ontology_term_master where term_id = $termID");
    $row = pg_fetch_row($result);
    if (!$row) return;
    $ontologyID = $row[0];
    $ontologyTermID = $row[1];

    $result = pg_query("select a.term_id, ontology_term_id, term_name, term_definition from ontology_term_master a, ontology_term_detail b where a.term_id = b.term_id and b.relation_value = '$ontologyTermID' and b.term_relation = 'is_a' and a.ontology_id = $ontologyID order by term_name");
    while($row = pg_fetch_row($result)) {
       
       $cntRes = pg_query("select count(*) from ontology_term_master a, ontology_term_detail b where a.term_id = b.term_id and b.relation_value = '$row[1]' and b.term_relation = 'is_a' and a.ontology_id = $ontologyID");
       $cntRow = pg_fetch_row($cntRes);

       $xml_term = $xml_list->appendChild($xml_output->createElement("ontology_term"));
       $xml_term->appendChild($xml_output->createElement("term_id", $row[0]));
       $xml_term->appendChild($xml_output->createElement("ontology_term_id", $row[1]));
       $xml_term->appendChild($xml_output->createElement("term_name", $row[2]));
       $xml_term->appendChild($xml_output->createElement("term_definition", $row[3]));
       $xml_term->appendChild($xml_output->createElement("num_children", $cntRow[0]));
    }

    $xml_output->formatOutput = true;
    echo $xml_output->saveXML();
}

//
// search ontology terms
//
function ontologyTermSearch($termString, $isExact)
{
    if ($isExact) {
        $result = pg_prepare("column", "SELECT term_id, ontology_id, ontology_term_id, term_name, term_definition"
    	    	. " FROM ontology_term_master"
    	    	. " WHERE term_name = $1");
    	if (!$result) return failXML();
    	$result = pg_execute("column", array($termString));
    	if (!$result) return failXML();
    } else {
        $result = pg_prepare("column", "SELECT term_id, ontology_id, ontology_term_id, term_name, term_definition"
    	    	. " FROM ontology_term_master"
    	    	. " WHERE term_name like $1 ORDER BY term_name");
    	if (!$result) return failXML();
    	$result = pg_execute("column", array($termString . "%"));
    	if (!$result) return failXML();
    }

    $xml_output = new DOMDocument();
    $xml_list = $xml_output->appendChild($xml_output->createElement("term_list"));

    while($row = pg_fetch_row($result)) {
       
       $xml_term = $xml_list->appendChild($xml_output->createElement("ontology_term"));
       $xml_term->appendChild($xml_output->createElement("term_id", $row[0]));
       $xml_term->appendChild($xml_output->createElement("ontology_id", $row[1]));
       $xml_term->appendChild($xml_output->createElement("ontology_term_id", $row[2]));
       $xml_term->appendChild($xml_output->createElement("term_name", $row[3]));
       $xml_term->appendChild($xml_output->createElement("term_definition", $row[4]));
    }

    $xml_output->formatOutput = true;
    echo $xml_output->saveXML();
}

//
//-----//-----//-----//-----//-----//-----//-----//-----
//
// Computational methods
//
//-----//-----//-----//-----//-----//-----//-----//-----
//


//
//-----//-----//-----//-----//-----//-----//-----//-----
//
// Mappings
//
//-----//-----//-----//-----//-----//-----//-----//-----
//
// get mapping list
//
function mappingList()
{
    $xml_output = new DOMDocument();
    $xml_list = $xml_output->appendChild($xml_output->createElement("mapping_list"));

    $result = pg_query("SELECT mapping_id, mapping_name FROM mapping_master");
    while ($row = pg_fetch_row($result)) {
       $xml_item = $xml_list->appendChild($xml_output->createElement("mapping"));
       $xml_item->appendChild($xml_output->createElement("mapping_id", $row[0]));
       $xml_item->appendChild($xml_output->createElement("mapping_name", urlencode($row[1])));
    }

    $xml_output->formatOutput = true;
    echo $xml_output->saveXML();
}

//
//-----//-----//-----//-----//-----//-----//-----//-----
//
// process incoming request
//
switch($_POST['procedure'])
{
    // Biomodel
    case "NEW_BIOMODEL":
        newBioModel();
        break;
    case "NEW_BIOMODEL_VERSION":
        newBioModelVersion();
	break;
    case "BIOMODEL_LIST":
        biomodelList();
        break;
    case "OPEN_BIOMODEL":
        openBioModel($_POST["model_id"]);
        break;
    case "UPDATE_BIOMODEL_MASTER":
        updateBioModelMaster($_POST["model_id"], $_POST["field"], $_POST["value"]);
        break;
    case "NEW_BIOSTATEMENT":
        newBioStatement($_POST["model_id"], $_POST["statement"], $_POST["is_definition"], $_POST["term_id"]);
	break;
    case "UPDATE_BIOSTATEMENT":
        updateBioStatement($_POST["statement_id"], $_POST["field"], $_POST["value"]);
	break;
    case "DELETE_BIOSTATEMENT":
        deleteBioStatement($_POST["model_id"], $_POST["statement_id"]);
	break;

    // Error check BioModel
    case "ERROR_CHECK_BIOMODEL":
        errorCheckBioModel($_POST["model_id"]);
        break;

    // Model specification
    case "GENERATE_SPECIFICATION":
        generateSpecification($_POST["model_id"], $_POST["mapping_id"], $_POST["spec_name"]);
        break;
    case "LOAD_SPECIFICATION_TEXT":
        loadSpecificationText($_POST["spec_id"]);
        break;

    // Simulations
    case "GENERATE_SIMULATION_CODE":
        generateSimulationCode($_POST["model_id"], $_POST["spec_id"]);
        break;
    case "LOAD_MODEL_PARAMETERS":
        loadModelParameters($_POST["code_id"]);
        break;
    case "RUN_SIMULATION":
        runSimulation($_POST["model_id"], $_POST["code_id"]);
        break;

    // Language operators
    case "OPERATOR_LIST":
        operatorList();
        break;
    case "NEW_OPERATOR":
        newLanguageOperator($_POST["operator_name"], $_POST["reduce_id"], processDomainArguments());
        break;
    case "UPDATE_OPERATOR":
        updateLanguageOperator($_POST["operator_id"], $_POST["operator_name"], $_POST["reduce_id"], processDomainArguments());
        break;
    case "DELETE_OPERATOR":
        deleteLanguageOperator($_POST["operator_id"]);
        break;

    // Ontologies
    case "ONTOLOGY_LIST":
        ontologyList();
        break;
    case "TERM_LIST":
        ontologyTermList($_POST["term_id"]);
	break;
    case "TERM_SEARCH":
        ontologyTermSearch($_POST["term_substring"], $_POST["is_exact"]);
	break;

    // Computational methods

    // Mappings
    case "MAPPING_LIST":
        mappingList();
        break;

    default:
        echo "And?";
}

pg_close($dbconn);

?>
