//
// manageOntology.m
//
// Perform various ontology management processes
// 1) Load an ontology file into database
// 2) Generate Maude sort/subsort files
//

//
// Deletion of ontology from database can be done manually:
//
// delete from ontology_term_detail where term_detail_id in
//   (select a.term_detail_id from ontology_term_detail a, ontology_term_master b where a.term_id = b.term_id and b.ontology_id = CMA_ID);
// delete from ontology_term_master where ontology_id = CMA_ID;
// delete from ontology where ontology_id = CMA_ID;
//

#import <BioCocoa/BCParseOBO.h>
#import <BioCocoa/BCBioPortalREST.h>

#include <postgresql/libpq-fe.h>

@interface OntologyManager : NSObject
{
  NSString *ontologyID;
  NSMutableString *connectString;
  NSString *apiKey;
  BOOL download;
  BOOL listDatabase;
  BOOL loadDatabase;
  BOOL generateMaude;

  PGconn *dbConnection;
  BCBioPortalREST *bpr;
  NSDictionary *ontology;
}
@end

@implementation OntologyManager

- init
{
  [super init];

  listDatabase = NO;
  loadDatabase = NO;
  generateMaude = NO;
  ontologyID = nil;
  download = NO;
  apiKey = nil;

  dbConnection = NULL;
  connectString = [NSMutableString new];

  return self;
}

- (void)printUsage: (NSArray *)args
{
  printf("Usage:\n");
  printf("%s ontologyID\n\n", [[[args objectAtIndex: 0] lastPathComponent] UTF8String]);

  printf("Options:\n");
  printf("--help               Display usage information.\n");
  printf("--user username      Username for connecting to database.\n");
  printf("--password pw        Password for connecting to database.\n");
  printf("--dbname name        Connect to specified database name.\n");
  printf("--download           Download the ontology from BioPortal if necessary.\n");
  printf("--apikey apikey      API key for accessing BioPortal REST web service.\n");
  printf("--list-database      List the ontologies loaded in database.\n");
  printf("--load-database      Load the ontology terms into database.\n");
  printf("--generate-maude     Generate Maude sort/subsort files.\n");
  printf("ontologyID           BioPortal ID for ontology.\n");
}

- (BOOL)processArguments:(NSArray *)args
{
  BOOL cont = YES;
  NS_DURING {
    
    // process parameters
    NSArray *args = [[NSProcessInfo processInfo] arguments];
    int i, argsCount = [args count];
    for (i = 1; i < argsCount; ++i) {
      if ([[args objectAtIndex: i] isEqualToString: @"--help"]) {
        [self printUsage: args];
        cont = NO;
        goto done;
      }

      if ([[args objectAtIndex: i] isEqualToString: @"--user"]) {
	i++;
	[connectString appendFormat: @" user=%@", [args objectAtIndex: i]];
        continue;
      }
      
      if ([[args objectAtIndex: i] isEqualToString: @"--password"]) {
	i++;
	[connectString appendFormat: @" password=%@", [args objectAtIndex: i]];
        continue;
      }
      
      if ([[args objectAtIndex: i] isEqualToString: @"--dbname"]) {
	i++;
	[connectString appendFormat: @" dbname=%@", [args objectAtIndex: i]];
        continue;
      }
      
      if ([[args objectAtIndex: i] isEqualToString: @"--download"]) {
	download = YES;
        continue;
      }
      
      if ([[args objectAtIndex: i] isEqualToString: @"--apikey"]) {
	i++;
	apiKey = [args objectAtIndex: i];
        continue;
      }
      
      if ([[args objectAtIndex: i] isEqualToString: @"--list-database"]) {
	listDatabase = YES;
        continue;
      }
      
      if ([[args objectAtIndex: i] isEqualToString: @"--load-database"]) {
	loadDatabase = YES;
        continue;
      }
      
      if ([[args objectAtIndex: i] isEqualToString: @"--generate-maude"]) {
	generateMaude = YES;
        continue;
      }
      
      ontologyID = [args objectAtIndex: i];
    }
    
  } NS_HANDLER {
    
    printf("ERROR: Invalid usage.\n\n");
    [self printUsage: args];
    cont = NO;
    
  } NS_ENDHANDLER

  if ((!ontologyID) && (!listDatabase)) {
    printf("ERROR: No ontology ID given.\n");
    cont = NO;
  }

  if ((ontologyID) && ([ontologyID intValue] == 0)) {
    printf("ERROR: Invalid ontology ID: %s\n", [ontologyID UTF8String]);
    cont = NO;
  }

  if (!cont) goto done;

 done:
  return cont;
}

- (BOOL)listOntologies
{
  dbConnection = PQconnectdb([connectString UTF8String]);
  if (PQstatus(dbConnection) != CONNECTION_OK) {
    fprintf(stderr, "ERROR: Connection to database failed: %s",
	    PQerrorMessage(dbConnection));
    dbConnection = NULL;
    return NO;
  }

  NSString *s = @"select bioportal_id, version_id, name from ontology";
  PGresult *res = PQexec(dbConnection, [s UTF8String]);
  if (PQresultStatus(res) != PGRES_TUPLES_OK) {
    fprintf(stderr, "ERROR: Query failed (%d): %s\n", PQresultStatus(res),
	    PQresultErrorMessage(res));
    return NO;
  }

  printf("Ontology ID  Version ID  Name\n");
  printf("===========  ==========  ==================================================\n");

  int numRows = PQntuples(res);
  if (numRows == 0) printf("No ontologies in database.\n");

  int i;
  for (i = 0; i < numRows; ++i) {
    char *oID = PQgetvalue(res, i, 0);
    char *vID = PQgetvalue(res, i, 1);
    char *name = PQgetvalue(res, i, 2);
    printf("%11s  %10s  %-50s\n", oID, vID, name);
  }
  PQclear(res);

  return YES;
}

- (BOOL)insertOntologyIntoDatabase
{
  int j, k;

  // get the latest version of ontology from BioPortal
  printf("Retrieving ontology information from BioPortal...");
  fflush(stdout);
  bpr = [[BCBioPortalREST alloc] initWithAPIKey: apiKey];
  if (!bpr) {
    printf("\nERROR: Could not access BioPortal\n");
    return NO;
  }

  NSDictionary *ontologyInfo = [bpr getOntology: ontologyID];
  if (!ontologyInfo) {
    printf("\nERROR: Could not access BioPortal\n");
    return NO;
  }
  
  //printf("%s\n", [[ontologyInfo description] UTF8String]);

  NSXMLElement *xml = [bpr getOntologyVersionXML: [ontologyInfo objectForKey: @"id"]];
  if (!xml) {
    printf("\nERROR: Could not access BioPortal\n");
    return NO;
  } else printf("Success\n");

  NSString *xmlString = [xml XMLString];
  //printf("%s\n", [xmlString UTF8String]);

  // load ontology file
  printf("Loading ontology...");
  fflush(stdout);
  ontology = [bpr loadOntology: ontologyID];
  if (!ontology) {
    if (download) {
      printf("\nOntology not available locally. Downloading from BioPortal...");
      [bpr downloadOntology: ontologyID];
      ontology = [bpr loadOntology: ontologyID];
    }
  }
  if (!ontology) {
    printf("ERROR: Could not open ontology: %s\n", [ontologyID UTF8String]);
    return NO;
  } else printf("Success\n");

  printf("Connecting to database...");
  fflush(stdout);
  dbConnection = PQconnectdb([connectString UTF8String]);
  if (PQstatus(dbConnection) != CONNECTION_OK) {
    fprintf(stderr, "ERROR: Connection to database failed: %s",
	    PQerrorMessage(dbConnection));
    return NO;
  } else {
    printf("Success\n");
  }

  printf("Inserting ontology header...");
  fflush(stdout);
  NSString *s = [NSString stringWithFormat: @"insert into ontology (name, description, bioportal_id, version_id, bioportal_info) values ('%@', '%@', %@, %@, '%@') returning ontology_id", [ontologyInfo objectForKey: @"displayLabel"], [ontologyInfo objectForKey: @"description"], ontologyID, [ontologyInfo objectForKey: @"id"], xmlString];
  //printf("%s\n", [s UTF8String]);
  PGresult *res = PQexec(dbConnection, [s UTF8String]);
  if (PQresultStatus(res) != PGRES_TUPLES_OK) {
    fprintf(stderr, "ERROR: Insert failed (%d): %s\n", PQresultStatus(res),
	    PQresultErrorMessage(res));
    return NO;
  } else {
    printf("Success\n");
  }

  char *value = PQgetvalue(res, 0, 0);
  NSString *ontologySEQ = [[NSString alloc] initWithCString: value encoding: NSUTF8StringEncoding];
  PQclear(res);
  //printf("%s\n", [ontologySEQ UTF8String]);

  // insert terms into database
  printf("Inserting ontology terms...");
  fflush(stdout);
  //NSMutableDictionary *mergeTerms = [NSMutableDictionary dictionary];
  NSDictionary *classes = [ontology objectForKey: @"classes"];
  NSArray *keys = [classes allKeys];
  int cnt = 0;
  for (j = 0; j < [keys count]; ++j) {
    NSString *key = [keys objectAtIndex: j];
    NSDictionary *c = [classes objectForKey: key];
    if ([[c objectForKey: @"is_obsolete"] isEqualToString: @"true"]) continue;
    if (![[c objectForKey: @"stanza"] isEqualToString: @"[Term]"]) continue;
    
    char *name = NULL, *def = NULL;
    NSString *s, *cmd, *checkCMD;
    NSString *termName = [c objectForKey: @"name"];

    // check if term already exists with name
    checkCMD = [NSString stringWithFormat: @"select ontology_term_id from ontology_term_master where term_name = '%@'", termName];
    PGresult *checkResult = PQexec(dbConnection, [checkCMD UTF8String]);
    if (PQresultStatus(checkResult) != PGRES_TUPLES_OK) {
      fprintf(stderr, "ERROR: Query failed (%d): %s\n", PQresultStatus(checkResult),
	      PQresultErrorMessage(checkResult));
      return NO;
    }
    int numRows = PQntuples(checkResult);
    if (numRows != 0) {
      printf("WARNING: ontology term with name '%s' already exists.  Appending identifier.\n", [termName UTF8String]);
      fflush(stdout);
      termName = [NSString stringWithFormat: @"%@ [%@]", termName, [ontologyInfo objectForKey: @"displayLabel"]];
    }
    PQclear(checkResult);

    name = PQescapeLiteral(dbConnection, [termName UTF8String], [termName length]);
    s = [c objectForKey: @"def"];
    if (s) def = PQescapeLiteral(dbConnection, [s UTF8String], [s length]);
    
    if (def)
      cmd = [NSString stringWithFormat: @"insert into ontology_term_master (ontology_id, ontology_term_id, term_name, term_definition) values (%@, '%@', %s, %s) returning term_id", ontologySEQ, [c objectForKey: @"id"], name, def];
    else
      cmd = [NSString stringWithFormat: @"insert into ontology_term_master (ontology_id, ontology_term_id, term_name) values (%@, '%@', %s) returning term_id", ontologySEQ, [c objectForKey: @"id"], name];

    PGresult *res = PQexec(dbConnection, [cmd UTF8String]);
    if (PQresultStatus(res) != PGRES_TUPLES_OK) {
      fprintf(stderr, "ERROR: Insert failed (%d): %s\n", PQresultStatus(res),
	      PQresultErrorMessage(res));
      return NO;
    }

    value = PQgetvalue(res, 0, 0);
    NSString *termSEQ = [[NSString alloc] initWithCString: value encoding: NSUTF8StringEncoding];
    if (name) PQfreemem(name);
    if (def) PQfreemem(def);
    PQclear(res);

    // isa hierarchy
    NSArray *isaList = [c objectForKey: @"is_a"];
    if (!isaList) {
      // a root term
      cmd = [NSString stringWithFormat: @"insert into ontology_term_detail (term_id, term_relation) values (%@, 'root')", termSEQ];

      PGresult *res = PQexec(dbConnection, [cmd UTF8String]);
      if (PQresultStatus(res) != PGRES_COMMAND_OK) {
	fprintf(stderr, "ERROR: Insert failed (%d): %s\n", PQresultStatus(res),
		PQresultErrorMessage(res));
	return NO;
      }

      PQclear(res);
    }

    for (k = 0; k < [isaList count]; ++k) {
      NSDictionary *p = [classes objectForKey: [isaList objectAtIndex: k]];
      if ([[p objectForKey: @"is_obsolete"] isEqualToString: @"true"]) continue;

      char *relval = NULL;
      s = [isaList objectAtIndex: k];
      relval = PQescapeLiteral(dbConnection, [s UTF8String], [s length]);

      cmd = [NSString stringWithFormat: @"insert into ontology_term_detail (term_id, term_relation, relation_value) values (%@, 'is_a', %s)", termSEQ, relval];

      PGresult *res = PQexec(dbConnection, [cmd UTF8String]);
      if (PQresultStatus(res) != PGRES_COMMAND_OK) {
	fprintf(stderr, "ERROR: Insert failed (%d): %s\n", PQresultStatus(res),
		PQresultErrorMessage(res));
	return NO;
      }

      if (relval) PQfreemem(relval);
      PQclear(res);
    }

    ++cnt;
    if ((cnt % 1000) == 0) { printf("."); fflush(stdout); }
  }
  printf("Success\n");

  return YES;
}

- (BOOL)generateSorts
{
  int i;

  // get database conneciton
  dbConnection = PQconnectdb([connectString UTF8String]);
  if (PQstatus(dbConnection) != CONNECTION_OK) {
    fprintf(stderr, "ERROR: Connection to database failed: %s",
	    PQerrorMessage(dbConnection));
    dbConnection = NULL;
    return NO;
  }

  // Get ontology info from database
  NSString *sql = [NSString stringWithFormat: @"select ontology_id, name from ontology where bioportal_id = %d", [ontologyID intValue]];
  PGresult *res = PQexec(dbConnection, [sql UTF8String]);
  if (PQresultStatus(res) != PGRES_TUPLES_OK) {
    fprintf(stderr, "ERROR: Query failed (%d): %s\n", PQresultStatus(res),
	    PQresultErrorMessage(res));
    return NO;
  }

  int numRows = PQntuples(res);
  if (numRows == 0) printf("No ontology with BioPortal ID: %d\n", [ontologyID intValue]);

  char *value = PQgetvalue(res, 0, 0);
  NSString *ontologySEQ = [[NSString alloc] initWithCString: value encoding: NSUTF8StringEncoding];
  value = PQgetvalue(res, 0, 1);
  NSString *ontologyName = [[NSString alloc] initWithCString: value encoding: NSUTF8StringEncoding];
  PQclear(res);

  // collect all the terms from database
  printf("Processing ontology (%s)...", [ontologyName UTF8String]);
  fflush(stdout);

  sql = [NSString stringWithFormat: @"select term_name from ontology_term_master where ontology_id = %d", [ontologySEQ intValue]];
  res = PQexec(dbConnection, [sql UTF8String]);
  if (PQresultStatus(res) != PGRES_TUPLES_OK) {
    fprintf(stderr, "ERROR: Query failed (%d): %s\n", PQresultStatus(res),
	    PQresultErrorMessage(res));
    return NO;
  }

  NSString *sortFilename = [NSString stringWithFormat: @"sorts_%d.maude", [ontologyID intValue]];
  FILE *sf = fopen([sortFilename UTF8String], "w");

  fprintf(sf, "mod ONTOLOGY_%d_SORTS is\n", [ontologyID intValue]);
  fprintf(sf, "    including ONTOLOGY .\n\n");

  numRows = PQntuples(res);
  for (i = 0; i < numRows; ++i) {
    char *termName = PQgetvalue(res, i, 0);
    fprintf(sf, "    sort \"%s\" .\n", termName);
  }
  fprintf(sf, "\n");
  PQclear(res);

  sql = [NSString stringWithFormat: @"select a.term_name, c.term_name from ontology_term_master a, ontology_term_detail b, ontology_term_master c where a.term_id = b.term_id and b.term_relation = 'is_a' and b.relation_value = c.ontology_term_id and a.ontology_id = %d", [ontologySEQ intValue]];
  res = PQexec(dbConnection, [sql UTF8String]);
  if (PQresultStatus(res) != PGRES_TUPLES_OK) {
    fprintf(stderr, "ERROR: Query failed (%d): %s\n", PQresultStatus(res),
	    PQresultErrorMessage(res));
    return NO;
  }

  numRows = PQntuples(res);
  for (i = 0; i < numRows; ++i) {
    char *termName = PQgetvalue(res, i, 0);
    char *parentName = PQgetvalue(res, i, 1);
    fprintf(sf, "    subsort \"%s\" < \"%s\" .\n", termName, parentName);
  }
  PQclear(res);

  fprintf(sf, "endm\n");
  fclose(sf);
  printf("Success\n");

  return YES;
}

- (BOOL)performOperation
{
  BOOL didOperation = NO;

  if (listDatabase) {
    didOperation = YES;
    if (![self listOntologies]) return NO;
  }

  if (loadDatabase) {
    didOperation = YES;
    if (![self insertOntologyIntoDatabase]) return NO;
  }

  if (generateMaude) {
    didOperation = YES;
    if (![self generateSorts]) return NO;
  }

  if (!didOperation) {
    printf("Nothing to do.\n");
  }

  // if db connection still open, then close it
  if (dbConnection) PQfinish(dbConnection);

  return YES;
}

@end

int main()
{
  NSAutoreleasePool *pool = [NSAutoreleasePool new];

  OntologyManager *aManager = [OntologyManager new];
  if ([aManager processArguments: [[NSProcessInfo processInfo] arguments]]) {
    if (![aManager performOperation]) return 1;
  }

  [pool drain];
  return 0;
}
