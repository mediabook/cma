/*
 * LanguageOperatorController.j
 * Part of Computational Modeling Assistant
 *
 * Controller for Language Operator GUI
 *
 * Created by Scott Christley on April 22, 2013.
 * Copyright 2013, Your Company All rights reserved.
 */

@import <Foundation/CPObject.j>
@import <AppKit/CPView.j>
@import <AppKit/CPWindow.j>
@import <AppKit/CPTokenField.j>
@import <LPKit/LPMultiLineTextField.j>

@import "LanguageOperator.j"

@implementation LanguageOperatorController : CPObject
{
    @outlet CPWindow    theWindow;

    // language operator window
    @outlet CPTableView operatorTable;
    @outlet CPButton addButton;
    @outlet CPButton deleteButton;
    @outlet CPTokenField entryField;
    CPURLConnection operatorConnection;
    CPArray operatorList;
}

- (id)init
{
  self = [super init];
  if (self) {
  }

  return self;
}

- (void)applicationDidFinishLaunching:(CPNotification)aNotification
{
    // This is called when the application is done loading.
}

- (void)awakeFromCib
{
    // This is called when the cib is done loading.
    // You can implement this method on any object instantiated from a Cib.
    // It's a useful hook for setting up current UI values, and other things.

    // In this case, we want the window from Cib to become our full browser window
    //[theWindow setFullPlatformWindow:YES];
    
    /*
    var dataCol = [operatorTable tableColumnWithIdentifier: @"arguments"];
    var aField = [[CPTokenField alloc] initWithFrame:CGRectMakeZero()];
    [aField setDelegate: self];
    [aField setBordered: NO];
    [dataCol setDataView: aField];
    [operatorTable setRowHeight: 30.0]; */
}

- (void)showWindow
{
	if (!theWindow) [CPBundle loadCibNamed: @"LanguageOperators" owner: self];
	[theWindow makeKeyAndOrderFront: self];

  var body = "procedure=OPERATOR_LIST";
  var request = [CPURLRequest requestWithURL: @"http://sur-gpu01.bsd.uchicago.edu/newcma/php/CMAServer.php"];
  
  [request setHTTPMethod: "POST"]; 
  [request setValue:"application/x-www-form-urlencoded" forHTTPHeaderField:"Content-Type"]; 
  [request setValue:[body length] forHTTPHeaderField:"Content-Length"]; 
  [request setHTTPBody: body]; 

  operatorConnection = [CPURLConnection connectionWithRequest:request delegate:self];
}

- (IBAction)addLanguageOperator:(id)sender
{
}

- (IBAction)deleteLanguageOperator:(id)sender
{
}

- (void)retrieveSearchTerms:(CPString)termSubstring
{
	var body = "procedure=TERM_SEARCH&term_substring=" + termSubstring;
  var request = [CPURLRequest requestWithURL: @"http://sur-gpu01.bsd.uchicago.edu/newcma/php/CMAServer.php"];
  
  [request setHTTPMethod: "POST"]; 
  [request setValue:"application/x-www-form-urlencoded" forHTTPHeaderField:"Content-Type"]; 
  [request setValue:[body length] forHTTPHeaderField:"Content-Length"]; 
  [request setHTTPBody: body]; 

  var data = [CPURLConnection sendSynchronousRequest:request returningResponse:nil];

	var terms = [CPArray array];

	var parser = new DOMParser();
	var xmlDoc = parser.parseFromString([data string], "application/xml");
	var node = xmlDoc.getElementsByTagName("term_list");
	var listNode = node[0];
	var ontologyNode = listNode.getElementsByTagName("ontology_term");
	
	for (var j = 0; j < ontologyNode.length; ++j) {
		var ontology = ontologyNode[j];
		var aTerm = [CPDictionary dictionary];
		[terms addObject: aTerm];

		for (var i = 0; i < ontology.childNodes.length; i++) {
			var termItem = ontology.childNodes[i];

			if (termItem.nodeName == "term_id") {
				[aTerm setObject: termItem.textContent forKey: @"term_id"];
			}
			if (termItem.nodeName == "ontology_term_id") {
				[aTerm setObject: termItem.textContent forKey: @"bioportal_id"];
			}
			if (termItem.nodeName == "term_name") {
				[aTerm setObject: termItem.textContent forKey: @"name"];
			}
			if (termItem.nodeName == "term_definition") {
				[aTerm setObject: termItem.textContent forKey: @"description"];
			}
		}
	}

	return terms;
}

- (CPArray)tokenField:(CPTokenField)tokenFieldArg completionsForSubstring:(CPString)substring indexOfToken:(CPInteger)tokenIndex indexOfSelectedItem:(CPInteger)selectedIndex
{
	if ([substring length] < 3) return nil;
	
	var terms = [self retrieveSearchTerms: substring];
	if ([terms count] == 0) return nil;
	
	var tokenList = [CPArray array];
	for (var i = 0; i < [terms count]; ++i) {
		[tokenList addObject: [[terms objectAtIndex: i] objectForKey: @"name"]];
	}
  return tokenList;
}

//
//-----//-----//-----//-----//-----//-----//-----//-----
//
// Manage connection data from URL requests
//

- (void)connection:(CPURLConnection) connection didReceiveData:(CPString)data
{
	//
	// operator list data
	//
	if (connection == operatorConnection) {
		operatorList = [CPArray array];
	
		var parser = new DOMParser();
		var xmlDoc = parser.parseFromString([data string], "application/xml");
		var node = xmlDoc.getElementsByTagName("operator_list");
		var listNode = node[0];
		var operatorNode = listNode.getElementsByTagName("operator");
	
		for (var j = 0; j < operatorNode.length; ++j) {
			var operator = operatorNode[j];
			var anOperator = [LanguageOperator new];
			[operatorList addObject: anOperator];

			for (var i = 0; i < operator.childNodes.length; i++) {
				var item = operator.childNodes[i];

				if (item.nodeName == "operator_name") {
					anOperator.declaration = item.textContent;
				}
				if (item.nodeName == "operator_id") {
					anOperator.operatorID = item.textContent;
				}
				if (item.nodeName == "reduce_id") {
					anOperator.reduceID = item.textContent;
				}
			}
		}

		[operatorTable reloadData];
		operatorConnection = nil;
	}
}

- (void)connection:(CPURLConnection)connection didFailWithError:(CPString)error
{
    //This method is called if the request fails for any reason.
    //[otherLabel setStringValue: @"I got an error!"];
}

//
//-----//-----//-----//-----//-----//-----//-----//-----
//
// Manage table data
//

//
// CPTableView delegate protocol
//
- (void)tableViewSelectionDidChange:(NSNotification *)aNotification
{
}

// CPTableView data source methods
- (int)numberOfRowsInTableView:(CPTableView)tView
{
	return [operatorList count];
}

- (id)tableView:(CPTableView)tView objectValueForTableColumn:(CPTableColumn)tableColumn row:(int)row
{
	if ([tableColumn identifier] == @"operatorID") {
		var anOperator = [operatorList objectAtIndex: row];
		return anOperator.operatorID;
	}
	if ([tableColumn identifier] == @"reduceID") {
		var anOperator = [operatorList objectAtIndex: row];
		return anOperator.reduceID;
	}
	if ([tableColumn identifier] == @"declaration") {
		var anOperator = [operatorList objectAtIndex: row];
		return anOperator.declaration;
	}
	if ([tableColumn identifier] == @"arguments") {
		var anArray = [CPArray array];
		[anArray addObject: @"gene"];
		[anArray addObject: @"entity"];
		return anArray;
	}
	
	return nil;
}

-(void)tableView:(NSTableView *)aTableView setObjectValue:anObject forTableColumn:(NSTableColumn *)aTableColumn row:(int)rowIndex
{
}

- (void)tableView:(NSTableView *)tableView didClickTableColumn:(NSTableColumn *)tableColumn
{
}

@end
