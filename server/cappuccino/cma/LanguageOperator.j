/*
 * LanguageOperator.j
 *
 * Part of Computational Modeling Assistant
 *
 * Created by Scott Christley on April 22, 2013.
 * Copyright 2013, Your Company All rights reserved.
 */

@import <Foundation/CPObject.j>

@implementation LanguageOperator : CPObject
{
	int operatorID;
	int reduceID;
	CPString declaration;
	CPString arguments;
}

@end
