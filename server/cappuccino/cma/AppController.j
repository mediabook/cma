/*
 * AppController.j
 * Computational Modeling Assistant
 *
 * Created by Scott Christley on March 8, 2013.
 * Copyright 2013, Your Company All rights reserved.
 */

@import <Foundation/CPObject.j>
@import <AppKit/CPView.j>
@import <AppKit/CPWindow.j>
@import <LPKit/LPMultiLineTextField.j>

@import "BioModel.j"
@import "LanguageOperatorController.j"
@import "OntologyController.j"

@implementation AppController : CPObject
{
    @outlet CPWindow    theWindow; //this "outlet" is connected automatically by the Cib

		// new biomodel panel

    // open biomodel panel
    @outlet CPPanel openBioModelPanel;
    @outlet CPTableView biomodelListTable;
    @outlet CPButton openButton;
    CPURLConnection biomodelListConnection;
    CPArray biomodelList;

		CPDictionary workbench;
    BioModel theBioModel;
    CPURLConnection biomodelConnection;

		// BioModel outline
		@outlet CPTextField bioModelHeaderTextField;
    @outlet CPOutlineView biomodelOutlineView;
    
    // Swapping details views
    @outlet CPView detailView;
    CPView currentDetailView;
    
    // Info detail view
    @outlet CPWindow infoWindow;
    @outlet CPView infoView;
		// BioModel main form
		@outlet CPTextField bioModelNameTextField;
		@outlet CPTextField bioModelVersionTextField;
		@outlet CPTextField bioModelDescriptionTextField;
		@outlet CPTextField bioModelDateTextField;
		@outlet CPTextField bioModelOwnerTextField;
		@outlet LPMultiLineTextField bioModelNotesTextField;
    @outlet CPOutlineView bioModelHistoryOutlineView;

    // Statements detail view
    @outlet CPWindow biostatementsWindow;
    @outlet CPView biostatementsView;
    @outlet CPTableView biostatementTable;

		// Specifications detail view
    @outlet CPView specificationsView;
		
		// Data detail view
    @outlet CPView dataView;
		
		// Simulations detail view
    @outlet CPView simulationsView;

		// Analyses detail view
    @outlet CPView analysesView;
    
    // KnowledgeBase
    //
    // ontologies
    OntologyController ontologyController;
    // language operators
    LanguageOperatorController loController;
}

- (id)init
{
  self = [super init];
  if (self) {
		workbench = [CPDictionary new];
		var a = [CPArray new];
		[workbench setObject: a forKey: @"managedObjects"];
		var d = [CPDictionary new];
		[d setObject: @"Info" forKey: @"name"];
		[a addObject: d];

		d = [CPDictionary new];
		[d setObject: @"Statements" forKey: @"name"];
		[a addObject: d];

		d = [CPDictionary new];
		[d setObject: @"Specifications" forKey: @"name"];
		[a addObject: d];

		d = [CPDictionary new];
		[d setObject: @"Data" forKey: @"name"];
		[a addObject: d];

		d = [CPDictionary new];
		[d setObject: @"Simulations" forKey: @"name"];
		[a addObject: d];

		d = [CPDictionary new];
		[d setObject: @"Analyses" forKey: @"name"];
		[a addObject: d];
		
		a = [CPArray new];
		[d setObject: a forKey: @"children"];
		d = [CPDictionary new];
		[d setObject: @"subgroup" forKey: @"name"];
		[a addObject: d];
  }

  return self;
}

- (void)applicationDidFinishLaunching:(CPNotification)aNotification
{
    // This is called when the application is done loading.
}

- (void)awakeFromCib
{
    // This is called when the cib is done loading.
    // You can implement this method on any object instantiated from a Cib.
    // It's a useful hook for setting up current UI values, and other things.

    // In this case, we want the window from Cib to become our full browser window
    [theWindow setFullPlatformWindow:YES];
    
    [bioModelNotesTextField setTextColor: [CPColor blackColor]];
}

- (void)swapDetailView:(id)aView
{
	// remove the current detail view
	if (currentDetailView) {
		[currentDetailView removeFromSuperview];
		currentDetailView = nil;
	}

	if (!aView) return;
	currentDetailView = aView;

	[detailView setAutoresizesSubviews: YES];
	[currentDetailView setAutoresizesSubviews: YES];
	[detailView addSubview: currentDetailView];
	[currentDetailView setFrameOrigin: CPMakePoint(0, 0)];
	var r = [detailView frameSize];
	[currentDetailView setFrameSize: r];
	
	if (currentDetailView == infoView) [infoWindow makeFirstResponder: bioModelNameTextField];
}

//
//-----//-----//-----//-----//-----//-----//-----//-----
//
// Manage BioModels
//

- (void)openDocument:(id)sender
{
	[CPBundle loadCibNamed: @"OpenBioModelPanel" owner: self];
	[openBioModelPanel makeKeyAndOrderFront: self];

  var body = "procedure=BIOMODEL_LIST";
  var request = [CPURLRequest requestWithURL: @"http://sur-gpu01.bsd.uchicago.edu/newcma/php/CMAServer.php"];
  
  [request setHTTPMethod: "POST"]; 
  [request setValue:"application/x-www-form-urlencoded" forHTTPHeaderField:"Content-Type"]; 
  [request setValue:[body length] forHTTPHeaderField:"Content-Length"]; 
  [request setHTTPBody: body]; 

  biomodelListConnection = [CPURLConnection connectionWithRequest:request delegate:self];
}

- (IBAction)cancelOpenBioModel:(id)sender
{
  [openBioModelPanel close];
}

- (IBAction)openBioModel:(id)sender
{
	var row = [biomodelListTable selectedRow];
	var aModel = [biomodelList objectAtIndex: row];
	[self loadBioModel: [aModel objectForKey: @"modelID"]];

  [openBioModelPanel close];
}

- (void)loadBioModel: (CPString)anID
{
  var body = "procedure=OPEN_BIOMODEL&model_id=" + anID;
  var request = [CPURLRequest requestWithURL: @"http://sur-gpu01.bsd.uchicago.edu/newcma/php/CMAServer.php"];
  
  [request setHTTPMethod: "POST"]; 
  [request setValue:"application/x-www-form-urlencoded" forHTTPHeaderField:"Content-Type"]; 
  [request setValue:[body length] forHTTPHeaderField:"Content-Length"]; 
  [request setHTTPBody: body]; 

  biomodelConnection = [CPURLConnection connectionWithRequest:request delegate:self];
}

- (void)parseBioModelXML: (CPString)data
{
	theBioModel = [BioModel new];

	var parser = new DOMParser();
	var xmlDoc = parser.parseFromString([data string], "application/xml");
	var node = xmlDoc.getElementsByTagName("biomodel");
	var listNode = node[0];
	//[rosterName setStringValue: rosterListNode.childNodes.length];
	for (var i = 0; i < listNode.childNodes.length; i++) {
		var item = listNode.childNodes[i];
		if (item.nodeName == "model_name") {
			theBioModel.modelName = item.textContent;
			//warRosterName = item.textContent;
			//[rosterName setStringValue: warRosterName];
		}
		if (item.nodeName == "model_id") {
			theBioModel.modelID = item.textContent;
			//warID = item.textContent;
			//[rosterName setStringValue: [rosterName stringValue] + " : " + warID];
		}
	}

	theBioModel.biostatements = [CPArray array];
	var bioNode = listNode.getElementsByTagName("biostatement");
	for (var i = 0; i < bioNode.length; ++i) {
		var stmtNode = bioNode[i];
		var aStatement = [BioStatement new];
		[theBioModel.biostatements addObject: aStatement];

		for (var j = 0; j < stmtNode.childNodes.length; j++) {
			var item = stmtNode.childNodes[j];

			if (item.nodeName == "st_no") {
				aStatement.statementNum = item.textContent;
				//[kingdom setObject: item.textContent forKey: @"line_no"];
				//[rosterName setStringValue: [rosterName stringValue] + " : " + [kingdom objectForKey: @"clan_num"]];
			}

			if (item.nodeName == "st_id") {
				aStatement.statementID = item.textContent;
				//[kingdom setObject: item.textContent forKey: @"clan_num"];
				//[rosterName setStringValue: [rosterName stringValue] + " : " + [kingdom objectForKey: @"clan_num"]];
			}

			if (item.nodeName == "is_definition") {
				aStatement.isDefinition = item.textContent;
			}

			if (item.nodeName == "statement") {
				aStatement.statementText = item.textContent;
			}
		}
	}
	
}

- (void)updateBioModelDisplay
{
  [bioModelHeaderTextField setStringValue: theBioModel.modelName];

	[bioModelNameTextField setStringValue: theBioModel.modelName];

	[biostatementTable reloadData];
}

- (IBAction)addGroup:(id)sender
{
	var selectedRow = [[biomodelOutlineView selectedRowIndexes] firstIndex];
  var item = [biomodelOutlineView itemAtRow:selectedRow];

	var c = [item objectForKey: @"children"];
	if (!c) {
		c = [CPArray array];
		[item setObject: c forKey: @"children"];
	}
	
	var d = [CPDictionary dictionary];
	[d setObject: @"New Group" forKey: @"name"];
	[c addObject: d];
	
	[biomodelOutlineView reloadData];
}

//
//-----//-----//-----//-----//-----//-----//-----//-----
//
// Manage Data
//

//
//-----//-----//-----//-----//-----//-----//-----//-----
//
// Manage Simulations
//

//
//-----//-----//-----//-----//-----//-----//-----//-----
//
// Manage KnowledgeBase
//

- (IBAction)showOntologyBrowserWindow:(id)sender
{
	if (!ontologyController) {
		ontologyController = [OntologyController new];
	}
	[ontologyController showWindow];
}

- (IBAction)showLanguageOperatorWindow:(id)sender
{
	if (!loController) {
		loController = [LanguageOperatorController new];
	}
	[loController showWindow];
}

//
//-----//-----//-----//-----//-----//-----//-----//-----
//
// Manage connection data from URL requests
//

- (void)connection:(CPURLConnection) connection didReceiveData:(CPString)data
{
	//
	// biomodel list data for opening an existing biomodel
	//
	if (connection == biomodelListConnection) {
		biomodelList = [CPArray array];
	
		var parser = new DOMParser();
		var xmlDoc = parser.parseFromString([data string], "application/xml");
		var node = xmlDoc.getElementsByTagName("biomodel_list");
		var listNode = node[0];
		var biomodelNode = listNode.getElementsByTagName("biomodel");
	
		for (var j = 0; j < biomodelNode.length; ++j) {
			var biomodel = biomodelNode[j];
			var aModel = [CPDictionary dictionary];
			[biomodelList addObject: aModel];

			for (var i = 0; i < biomodel.childNodes.length; i++) {
				var item = biomodel.childNodes[i];

				if (item.nodeName == "model_name") {
					[aModel setObject: item.textContent forKey: @"modelName"];
				}
				if (item.nodeName == "model_id") {
					[aModel setObject: item.textContent forKey: @"modelID"];
				}
			}
		}

		[biomodelListTable reloadData];
		biomodelListConnection = nil;
	}

	//
	// Load an existing biomodel
	//
	if (connection == biomodelConnection) {
		[self parseBioModelXML: data];
		[self updateBioModelDisplay];
		biomodelConnection = nil;
	}
}

- (void)connection:(CPURLConnection)connection didFailWithError:(CPString)error
{
    //This method is called if the request fails for any reason.
    //[otherLabel setStringValue: @"I got an error!"];
}

//
//-----//-----//-----//-----//-----//-----//-----//-----
//
// Manage the outline view for the biomodel
//

- (int)outlineView:(NSOutlineView *)outlineView numberOfChildrenOfItem:(id)item
{
//    return (item == nil) [[workbench objectForKey: @"managedObjects"] count] : 0;

  if (item == nil)
    return [[workbench objectForKey: @"managedObjects"] count];
  else {
  	var c = [item objectForKey: @"children"];
  	if (c) return [c count];
  	else return 0;
  }
}
 
- (BOOL)outlineView:(NSOutlineView *)outlineView isItemExpandable:(id)item
{
	if (item == nil) return YES;
	
	var c = [item objectForKey: @"children"];
	if (!c) return NO;
	if ([c count] == 0) return NO;

	return YES;
}
 
- (id)outlineView:(NSOutlineView *)outlineView
    child:(int)index
    ofItem:(id)item
{
  if (item == nil) {
		return [[workbench objectForKey: @"managedObjects"] objectAtIndex: index];
	} else {
		[bioModelHeaderTextField setStringValue: @"hello"];
		return [[item objectForKey: @"children"] objectAtIndex: index];
	}
}
 
- (id)outlineView:(NSOutlineView *)outlineView
    objectValueForTableColumn:(NSTableColumn *)tableColumn
    byItem:(id)item
{
	if ([[tableColumn identifier] isEqualToString: @"name"])
		return [item objectForKey: @"name"];
	else
		return nil;
}

- (void)outlineViewSelectionDidChange:(CPNotification)aNotification
{
	var obj = [aNotification object];
	if (obj == biomodelOutlineView) {
    var selectedRow = [[biomodelOutlineView selectedRowIndexes] firstIndex];
    var item = [biomodelOutlineView itemAtRow:selectedRow];

    if ([item objectForKey: @"name"] == @"Info") {
    	[self swapDetailView: infoView];
   	}

    if ([item objectForKey: @"name"] == @"Statements") {
    	[self swapDetailView: biostatementsView];
   	}

    if ([item objectForKey: @"name"] == @"Specifications") {
    	[self swapDetailView: specificationsView];
   	}

    if ([item objectForKey: @"name"] == @"Data") {
    	[self swapDetailView: dataView];
   	}

    if ([item objectForKey: @"name"] == @"Simulations") {
    	[self swapDetailView: simulationsView];
   	}

    if ([item objectForKey: @"name"] == @"Analyses") {
    	[self swapDetailView: analysesView];
   	}

   //[[theWindow contentView] setBackgroundColor:[CPColor blackColor]];
	}
}

//
//-----//-----//-----//-----//-----//-----//-----//-----
//
// Manage table data
//

//
// CPTableView delegate protocol
//
- (void)tableViewSelectionDidChange:(NSNotification *)aNotification
{
	var obj = [aNotification object];
	if (obj == biomodelListTable) {
		if ([biomodelListTable selectedRow] == CPNotFound) {
			[openButton setEnabled: NO];
		} else {
			[openButton setEnabled: YES];
		}
	}
}

// CPTableView data source methods
- (int)numberOfRowsInTableView:(CPTableView)tView
{
	if (tView == biomodelListTable) {
		return [biomodelList count];
	}

	if (tView == biostatementTable) {
		if (theBioModel) return [theBioModel.biostatements count];
	}

	return 0;
}

- (id)tableView:(CPTableView)tView objectValueForTableColumn:(CPTableColumn)tableColumn row:(int)row
{
	if (tView == biomodelListTable) {
	  var aModel = [biomodelList objectAtIndex: row];
		return [aModel objectForKey: [tableColumn identifier]];
	}

	if (tView == biostatementTable) {
		if (theBioModel) {
			if ([tableColumn identifier] == @"statement") {
				var aStatement = [theBioModel.biostatements objectAtIndex: row];
				return aStatement.statementText;
			}
		}
	}

}

-(void)tableView:(NSTableView *)aTableView setObjectValue:anObject forTableColumn:(NSTableColumn *)aTableColumn row:(int)rowIndex
{
}

- (void)tableView:(NSTableView *)tableView didClickTableColumn:(NSTableColumn *)tableColumn
{
}

@end
