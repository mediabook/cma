/*
 * BioModel.j
 *
 * Part of Computational Modeling Assistant
 *
 * Created by Scott Christley on March 12, 2013.
 * Copyright 2013, Your Company All rights reserved.
 */

@import <Foundation/CPObject.j>

@implementation BioStatement : CPObject
{
	int statementID;
	int statementNum;
	CPString isDefinition;
	CPString statementText;
}

@end

@implementation BioModel : CPObject
{
	int modelID;
	CPString modelName;
	CPString modelVersion;
	int versionedFrom;
	CPDate modelDate;
	
	CPArray biostatements;
}

@end
