/*
 * OntologyController.j
 * Part of Computational Modeling Assistant
 *
 * Controller for Ontology GUI
 *
 * Created by Scott Christley on April 22, 2013.
 * Copyright 2013, Your Company All rights reserved.
 */

@import <Foundation/CPObject.j>
@import <AppKit/CPView.j>
@import <AppKit/CPWindow.j>
@import <LPKit/LPMultiLineTextField.j>

var ProgressHUDSharedInstance = nil; 

@implementation ProgressHUD : CPWindowController 
{ 
        id                                        delegate; 
        CPWindow                                theWindow; 
        CPProgressIndicator                progressIndicator; 
} 

+ (ProgressHUD)sharedProgressHUD 
{ 
    if (!ProgressHUDSharedInstance) 
        ProgressHUDSharedInstance = [[ProgressHUD alloc] init]; 

    return ProgressHUDSharedInstance; 
} 

- (id)init 
{ 
    var theWindow = [[CPPanel alloc] 
        initWithContentRect:CGRectMake(0, 0, 64, 64) 
        styleMask:CPBorderlessWindowMask]; 

    self = [super initWithWindow:theWindow]; 

    if (self) { 

            [theWindow center]; 
        [theWindow setFloatingPanel:YES]; 
        [theWindow setBackgroundColor:[CPColor whiteColor]]; 

        var contentView = [theWindow contentView]; 
        var progressIndicator = [[CPProgressIndicator alloc] 
initWithFrame:[contentView bounds]]; 

        [progressIndicator setStyle:CPProgressIndicatorSpinningStyle]; 
            [progressIndicator sizeToFit]; 
            [contentView addSubview:progressIndicator]; 
    } 
    return self; 
} 

- (void)close 
{ 
        // Convenience class to close window. 
        [[self window] close]; 
} 
@end

@implementation OntologyController : CPObject
{
    @outlet CPWindow    theWindow;

    // ontology window
    @outlet CPOutlineView ontologyTable;
    @outlet CPTextField termID;
    @outlet CPTextField bioportalID;
    @outlet LPMultiLineTextField nameField;
    @outlet LPMultiLineTextField descriptionField;
    @outlet CPWebView mathField;

    CPURLConnection ontologyConnection;
    CPArray ontologyList;
    CPURLConnection termConnection;

    //@outlet CPProgressIndicator anIndicator;
    ProgressHUD progressHUD;
}

- (id)init
{
  self = [super init];
  if (self) {
  }

  return self;
}

- (void)applicationDidFinishLaunching:(CPNotification)aNotification
{
    // This is called when the application is done loading.
}

- (void)awakeFromCib
{
    // This is called when the cib is done loading.
    // You can implement this method on any object instantiated from a Cib.
    // It's a useful hook for setting up current UI values, and other things.

    // In this case, we want the window from Cib to become our full browser window
    //[theWindow setFullPlatformWindow:YES];
    //[anIndicator setStyle:CPProgressIndicatorSpinningStyle]; 
	//[anIndicator startAnimation: self];
    progressHUD = [ProgressHUD sharedProgressHUD]; 
    //[progressHUD setDelegate:self]; 

		var hc = [[ontologyTable tableColumns] objectAtIndex: 0];
		[hc setWidth: [hc maxWidth]];

    [nameField setTextColor: [CPColor blackColor]];
    [descriptionField setTextColor: [CPColor blackColor]];
    
    [mathField loadHTMLString: @"<math display=\"block\"><mrow><mi>x</mi><mo>=</mo><mfrac><mrow><mo>−</mo><mi>b</mi><mo>±</mo><msqrt><mrow><msup><mi>b</mi><mn>2</mn></msup><mo>−</mo><mn>4</mn><mi>a</mi><mi>c</mi></mrow></msqrt></mrow><mrow><mn>2</mn><mi>a</mi></mrow></mfrac></mrow></math>"];
}

- (void)showWindow
{
	if (!theWindow) [CPBundle loadCibNamed: @"Ontologies" owner: self];
	[theWindow makeKeyAndOrderFront: self];

  var body = "procedure=ONTOLOGY_LIST";
  var request = [CPURLRequest requestWithURL: @"http://sur-gpu01.bsd.uchicago.edu/newcma/php/CMAServer.php"];
  
  [request setHTTPMethod: "POST"]; 
  [request setValue:"application/x-www-form-urlencoded" forHTTPHeaderField:"Content-Type"]; 
  [request setValue:[body length] forHTTPHeaderField:"Content-Length"]; 
  [request setHTTPBody: body]; 

  ontologyConnection = [CPURLConnection connectionWithRequest:request delegate:self];


}

- (IBAction)openBioPortal:(id)sender
{
	var item = [ontologyTable itemAtRow: [ontologyTable selectedRow]];
	if (!item) return;

	var rootItem = item;
	var parent = [ontologyTable parentForItem: item];
	while (parent) {
		rootItem = parent;
		parent = [ontologyTable parentForItem: parent];
	}

	var url = "http://bioportal.bioontology.org/ontologies/"
		+ [rootItem objectForKey: @"version_id"];
	if (rootItem != item) url = url + "/?p=terms&conceptid=" + [item objectForKey: @"bioportal_id"];

	window.open(url);
}

- (void)retrieveChildTerms:(CPDictionary)anItem
{
	var body = "procedure=TERM_LIST&term_id=" + [anItem objectForKey: @"term_id"];
  var request = [CPURLRequest requestWithURL: @"http://sur-gpu01.bsd.uchicago.edu/newcma/php/CMAServer.php"];
  
  [request setHTTPMethod: "POST"]; 
  [request setValue:"application/x-www-form-urlencoded" forHTTPHeaderField:"Content-Type"]; 
  [request setValue:[body length] forHTTPHeaderField:"Content-Length"]; 
  [request setHTTPBody: body]; 

  var data = [CPURLConnection sendSynchronousRequest:request returningResponse:nil];

	var terms = [CPArray array];
	[anItem setObject: terms forKey: @"children"];

	var parser = new DOMParser();
	var xmlDoc = parser.parseFromString([data string], "application/xml");
	var node = xmlDoc.getElementsByTagName("term_list");
	var listNode = node[0];
	var ontologyNode = listNode.getElementsByTagName("ontology_term");
	
	for (var j = 0; j < ontologyNode.length; ++j) {
		var ontology = ontologyNode[j];
		var aTerm = [CPDictionary dictionary];
		[terms addObject: aTerm];

		for (var i = 0; i < ontology.childNodes.length; i++) {
			var termItem = ontology.childNodes[i];

			if (termItem.nodeName == "term_id") {
				[aTerm setObject: termItem.textContent forKey: @"term_id"];
			}
			if (termItem.nodeName == "ontology_term_id") {
				[aTerm setObject: termItem.textContent forKey: @"bioportal_id"];
			}
			if (termItem.nodeName == "term_name") {
				[aTerm setObject: termItem.textContent forKey: @"name"];
			}
			if (termItem.nodeName == "term_definition") {
				[aTerm setObject: termItem.textContent forKey: @"description"];
			}
			if (termItem.nodeName == "num_children") {
				[aTerm setObject: termItem.textContent forKey: @"num_children"];
			}
		}
	}

	//
	// ontology term data
	//
/*		var parser = new DOMParser();
		var node = xmlDoc.getElementsByTagName("term_list");
		var listNode = node[0];
		var ontologyNode = listNode.getElementsByTagName("ontology_term");

		for (var j = 0; j < ontologyNode.length; ++j) {
			var ontology = ontologyNode[j];
			var anOntology = [CPDictionary dictionary];
			[ontologyList addObject: anOntology];
		}
*/
		//termConnection = nil;
}

//
//-----//-----//-----//-----//-----//-----//-----//-----
//
// Manage connection data from URL requests
//

- (void)connection:(CPURLConnection) connection didReceiveData:(CPString)data
{
     //[progressHUD showWindow:self]; 

	//
	// ontology list data
	//
	if (connection == ontologyConnection) {
		ontologyList = [CPArray array];
	
		var parser = new DOMParser();
		var xmlDoc = parser.parseFromString([data string], "application/xml");
		var node = xmlDoc.getElementsByTagName("ontology_list");
		var listNode = node[0];
		var ontologyNode = listNode.getElementsByTagName("ontology");
	
		for (var j = 0; j < ontologyNode.length; ++j) {
			var ontology = ontologyNode[j];
			var anOntology = [CPDictionary dictionary];
			[ontologyList addObject: anOntology];

			for (var i = 0; i < ontology.childNodes.length; i++) {
				var item = ontology.childNodes[i];

				if (item.nodeName == "ontology_id") {
					[anOntology setObject: item.textContent forKey: @"ontology_id"];
				}
				if (item.nodeName == "name") {
					[anOntology setObject: item.textContent forKey: @"name"];
				}
				if (item.nodeName == "bioportal_id") {
					[anOntology setObject: item.textContent forKey: @"bioportal_id"];
				}
				if (item.nodeName == "version_id") {
					[anOntology setObject: item.textContent forKey: @"version_id"];
				}
				if (item.nodeName == "description") {
					[anOntology setObject: item.textContent forKey: @"description"];
				}
				if (item.nodeName == "ontology_term") {
					var terms = [anOntology objectForKey: @"children"];
					if (!terms) {
						terms = [CPArray array];
						[anOntology setObject: terms forKey: @"children"];
					}
					
					var aTerm = nil;
					for (var k = 0; k < item.childNodes.length; ++k) {
						var termItem = item.childNodes[k];
						if (!aTerm) {
							aTerm = [CPDictionary dictionary];
							[terms addObject: aTerm];
						}
						
						if (termItem.nodeName == "term_id") {
							[aTerm setObject: termItem.textContent forKey: @"term_id"];
						}
						if (termItem.nodeName == "ontology_term_id") {
							[aTerm setObject: termItem.textContent forKey: @"bioportal_id"];
						}
						if (termItem.nodeName == "term_name") {
							[aTerm setObject: termItem.textContent forKey: @"name"];
						}
						if (termItem.nodeName == "term_definition") {
							[aTerm setObject: termItem.textContent forKey: @"description"];
						}
						if (termItem.nodeName == "num_children") {
							[aTerm setObject: termItem.textContent forKey: @"num_children"];
						}

					}
				}

			}
		}

		[ontologyTable reloadData];
		ontologyConnection = nil;
	}

}

- (void)connection:(CPURLConnection)connection didFailWithError:(CPString)error
{
    //This method is called if the request fails for any reason.
    //[otherLabel setStringValue: @"I got an error!"];
}

//
//-----//-----//-----//-----//-----//-----//-----//-----
//
// Manage the outline view for the biomodel
//

- (int)outlineView:(CPOutlineView *)outlineView numberOfChildrenOfItem:(id)item
{
//    return (item == nil) [[workbench objectForKey: @"managedObjects"] count] : 0;

  if (item == nil)
    return [ontologyList count];
  else {
  	var c = [item objectForKey: @"children"];
  	if (c) return [c count];
  	c = [item objectForKey: @"num_children"];
  	if (c) return [c intValue];
  }
  
  return 0;
}
 
- (BOOL)outlineView:(CPOutlineView *)outlineView isItemExpandable:(id)item
{
	if (item == nil) return YES;
	
	if ([item objectForKey: @"num_children"] != 0) return YES;
	var c = [item objectForKey: @"children"];
	if (!c) return NO;
	if ([c count] == 0) return NO;

	return YES;
}

- (void)outlineViewItemWillExpand:(CPNotification *)aNotification
{
	var obj = [aNotification object];
	if (obj == ontologyTable) {
		var item = [[aNotification userInfo] objectForKey: @"CPObject"];
		var c = [item objectForKey: @"children"];
		if (c) return;
		c = [item objectForKey: @"num_children"];
		if (c == 0) return; // should not be expandable anyways if no children

		// if children not loaded then retrieve them
		[self retrieveChildTerms: item];

	}
}

- (id)outlineView:(NSOutlineView *)outlineView
    child:(int)index
    ofItem:(id)item
{
  if (item == nil) {
		return [ontologyList objectAtIndex: index];
	} else {
		return [[item objectForKey: @"children"] objectAtIndex: index];
	}
	
	return nil;
}
 
- (id)outlineView:(CPOutlineView *)outlineView
    objectValueForTableColumn:(CPTableColumn *)tableColumn
    byItem:(id)item
{
  return [item objectForKey: @"name"];
}

- (void)outlineViewSelectionDidChange:(CPNotification)aNotification
{
	var obj = [aNotification object];
	if (obj == ontologyTable) {
		var item = [ontologyTable itemAtRow: [ontologyTable selectedRow]];
		if (!item) return;

		var anID = [item objectForKey: @"ontology_id"];
		if (!anID) anID = [item objectForKey: @"term_id"];
		[termID setStringValue: anID];
		[bioportalID setStringValue: [item objectForKey: @"bioportal_id"]];
		[nameField setStringValue: [item objectForKey: @"name"]];
		[descriptionField setStringValue: [item objectForKey: @"description"]];
	}
}

@end
