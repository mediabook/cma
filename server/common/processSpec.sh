# setup environment
source /home/www-data/common/gnustep.sh
source /home/www-data/common/cuda.sh

simulationTemplate --code $1 --user $2 --password $3 --dbname $4
