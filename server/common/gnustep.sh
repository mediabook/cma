# Setup GNUstep environment

if [[ `uname` = "Darwin" ]]; then
    # Mac OSX
    . $HOME/Projects/env/jdk.sh
    . $HOME/Projects/env/gnu.sh

    . $HOME/Projects/local/gnustep/System/Library/Makefiles/GNUstep.sh
else
    # linux systems
    if [ -e /usr/share/GNUstep/Makefiles/GNUstep.sh ]; then
	. /usr/share/GNUstep/Makefiles/GNUstep.sh
    else
	# custom GNUstep
	if [ -e /home/scottc/Projects/local/gnustep/System/Library/Makefiles/GNUstep.sh ]; then
	    . /home/scottc/Projects/local/gnustep/System/Library/Makefiles/GNUstep.sh
	else
	    if [ -e /home/scottc/Projects/local/gnustep/share/GNUstep/Makefiles/GNUstep.sh ]; then
	        . /home/scottc/Projects/local/gnustep/share/GNUstep/Makefiles/GNUstep.sh
	    else
	        # standard local GNUstep
		if [ -e /home/scottc/GNUstep/System/Library/Makefiles/GNUstep.sh ]; then
		    . /home/scottc/GNUstep/System/Library/Makefiles/GNUstep.sh
		else
		    echo Cannot find GNUstep!
		fi
	    fi
	fi
    fi
fi

export PS1="[\u@\h (gnustep) \W]\$ "
