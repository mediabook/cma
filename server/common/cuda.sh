# local gnu tools

export PATH=/usr/local/cuda/bin:$PATH

if [[ `uname` = "Darwin" ]]; then
    # Mac OSX
    export DYLD_LIBRARY_PATH=/usr/local/cuda/lib:$DYLD_LIBRARY_PATH
else
    # assume its Linux
    if [[ `uname -m` = "x86_64" ]]; then
	export LD_LIBRARY_PATH=/usr/local/cuda/lib64:$LD_LIBRARY_PATH
	export CUDA_LIBRARY_PATH=/usr/local/cuda/lib64
    else
	export LD_LIBRARY_PATH=/usr/local/cuda/lib:$LD_LIBRARY_PATH
	export CUDA_LIBRARY_PATH=/usr/local/cuda/lib
    fi
fi
